import * as React from 'react';
import Svg, {Circle} from 'react-native-svg';

const CircleTwoSvg = () => (
  <Svg width={146} height={98} fill='none' xmlns='http://www.w3.org/2000/svg'>
    <Circle opacity={0.05} cx={118} cy={118} r={118} fill='#fff' />
  </Svg>
);

export default CircleTwoSvg;
