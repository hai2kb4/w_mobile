import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const SearchSvg = (props) => {
  const {color, width = 24, height = 24, style, strokeWidth = 1.5} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17" fill="none">
        <g opacity="0.75">
        <path d="M7.55151 14.3333C11.1698 14.3333 14.103 11.3485 14.103 7.66666C14.103 3.98476 11.1698 1 7.55151 1C3.93321 1 1 3.98476 1 7.66666C1 11.3485 3.93321 14.3333 7.55151 14.3333Z" stroke="${color}" stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M15.7409 16L12.1785 12.375" stroke="${color}" stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round"/>
        </g>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default SearchSvg;
