import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';
import {theme} from '../../constants';

const HeadsetSvg = (props) => {
  const {color = theme.colors.black, width = 20, height = 20, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
        <g clip-path="url(#clip0_1116_11676)">
        <path d="M12.083 19.582H16.2497C17.6305 19.582 18.7497 18.4629 18.7497 17.082V14.582" stroke="${color}" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M6.24967 10.418H2.08301V14.5846C2.08301 15.5055 2.82884 16.2513 3.74967 16.2513H6.24967V10.418Z" stroke="${color}" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M18.7497 10.418H14.583V16.2513H17.083C18.0038 16.2513 18.7497 15.5055 18.7497 14.5846V10.418Z" stroke="${color}" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M18.7497 10.4167V9.58333C18.7497 4.98083 15.0188 1.25 10.4163 1.25C5.81384 1.25 2.08301 4.98083 2.08301 9.58333V10.4167" stroke="${color}" stroke-miterlimit="10" stroke-linecap="round"/>
        </g>
        <defs>
        <clipPath id="clip0_1116_11676">
        <rect width="20" height="20" fill="white"/>
        </clipPath>
        </defs>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default HeadsetSvg;
