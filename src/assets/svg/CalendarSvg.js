import * as React from 'react';
import {SvgCss} from 'react-native-svg';
import {View} from 'react-native';

const CalendarSvg = ({
  style,
  width = 22,
  height = 24,
  color,
  strokeWidth = 1,
}) => (
  <View style={{...style}}>
    <SvgCss
      xml={`<svg xmlns="http://www.w3.org/2000/svg" width="22" height="25" viewBox="0 0 22 25" fill="none">
      <path d="M6.55556 1.5V4.83333" stroke="${color}" stroke-width="${strokeWidth}" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M15.4444 1.5V4.83333" stroke="${color}" stroke-width="${strokeWidth}" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M1.55556 9.37774H20.4444" stroke="${color}" stroke-width="${strokeWidth}" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M21 8.72222V18.1667C21 21.5 19.3333 23.7222 15.4444 23.7222H6.55556C2.66667 23.7222 1 21.5 1 18.1667V8.72222C1 5.38889 2.66667 3.16667 6.55556 3.16667H15.4444C19.3333 3.16667 21 5.38889 21 8.72222Z" stroke="${color}" stroke-width="${strokeWidth}" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M15.1052 14.5H15.1152" stroke="${color}" stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M15.1052 17.8333H15.1152" stroke="${color}" stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M10.995 14.5H11.005" stroke="${color}" stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M10.995 17.8333H11.005" stroke="${color}" stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M6.88257 14.5H6.89254" stroke="${color}" stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M6.88257 17.8333H6.89254" stroke="${color}" stroke-width="${strokeWidth}" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>`}
      width={width}
      height={height}
    />
  </View>
);

export default CalendarSvg;
