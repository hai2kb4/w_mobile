import * as React from 'react';
import {SvgCss} from 'react-native-svg';
import {View} from 'react-native';
import {theme} from '../../constants';

const ExclamationSvg = (props) => {
  const {color = theme.colors.primary, width, height, style} = props;
  return (
    <View style={style}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17" fill="none">
        <path d="M8.33333 15.6667C12.3834 15.6667 15.6667 12.3834 15.6667 8.33333C15.6667 4.28325 12.3834 1 8.33333 1C4.28325 1 1 4.28325 1 8.33333C1 12.3834 4.28325 15.6667 8.33333 15.6667Z" stroke="${color}" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M8.33398 7.66797V11.668" stroke="${color}" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M8.33268 5.66536C8.70087 5.66536 8.99935 5.36689 8.99935 4.9987C8.99935 4.63051 8.70087 4.33203 8.33268 4.33203C7.96449 4.33203 7.66602 4.63051 7.66602 4.9987C7.66602 5.36689 7.96449 5.66536 8.33268 5.66536Z" fill="${color}"/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default ExclamationSvg;
