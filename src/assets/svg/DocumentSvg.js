import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const DocumentSvg = (props) => {
  const {color = '#b6babf', width = 16, height = 16, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16" fill="none">
        <path d="M9 4H1C0.4 4 0 4.4 0 5V15C0 15.6 0.4 16 1 16H9C9.6 16 10 15.6 10 15V5C10 4.4 9.6 4 9 4Z" fill="${color}"/>
        <path d="M13 0H3V2H12V13H14V1C14 0.4 13.6 0 13 0Z" fill="${color}"/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default DocumentSvg;
