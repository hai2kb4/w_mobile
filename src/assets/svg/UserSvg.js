import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const UserSvg = (props) => {
  const {color, width = 20, height = 24, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="24" viewBox="0 0 20 24" fill="none">
        <path d="M15.2143 6.21429C15.2143 9.09439 12.8801 11.4286 10 11.4286C7.11989 11.4286 4.78571 9.09439 4.78571 6.21429C4.78571 3.33418 7.11989 1 10 1C12.8801 1 15.2143 3.33418 15.2143 6.21429ZM10 14.5714C11.1964 14.5714 12.3345 14.313 13.3621 13.8571H14C17.0364 13.8571 19.5 16.3208 19.5 19.3571V21.2143C19.5 22.1212 18.764 22.8571 17.8571 22.8571H2.14286C1.23596 22.8571 0.5 22.1212 0.5 21.2143V19.3571C0.5 16.3208 2.96364 13.8571 6 13.8571H6.63832C7.669 14.3127 8.80296 14.5714 10 14.5714Z" stroke="${color}"/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default UserSvg;
