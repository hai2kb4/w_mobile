import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const WatchSvg = (props) => {
  const {color, width = 20, height = 20, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="11" height="14" viewBox="0 0 11 14" fill="none">
        <path d="M3.54883 1.375H8.04883" stroke="#123258" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M5.79883 2.125V1.375" stroke="#123258" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M5.79883 5.875V8.125" stroke="#123258" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M5.79883 12.625C8.28411 12.625 10.2988 10.6103 10.2988 8.125C10.2988 5.63972 8.28411 3.625 5.79883 3.625C3.31355 3.625 1.29883 5.63972 1.29883 8.125C1.29883 10.6103 3.31355 12.625 5.79883 12.625Z" stroke="#123258" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default WatchSvg;
