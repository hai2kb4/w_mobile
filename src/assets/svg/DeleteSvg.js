import * as React from 'react';
import {SvgCss} from 'react-native-svg';
import {View} from 'react-native';

const DeleteSvg = (props) => {
  const {color, width = 20, height = 20, style} = props;
  return (
    <View style={{...style}}>
      <SvgCss
        xml={`
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
        <path d="M17.0312 6.09375V17.6562C17.0312 18.0707 16.8666 18.4681 16.5736 18.7611C16.2806 19.0541 15.8832 19.2188 15.4688 19.2188H4.84375C4.42935 19.2188 4.03192 19.0541 3.7389 18.7611C3.44587 18.4681 3.28125 18.0707 3.28125 17.6562V6.09375" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M10.1562 8.59375V15.4688" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M7.03125 8.59375V15.4688" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M13.2812 8.59375V15.4688" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M7.03125 4.21875V1.09375H13.2812V4.21875" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M18.9062 4.21875H1.40625" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        </svg>
        `}
        width={width}
        height={height}
      />
    </View>
  );
};
export default DeleteSvg;
