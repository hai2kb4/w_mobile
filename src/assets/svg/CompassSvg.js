import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';
import {theme} from '../../constants';

const CompassSvg = (props) => {
  const {color = theme.colors.primary, width = 16, height = 16, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
        <path d="M16 8C16 3.6 12.4 0 8 0C3.6 0 0 3.6 0 8C0 12.4 3.6 16 8 16C12.4 16 16 12.4 16 8ZM8.73333 11.6L7.53333 8.53333L4.46667 7.33333C4.13333 7.2 4 6.93333 4 6.66667C4 6.66667 4 6.66667 4 6.6C4 6.33333 4.2 6.06667 4.46667 6L10.8 4.33333C11 4.26667 11.2667 4.33333 11.4667 4.53333C11.6667 4.73333 11.7333 4.93333 11.6667 5.2L10 11.5333C9.93333 11.8 9.66667 12 9.4 12C9.13333 12 8.8 11.8667 8.73333 11.6Z" fill="${color}"/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default CompassSvg;
