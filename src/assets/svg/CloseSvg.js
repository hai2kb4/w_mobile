import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const CloseSvg = () => (
  <Svg width={34} height={34} fill='none' xmlns='http://www.w3.org/2000/svg'>
    <Path
      d='m12.02 12.05 9.9 9.9M12.02 21.95l9.9-9.9'
      stroke='#4C6780'
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </Svg>
);

export default CloseSvg;
