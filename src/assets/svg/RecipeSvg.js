import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const RecipeSvg = (props) => {
  const {color, width = 20, height = 20, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
        <g clip-path="url(#clip0_1116_11686)">
        <path d="M12.6562 19.5312V14.5312H17.6562" stroke="#181918" stroke-miterlimit="10"/>
        <path d="M12.6562 19.5312H2.65625V0.78125H17.6562V14.5312L12.6562 19.5312Z" stroke="#181918" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M9.21875 5.15625H14.2188" stroke="#181918" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M6.09375 14.5312H10.1562" stroke="#181918" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M6.09375 5.15625H7.03125" stroke="#181918" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M9.21875 8.28125H14.2188" stroke="#181918" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M6.09375 8.28125H7.03125" stroke="#181918" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M9.21875 11.4062H14.2188" stroke="#181918" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M6.09375 11.4062H7.03125" stroke="#181918" stroke-miterlimit="10" stroke-linecap="round"/>
        </g>
        <defs>
        <clipPath id="clip0_1116_11686">
        <rect width="20" height="20" fill="white"/>
        </clipPath>
        </defs>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default RecipeSvg;
