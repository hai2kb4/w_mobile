import * as React from 'react';
import {SvgCss} from 'react-native-svg';
import {View} from 'react-native';

const DashboardSvg = (props) => {
  const {color, width = 24, height = 24, style, strokeWidth = 1} = props;
  return (
    <View style={{...style}}>
      <SvgCss
        xml={`
        <svg xmlns="http://www.w3.org/2000/svg" width="23" height="22" viewBox="0 0 23 22" fill="none">
        <path d="M7.81579 20V14.6032H15.1842V20C15.1842 20.5523 15.6319 21 16.1842 21H20.5C21.0523 21 21.5 20.5523 21.5 20V10.3485C21.5 9.4304 21.0796 8.56281 20.3589 7.99389L12.7393 1.97839C12.0127 1.40474 10.9873 1.40474 10.2607 1.97839L2.64107 7.99389C1.92043 8.56282 1.5 9.4304 1.5 10.3485V20C1.5 20.5523 1.94771 21 2.5 21H6.81579C7.36807 21 7.81579 20.5523 7.81579 20Z" stroke=${color} stroke-width="${strokeWidth}"/>
        </svg>
        `}
        width={width}
        height={height}
      />
    </View>
  );
};
export default DashboardSvg;
