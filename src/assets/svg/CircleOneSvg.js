import * as React from 'react';
import Svg, {Circle} from 'react-native-svg';

const CircleOneSvg = () => (
  <Svg width={106} height={88} fill='none' xmlns='http://www.w3.org/2000/svg'>
    <Circle opacity={0.05} cx={118} cy={-30} r={118} fill='#fff' />
  </Svg>
);

export default CircleOneSvg;
