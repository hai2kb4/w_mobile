import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const PlusSvg = (props) => {
  const {color = '#1B78D4', width = 30, height = 30} = props;

  return (
    <View>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <rect width="24" height="24" rx="4" fill="none"/>
        <path d="M19 11.25H12.75V5H11.25V11.25H5V12.75H11.25V19H12.75V12.75H19V11.25Z" fill="${color}"/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default PlusSvg;
