import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const Heart1Svg = (props) => {
  const {
    color = '#181918',
    width = 20,
    height = 20,
    style,
    fill = 'none',
  } = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="${fill}">
        <path d="M17.5622 3.43785C15.645 1.52072 12.5365 1.52072 10.6194 3.43785C10.3829 3.67432 10.1776 3.92879 10 4.19635C9.82244 3.92879 9.61625 3.6735 9.38059 3.43785C7.46346 1.52072 4.35498 1.52072 2.43785 3.43785C0.520717 5.35498 0.520717 8.46346 2.43785 10.3806L9.99918 17.9436L17.5622 10.3806C19.4793 8.46346 19.4793 5.35498 17.5622 3.43785Z" stroke=${color} stroke-width="1" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default Heart1Svg;
