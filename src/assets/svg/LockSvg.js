import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const LockSvg = (props) => {
  const {color = '#181918', width = 20, height = 20, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
        <g clip-path="url(#clip0_1116_11621)">
        <path d="M16.2503 9.58203H4.58366C3.66318 9.58203 2.91699 10.3282 2.91699 11.2487V17.9154C2.91699 18.8358 3.66318 19.582 4.58366 19.582H16.2503C17.1708 19.582 17.917 18.8358 17.917 17.9154V11.2487C17.917 10.3282 17.1708 9.58203 16.2503 9.58203Z" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M10.4167 16.2513C11.3371 16.2513 12.0833 15.5051 12.0833 14.5846C12.0833 13.6642 11.3371 12.918 10.4167 12.918C9.49619 12.918 8.75 13.6642 8.75 14.5846C8.75 15.5051 9.49619 16.2513 10.4167 16.2513Z" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M14.5833 6.2502V5.41687C14.5942 4.3229 14.1702 3.2694 13.4044 2.48803C12.6387 1.70667 11.594 1.26142 10.5 1.2502H10.4167C9.3227 1.23932 8.26919 1.66337 7.48783 2.42911C6.70646 3.19485 6.26122 4.23957 6.25 5.33354V6.2502" stroke="${color}" stroke-miterlimit="10" stroke-linecap="square"/>
        </g>
        <defs>
        <clipPath id="clip0_1116_11621">
        <rect width="20" height="20" fill="white"/>
        </clipPath>
        </defs>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default LockSvg;
