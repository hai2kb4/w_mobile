import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const DropSvg = (props) => {
  const {color, width = 20, height = 20, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="89" height="109" viewBox="0 0 89 109" fill="none">
        <path d="M47.3359 2.11843C45.4185 0.627189 42.7556 0.627189 40.8383 2.11843C30.7191 9.84095 0.840862 35.0323 1.00064 64.3779C1.00064 88.1313 20.3336 107.518 44.1403 107.518C67.947 107.518 87.2799 88.1846 87.2799 64.4312C87.3332 35.5117 57.4018 9.89421 47.3359 2.11843Z" stroke="#123258" stroke-width="2" stroke-miterlimit="10"/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default DropSvg;
