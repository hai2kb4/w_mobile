import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const QRCodeSvg = (props) => {
  const {color, width = 20, height = 20, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
        <g clip-path="url(#clip0_1725_827)">
        <path d="M3.83333 0.5H1.5C0.947715 0.5 0.5 0.947715 0.5 1.5V3.83333" stroke=${color} stroke-linecap="round"/>
        <path d="M16.1667 0.5H18.5C19.0523 0.5 19.5 0.947715 19.5 1.5V3.83333" stroke=${color} stroke-linecap="round"/>
        <path d="M16.1667 19.5H18.5C19.0523 19.5 19.5 19.0523 19.5 18.5V16.1667" stroke=${color} stroke-linecap="round"/>
        <path d="M3.83333 19.5H1.5C0.947715 19.5 0.5 19.0523 0.5 18.5V16.1667" stroke=${color} stroke-linecap="round"/>
        <path d="M8.5 8C8.5 8.27614 8.27614 8.5 8 8.5H4C3.72386 8.5 3.5 8.27614 3.5 8V4C3.5 3.72386 3.72386 3.5 4 3.5H8C8.27614 3.5 8.5 3.72386 8.5 4V8Z" stroke=${color} stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M8.5 16C8.5 16.2761 8.27614 16.5 8 16.5H4C3.72386 16.5 3.5 16.2761 3.5 16V12C3.5 11.7239 3.72386 11.5 4 11.5H8C8.27614 11.5 8.5 11.7239 8.5 12V16Z" stroke=${color} stroke-miterlimit="10" stroke-linecap="square"/>
        <path d="M16.5 8C16.5 8.27614 16.2761 8.5 16 8.5H12C11.7239 8.5 11.5 8.27614 11.5 8V4C11.5 3.72386 11.7239 3.5 12 3.5H16C16.2761 3.5 16.5 3.72386 16.5 4V8Z" stroke=${color} stroke-miterlimit="10" stroke-linecap="square"/>
        <rect x="11" y="11" width="2" height="2" rx="0.5" fill=${color}/>
        <rect x="5" y="5" width="2" height="2" fill=${color}/>
        <rect x="13" y="5" width="2" height="2" fill=${color}/>
        <rect x="5" y="13" width="2" height="2" fill=${color}/>
        <rect x="11" y="15" width="2" height="2" rx="0.5" fill=${color}/>
        <rect x="15" y="15" width="2" height="2" rx="0.5" fill=${color}/>
        <rect x="13" y="13" width="2" height="2" rx="0.5" fill=${color}/>
        <rect x="15" y="11" width="2" height="2" rx="0.5" fill=${color}/>
        </g>
        <defs>
        <clipPath id="clip0_1725_827">
        <rect width="20" height="20" fill="white"/>
        </clipPath>
        </defs>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default QRCodeSvg;
