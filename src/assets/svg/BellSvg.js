import React from 'react';
import {View} from 'react-native';
import {SvgCss} from 'react-native-svg';

const BellSvg = (props) => {
  const {color, width = 18, height = 20, style} = props;

  return (
    <View style={{...style}}>
      <SvgCss
        xml={`<svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
        <path d="M14.8337 9.16536V6.66536C14.8337 5.11827 14.2191 3.63454 13.1251 2.54057C12.0312 1.44661 10.5474 0.832031 9.00033 0.832031C7.45323 0.832031 5.9695 1.44661 4.87554 2.54057C3.78157 3.63454 3.16699 5.11827 3.16699 6.66536V9.16536C3.16699 11.9154 0.666992 12.582 0.666992 14.1654C0.666992 15.582 3.91699 16.6654 9.00033 16.6654C14.0837 16.6654 17.3337 15.582 17.3337 14.1654C17.3337 12.582 14.8337 11.9154 14.8337 9.16536Z" stroke= ${
          color || '#8D8D8D'
        } stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M9.00026 18.3333C8.15776 18.3333 7.36609 18.305 6.62109 18.25C6.77992 18.7566 7.09634 19.1994 7.52427 19.5137C7.9522 19.828 8.4693 19.9975 9.00026 19.9975C9.53122 19.9975 10.0483 19.828 10.4762 19.5137C10.9042 19.1994 11.2206 18.7566 11.3794 18.25C10.6344 18.305 9.84276 18.3333 9.00026 18.3333Z" fill=${
          color || '#8D8D8D'
        }/>
        </svg>`}
        width={width}
        height={height}
      />
    </View>
  );
};

export default BellSvg;
