import DashboardSvg from './svg/DashboardSvg';
import PeopleSvg from './svg/PeopleSvg';
import BellSvg from './svg/BellSvg';
import ChatSvg from './svg/ChatSvg';
import BurgerMenuSvg from './svg/BurgerMenuSvg';
import CircleOneSvg from './svg/CircleOneSvg';
import CircleTwoSvg from './svg/CircleTwoSvg';
import VerticalMenuSvg from './svg/VerticalMenuSvg';
import StarSvg from './svg/StarSvg';
import ClockSvg from './svg/ClockSvg';
import ViewAllSvg from './svg/ViewAllSvg';
import HeartSvg from './svg/HeartSvg';
import BabySvg from './svg/BabySvg';
import KidneySvg from './svg/KidneySvg';
import ToothSvg from './svg/ToothSvg';
import VirusSvg from './svg/VirusSvg';
import UltrasoundSvg from './svg/UltrasoundSvg';
import DnaStructureSvg from './svg/DnaStructureSvg';
import DiagnosticArrowSvg from './svg/DiagnosticArrowSvg';
import HeartBeatSvg from './svg/HeartBeatSvg';
import PatientSvg from './svg/PatientSvg';
import DoctorRatingStar from './svg/DoctorRatingStar';
import IllustrationOneSvg from './svg/IllustrationOneSvg';
import IllustrationTwoSvg from './svg/IllustrationTwoSvg';
import IllustrationThreeSvg from './svg/IllustrationThreeSvg';
import GoBackSvg from './svg/GoBackSvg';
import FacebookSvg from './svg/FacebookSvg';
import TestIsReadySvg from './svg/TestIsReadySvg';
import ConfirmedSvg from './svg/ConfirmedSvg';
import RejectedSvg from './svg/RejectedSvg';
import MarkAsReadSvg from './svg/MarkAsReadSvg';
import DiagnosticVirusSvg from './svg/DiagnosticVirusSvg';
import BigStarSvg from './svg/BigStarSvg';
import WorkingHoursSvg from './svg/WorkingHoursSvg';
import ReadMoreSvg from './svg/ReadMoreSvg';
import InputCheckSvg from './svg/InputCheckSvg';
import EyeOffSvg from './svg/EyeOffSvg';
import KeySvg from './svg/KeySvg';
import UserSvg from './svg/UserSvg';
import ReplySvg from './svg/ReplySvg';
import CalendarSvg from './svg/CalendarSvg';
import CalendarSuccesSvg from './svg/CalendarSuccesSvg';
import FiltersSvg from './svg/FiltersSvg';
import SortingSvg from './svg/SortingSvg';
import SmallStarSvg from './svg/SmallStarSvg';
import CheckSvg from './svg/CheckSvg';
import CloseSvg from './svg/CloseSvg';
import Arrow from './svg/Arrow';
import MedicalSvg from './svg/MedicalSvg';
import DoctorSvg from './svg/DoctorSvg';
import BloodDropSvg from './svg/BloodDropSvg';
import LogOutSvg from './svg/LogOutSvg';
import ElementsSvg from './svg/ElementsSvg';
import LoaderSvg from './svg/LoaderSvg';
import DownloadSvg from './svg/DownloadSvg';
import PhotoSvg from './svg/PhotoSvg';
import StarsSvg from './svg/StarsSvg';
import ExclamationSvg from './svg/ExclamationSvg';
import SearchSvg from './svg/SearchSvg';
import LockSvg from './svg/LockSvg';
import QRCodeSvg from './svg/QRCodeSvg';
import ShareSvg from './svg/ShareSvg';
import RecipeSvg from './svg/RecipeSvg';
import WatchSvg from './svg/WatchSvg';
import HeadsetSvg from './svg/HeadsetSvg';
import SettingSvg from './svg/SettingSvg';
import DropSvg from './svg/DropSvg';
import AddressSvg from './svg/AddressSvg';
import Heart1Svg from './svg/HeartSvg1';
import DocumentSvg from './svg/DocumentSvg';
import CallSvg from './svg/CallSvg';
import ImageSvg from './svg/ImageSvg';
import IdentificationSvg from './svg/IdentificationSvg';
import CameraSvg from './svg/CameraSvg';
import FlashLightSvg from './svg/FlashLightSvg';
import DeleteSvg from './svg/DeleteSvg';
import DotsSvg from './svg/DotsSvg';
import SendSvg from './svg/SendSvg';
import HospitalSvg from './svg/HospitalSvg';
import CommentMedicalSvg from './svg/CommentMedicalSvg';
import TruckMedicalSvg from './svg/TruckMedicalSvg';
import BookMedicalSvg from './svg/BookMedicalSvg';
import DoctorMedicalSvg from './svg/DoctorMedicalSvg';
import HeartPulseSvg from './svg/HeartPulseSvg';
import CompassSvg from './svg/CompassSvg';
import CommentDotSvg from './svg/CommentDotSvg';
import UserAccountSvg from './svg/UserAccountSvg';
import StoreSvg from './svg/StoreSvg';
import TikTokSvg from './svg/TikTokSvg';
import VerifySvg from './svg/VerifySvg';
import PlusSvg from './svg/PlusSvg';
import WalletSvg from './svg/WalletSvg';

const svg = {
  WalletSvg,
  PlusSvg,
  VerifySvg,
  TikTokSvg,
  UserAccountSvg,
  StoreSvg,
  CommentDotSvg,
  CompassSvg,
  HeartPulseSvg,
  DoctorMedicalSvg,
  BookMedicalSvg,
  TruckMedicalSvg,
  CommentMedicalSvg,
  HospitalSvg,
  SendSvg,
  DotsSvg,
  DeleteSvg,
  FlashLightSvg,
  CameraSvg,
  IdentificationSvg,
  ImageSvg,
  CallSvg,
  DocumentSvg,
  Heart1Svg,
  AddressSvg,
  DropSvg,
  SettingSvg,
  ShareSvg,
  RecipeSvg,
  WatchSvg,
  HeadsetSvg,
  LockSvg,
  QRCodeSvg,
  SearchSvg,
  ExclamationSvg,
  DashboardSvg,
  PeopleSvg,
  BellSvg,
  ChatSvg,
  BurgerMenuSvg,
  CircleOneSvg,
  CircleTwoSvg,
  VerticalMenuSvg,
  StarSvg,
  ClockSvg,
  ViewAllSvg,
  HeartSvg,
  BabySvg,
  KidneySvg,
  ToothSvg,
  VirusSvg,
  UltrasoundSvg,
  DnaStructureSvg,
  DiagnosticArrowSvg,
  HeartBeatSvg,
  PatientSvg,
  DoctorRatingStar,
  IllustrationOneSvg,
  IllustrationTwoSvg,
  IllustrationThreeSvg,
  GoBackSvg,
  FacebookSvg,
  TestIsReadySvg,
  ConfirmedSvg,
  RejectedSvg,
  MarkAsReadSvg,
  DiagnosticVirusSvg,
  BigStarSvg,
  WorkingHoursSvg,
  ReadMoreSvg,
  InputCheckSvg,
  EyeOffSvg,
  KeySvg,
  UserSvg,
  ReplySvg,
  CalendarSvg,
  CalendarSuccesSvg,
  FiltersSvg,
  SortingSvg,
  SmallStarSvg,
  CheckSvg,
  CloseSvg,
  Arrow,
  MedicalSvg,
  DoctorSvg,
  BloodDropSvg,
  LogOutSvg,
  ElementsSvg,
  LoaderSvg,
  DownloadSvg,
  PhotoSvg,
  StarsSvg,
};

export {svg};
