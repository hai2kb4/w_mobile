import {View, ScrollView} from 'react-native';
import React, {memo} from 'react';
import {components} from '.';
import {theme} from '../constants';
import {useNavigation} from '@react-navigation/native';
import {SCREEN_NAME} from '../navigation/ScreenName';

const TopRateItems = ({
  version,
  title = 'Bác sỹ',
  viewall = false,
  data = [],
  onPress,
  screenName = SCREEN_NAME.DoctorDetails,
  isFetched,
}) => {
  const navigation = useNavigation();

  return (
    <View style={{marginTop: 8, backgroundColor: theme.colors.white}}>
      <components.BlockHeader
        title={title}
        containerStyle={{margin: 16}}
        viewAll={viewall}
        onPress={onPress}
      />
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{paddingLeft: 16}}
      >
        {data.map((item, index, array) => {
          return (
            <components.CardItem
              key={index}
              index={index}
              version={version}
              item={item}
              array={array}
              navigation={navigation}
              screenName={screenName}
              isFetched={isFetched}
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default memo(TopRateItems);
