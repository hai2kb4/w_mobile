import React from 'react';
import {Text, View} from 'react-native';
import {theme} from '../constants';
import {components} from '.';
import {formatDate, formatDayAndMonth} from '../utils';
import moment from 'moment';

export default function AvailableItem({
  data = [],
  dateValue,
  timeValue,
  setDateValue,
  setTimeValue,
  onPress,
}) {
  return (
    <>
      <components.Touchable
        containerStyle={{
          height: 36,
          ...theme.styles.justifyContentCenter,
          flex: 1,
          padding: 4,
          borderRadius: 8,
          borderColor: data.appointmentId
            ? theme.colors.primary
            : theme.colors.stroke,
          ...theme.fonts.textStyle14,
          borderWidth: 1,
        }}
        onPress={onPress}
        content={
          <Text
            style={{
              color: data.appointmentId
                ? theme.colors.primary
                : theme.colors.black,
            }}
          >
            {data.appointmentId
              ? `Lịch khám của bạn sắp tới ${formatDate(data.bookingDate)}`
              : 'Lịch khám'}
          </Text>
        }
      />
      <components.ScrollViewLayout
        containerStyle={{backgroundColor: theme.colors.white, marginTop: 12}}
        horizontal={true}
        content={data.availableDates?.map((item) => (
          <components.Touchable
            onPress={() => {
              setDateValue(moment(item.value).date());
              setTimeValue('');
            }}
            containerStyle={{
              backgroundColor:
                moment(item.value).date() === dateValue
                  ? theme.colors.primary
                  : theme.colors.white,
              borderRadius: 4,
              padding: 8,
            }}
            key={item.value}
            content={
              <View style={{alignItems: 'center'}}>
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    fontWeight: 'bold',
                    color:
                      moment(item.value).date() === dateValue
                        ? theme.colors.white
                        : theme.colors.black,
                  }}
                >
                  {item.title}
                </Text>
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    color:
                      moment(item.value).date() === dateValue
                        ? theme.colors.white
                        : theme.colors.black,
                  }}
                >
                  {formatDayAndMonth(item.value)}
                </Text>
              </View>
            }
          />
        ))}
      />
      <Text
        style={{
          ...theme.fonts.textStyle14,
          color: theme.colors.textColor,
          marginTop: 6,
        }}
      >
        Chọn một khung giờ để đặt
      </Text>
      <components.ScrollViewLayout
        containerStyle={{backgroundColor: theme.colors.white, marginTop: 12}}
        horizontal={true}
        content={data.availableDates
          ?.find((x) => moment(x.value).date() === dateValue)
          ?.timeSlot?.map((item, index, arr) => (
            <components.Touchable
              disabled={!item.isAvailable}
              onPress={() => setTimeValue(item.title)}
              containerStyle={{
                borderRadius: 6,
                paddingVertical: 10,
                paddingHorizontal: 8,
                borderWidth: 1,
                marginRight: arr.length === index + 1 ? 0 : 8,
                backgroundColor: !item.isAvailable
                  ? theme.colors.grey1
                  : theme.colors.white,
                borderColor: !item.isAvailable
                  ? theme.colors.grey1
                  : timeValue === item.title
                  ? theme.colors.primary
                  : theme.colors.textColor,
              }}
              key={index}
              content={
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    color:
                      timeValue === item.title
                        ? theme.colors.primary
                        : theme.colors.textColor,
                  }}
                >
                  {item.title}
                </Text>
              }
            />
          ))}
      />
    </>
  );
}
