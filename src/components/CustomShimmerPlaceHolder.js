import React, {memo} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {createShimmerPlaceholder} from 'react-native-shimmer-placeholder';

const ShimmerPlaceholder = createShimmerPlaceholder(LinearGradient);
const CustomShimmerPlaceHolder = ({
  containerStyle,
  content,
  isFetched,
  ...rest
}) => {
  return (
    <ShimmerPlaceholder style={containerStyle} visible={isFetched} {...rest}>
      {content}
    </ShimmerPlaceholder>
  );
};

export default memo(CustomShimmerPlaceHolder);
