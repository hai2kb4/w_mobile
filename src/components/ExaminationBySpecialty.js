import React, {useContext} from 'react';
import {Text, View} from 'react-native';
import {theme} from '../constants';
import {components} from '../components';
import globalContext from '../globalContext';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {searchType} from '../constants/constants';
import {SCREEN_NAME} from '../navigation/ScreenName';

const numberItem = 8;

const ExaminationBySpecialty = () => {
  const specialityList = [];
  const navigation = useNavigation();
  const global = useContext(globalContext);
  const renderItem = (item) => {
    return (
      <components.Touchable
        key={item.id}
        containerStyle={{
          width: theme.sizes.width / 4,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onPress={() =>
          navigation.navigate(SCREEN_NAME.SearchScreen, {
            type: searchType.all,
            speciality: item.id,
          })
        }
        content={
          <>
            <View
              style={{
                borderRadius: 100,
                padding: 5,
                marginTop: 4,
              }}
            >
              <components.CustomShimmerPlaceHolder
                containerStyle={{width: 30, height: 30, borderRadius: 100}}
                isFetched={true}
                content={
                  <components.ImageLoader
                    source={item.icon}
                    style={{width: 30, height: 30}}
                  />
                }
              />
            </View>
            <components.CustomShimmerPlaceHolder
              containerStyle={{width: 62, marginTop: 8}}
              isFetched={true}
              content={
                <Text
                  style={{
                    width: 65,
                    fontSize: 12,
                    textAlign: 'center',
                    color: theme.colors.darkBlue,
                    ...theme.fonts.SFProText_600SemiBold,
                  }}
                  numberOfLines={2}
                >
                  {item.name}
                </Text>
              }
            />
          </>
        }
      />
    );
  };

  return (
    specialityList.length > 0 && (
      <View
        style={{
          backgroundColor: theme.colors.white,
          marginTop: 8,
        }}
      >
        <components.BlockHeader
          title='Khám theo chuyên khoa'
          containerStyle={{margin: 16}}
        />
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            flexWrap: 'wrap',
            marginBottom: specialityList.length <= numberItem ? 16 : 0,
          }}
        >
          {specialityList?.slice(0, 8).map(renderItem)}
        </View>

        {specialityList.length > numberItem && (
          <components.Touchable
            containerStyle={{
              alignItems: 'center',
              padding: 10,
              borderWidth: 1,
              borderColor: theme.colors.stroke,
              borderRadius: 100,
              margin: 16,
            }}
            onPress={() => global.modalRef.current?.open()}
            content={
              <Text
                style={{
                  ...theme.fonts.textStyle13,
                  color: theme.colors.darkBlue,
                }}
              >
                Xem tất cả các chuyên khoa
              </Text>
            }
          />
        )}
      </View>
    )
  );
};

export default ExaminationBySpecialty;
