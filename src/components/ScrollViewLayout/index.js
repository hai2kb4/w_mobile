import React from 'react';
import {theme} from '../../constants';
import {ScrollView} from 'react-native';

const ScrollViewLayout = ({
  containerStyle,
  content,
  refreshControl,
  horizontal,
  contentContainerStyle,
  ...rest
}) => {
  return (
    <ScrollView
      refreshControl={refreshControl}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      horizontal={horizontal}
      style={{
        flex: 1,
        backgroundColor: theme.colors.backgroundColor,
        ...containerStyle,
      }}
      contentContainerStyle={contentContainerStyle}
      {...rest}
    >
      {content}
    </ScrollView>
  );
};
export default ScrollViewLayout;
