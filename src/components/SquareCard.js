import {View, Text} from 'react-native';
import React from 'react';
import {theme} from '../constants';

import {svg} from '../assets';

const SquareCard = ({version, children, containerStyle}) => {
  const v1 = () => {
    return (
      <View>
        <Text>Box</Text>
      </View>
    );
  };

  const v2 = () => {
    return (
      <View>
        <Text>Box</Text>
      </View>
    );
  };

  const v3 = ({children, containerStyle}) => {
    return (
      <View
        style={{
          width: 100,
          height: 100,
          backgroundColor: theme.colors.darkBlue,
          justifyContent: 'center',
          alignItems: 'center',
          ...containerStyle,
        }}
      >
        <View style={{position: 'absolute', right: 0, top: 0}}>
          <svg.ElementsSvg version={'v3'} />
        </View>
        <View style={{position: 'absolute', right: 0, bottom: 0}}>
          <svg.ElementsSvg version={'v4'} />
        </View>

        {children}
      </View>
    );
  };

  return version === 'v1'
    ? v1({children, containerStyle})
    : version === 'v2'
    ? v2({children, containerStyle})
    : version === 'v3'
    ? v3({children, containerStyle})
    : null;
};

export default SquareCard;
