import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';

import {theme} from '../constants';
import {svg} from '../assets';

const MyTestsDiagnosticsItem = ({title, date, containerStyle, btnText}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        paddingBottom: 20,
        borderBottomColor: theme.colors.stroke,
        ...containerStyle,
      }}
    >
      <View>
        <Text
          style={{
            ...theme.fonts.H6,
            color: theme.colors.darkBlue,
            marginBottom: 4,
          }}
        >
          {title}
        </Text>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <svg.ClockSvg />
          <Text
            style={{
              marginLeft: 6,
              fontSize: 12,
              color: theme.colors.textColor,
              ...theme.fonts.SFProText_400Regular,
              lineHeight: 12 * 1.7,
            }}
          >
            {date}
          </Text>
        </View>
      </View>
      <TouchableOpacity
        style={{
          width: 100,
          height: 33,
          backgroundColor:
            btnText === 'in progress'
              ? theme.colors.mainGreen
              : theme.colors.darkBlue,
          justifyContent: 'center',
          flexDirection: 'row',
          alignItems: 'center',
        }}
      >
        {btnText === 'in progress' && <svg.LoaderSvg />}
        {btnText === 'Download' && <svg.DownloadSvg />}
        <Text
          style={{
            ...theme.fonts.SFProText_400Regular,
            fontSize: 10,
            color: theme.colors.white,
            textTransform: 'capitalize',
            marginLeft: 4,
          }}
        >
          {btnText}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default MyTestsDiagnosticsItem;
