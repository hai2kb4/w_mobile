import React from 'react';
import {Calendar, LocaleConfig} from 'react-native-calendars';
import {theme} from '../constants';
const language = 'vi';
LocaleConfig.locales[language] = {
  monthNames: [
    'Tháng 1',
    'Tháng 2',
    'Tháng 3',
    'Tháng 4',
    'Tháng 5',
    'Tháng 6',
    'Tháng 7',
    'Tháng 8',
    'Tháng 9',
    'Tháng 10',
    'Tháng 11',
    'Tháng 12',
  ],
  dayNames: ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'],
  dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
};
LocaleConfig.defaultLocale = language;

const CalendarPicker = ({markedDates, onDayPress, markingType}, props) => {
  return (
    <Calendar
      theme={{
        selectedDayBackgroundColor: theme.colors.primary,
        selectedDayTextColor: theme.colors.white,
        todayTextColor: theme.colors.primary,
      }}
      onDayPress={onDayPress}
      markingType={markingType}
      markedDates={{
        [markedDates]: {
          selected: true,
          disableTouchEvent: true,
        },
      }}
      {...props}
    />
  );
};

export default CalendarPicker;
