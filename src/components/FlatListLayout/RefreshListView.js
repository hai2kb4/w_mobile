import React from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Text,
  View,
  SectionList,
  RefreshControl,
} from 'react-native';
import {theme} from '../../constants';
import {components} from '..';

export const RefreshState = {
  Idle: 0,
  HeaderRefreshing: 1,
  FooterRefreshing: 2,
  NoMoreData: 3,
  Failure: 4,
  EmptyData: 5,
};

const RefreshListView = (props) => {
  const onEndReached = () => {
    if (shouldStartFooterRefreshing()) {
      props.onFooterRefresh &&
        props.onFooterRefresh(RefreshState.FooterRefreshing);
    }
  };

  const shouldStartFooterRefreshing = () => {
    let {refreshState, data, scrolling, isSectionList} = props;
    if (data && (data.length === 0 || !scrolling)) {
      return false;
    }

    if (isSectionList) {
      let numOfRecord = 0;
      data.forEach((item) => {
        numOfRecord += item.data.length;
      });

      if (numOfRecord < 10) {
        return;
      }
    }

    return refreshState === RefreshState.Idle;
  };

  const onHeaderRefresh = () => {
    if (shouldStartHeaderRefreshing()) {
      props.onHeaderRefresh(RefreshState.HeaderRefreshing);
    }
  };

  const shouldStartHeaderRefreshing = () => {
    return !(
      props.refreshState === RefreshState.HeaderRefreshing ||
      props.refreshState === RefreshState.FooterRefreshing
    );
  };

  const renderFooter = () => {
    let footer = null;
    let {
      footerRefreshingText,
      footerFailureText,
      footerNoMoreDataText,
      footerEmptyDataText,

      footerRefreshingComponent,
      footerFailureComponent,
      footerNoMoreDataComponent,
      footerEmptyDataComponent,
    } = props;
    switch (props.refreshState) {
      case RefreshState.Idle:
        footer = <View style={styles.footerContainer} />;
        break;
      case RefreshState.Failure: {
        footer = (
          <components.Touchable
            onPress={() => {
              if (props.data.length === 0) {
                props.onHeaderRefresh &&
                  props.onHeaderRefresh(RefreshState.HeaderRefreshing);
              } else {
                props.onFooterRefresh &&
                  props.onFooterRefresh(RefreshState.FooterRefreshing);
              }
            }}
            content={
              footerFailureComponent ? (
                footerFailureComponent
              ) : (
                <View style={styles.footerContainer}>
                  <Text style={styles.footerText}>{footerFailureText}</Text>
                </View>
              )
            }
          />
        );
        break;
      }
      case RefreshState.EmptyData: {
        footer = (
          <components.Touchable
            onPress={() => {
              props.onHeaderRefresh &&
                props.onHeaderRefresh(RefreshState.HeaderRefreshing);
            }}
            content={
              footerEmptyDataComponent ? (
                footerEmptyDataComponent
              ) : (
                <View style={styles.footerContainer}>
                  <Text style={styles.footerText}>{footerEmptyDataText}</Text>
                </View>
              )
            }
          />
        );
        break;
      }
      case RefreshState.FooterRefreshing: {
        footer = footerRefreshingComponent ? (
          footerRefreshingComponent
        ) : (
          <View style={styles.footerContainer}>
            <ActivityIndicator size='small' color={theme.colors.textColor} />
            <Text style={{...styles.footerText, ...styles.footerTextNoMore}}>
              {footerRefreshingText}
            </Text>
          </View>
        );
        break;
      }
      case RefreshState.NoMoreData: {
        footer = footerNoMoreDataComponent ? (
          footerNoMoreDataComponent
        ) : (
          <View style={styles.footerContainer}>
            <Text style={styles.footerText}>{footerNoMoreDataText}</Text>
          </View>
        );
        break;
      }
    }
    return footer;
  };

  const renderContent = () => {
    let {data, renderItem, isSectionList, ...rest} = props;

    if (!isSectionList) {
      return (
        <FlatList
          ref={props.listRef}
          data={data}
          onEndReached={onEndReached}
          refreshControl={
            <RefreshControl
              refreshing={props.refreshState === RefreshState.HeaderRefreshing}
              onRefresh={onHeaderRefresh}
              title='Đang tải...'
              tintColor={theme.colors.black}
              titleColor={theme.colors.black}
            />
          }
          refreshing={props.refreshState === RefreshState.HeaderRefreshing}
          ListFooterComponent={renderFooter}
          onEndReachedThreshold={0.1}
          initialNumToRender={10}
          maxToRenderPerBatch={10}
          renderItem={renderItem}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          removeClippedSubviews={true}
          {...rest}
        />
      );
    }
    return (
      <SectionList
        ref={props.listRef}
        sections={data}
        onEndReached={onEndReached}
        refreshing={props.refreshState === RefreshState.HeaderRefreshing}
        refreshControl={
          <RefreshControl
            refreshing={props.refreshState === RefreshState.HeaderRefreshing}
            onRefresh={onHeaderRefresh}
            title='Đang tải...'
            tintColor={theme.colors.black}
            titleColor={theme.colors.black}
          />
        }
        ListFooterComponent={renderFooter}
        onEndReachedThreshold={0.1}
        initialNumToRender={10}
        maxToRenderPerBatch={10}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        {...rest}
      />
    );
  };

  return renderContent();
};

export default RefreshListView;

const styles = StyleSheet.create({
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 24,
  },
  footerText: {...theme.fonts.textStyle13, color: theme.colors.textColor},
  footerTextNoMore: {
    marginLeft: 7,
  },
});
