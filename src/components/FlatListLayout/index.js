import React, {useState, useRef, forwardRef, useImperativeHandle} from 'react';
import {Text} from 'react-native';
import {theme} from '../../constants';
import RefreshListView, {RefreshState} from './RefreshListView';
export const VIEWABILITY_CONFIG = {
  minimumViewTime: 3000,
  viewAreaCoveragePercentThreshold: 100,
  waitForInteraction: true,
};
const FlatListLayout = (props, ref) => {
  const {
    renderItem,
    withScrollIndicator = false,
    ListHeaderComponent,
    stickyHeaderIndices,
    onHeaderRefreshHandler,
    onFooterRefreshHandler,
    onScrolling,
    containerStyle,
    totalCount,
    hasNextPage = false,
  } = props;

  const [refreshState, setRefreshState] = useState(
    RefreshState.HeaderRefreshing,
  );
  const [scrolling, setScrolling] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const flatListRef = useRef(null);

  useImperativeHandle(ref, () => ({
    refreshingWithSmoothHandle() {
      if (flatListRef.current.props && flatListRef.current.props.listRef) {
        flatListRef.current.props.listRef.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        });
      }

      setRefreshState(RefreshState.HeaderRefreshing);
    },
    refreshingHandle() {
      setRefreshState(RefreshState.HeaderRefreshing);
    },
    idleHandle() {
      setRefreshState(RefreshState.Idle);
    },
    footerRefreshingHandle() {
      setRefreshState(RefreshState.FooterRefreshing);
    },
    setSource(source, isLoadMore) {
      if (isLoadMore) {
        const combineData = [...dataSource, ...source];
        setDataSource(combineData);
      } else {
        setDataSource([...source]);
      }
      setRefreshState(RefreshState.Idle);
    },
    getSource() {
      return dataSource;
    },
  }));

  const keyExtractor = (_, index) => index.toString();

  const onHeaderRefresh = () => {
    if (onHeaderRefreshHandler) {
      setRefreshState(RefreshState.HeaderRefreshing);
      onHeaderRefreshHandler(() => {
        setRefreshState(RefreshState.Idle);
      });
    }
  };

  const onFooterRefresh = () => {
    if (onFooterRefreshHandler) {
      if (!hasNextPage) {
        setRefreshState(RefreshState.NoMoreData);
      } else {
        setRefreshState(RefreshState.FooterRefreshing);
        onFooterRefreshHandler(() => {
          setRefreshState(RefreshState.Idle);
        });
      }
      setScrolling(false);
    }
  };

  const onScrollDragList = () => {
    setScrolling(true);
    onScrolling && onScrolling();
  };

  return (
    <RefreshListView
      listRef={flatListRef}
      data={dataSource}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
      refreshState={refreshState}
      onHeaderRefresh={onHeaderRefresh}
      onFooterRefresh={onFooterRefresh}
      initialNumToRender={10}
      maxToRenderPerBatch={10}
      onScrollBeginDrag={onScrollDragList}
      scrolling={scrolling}
      footerRefreshingText='Đang lấy thêm dữ liệu...'
      footerNoMoreDataText={`${totalCount} kết quả được tìm thấy.`}
      footerEmptyDataText='Không có dữ liệu'
      ListHeaderComponent={ListHeaderComponent}
      stickyHeaderIndices={stickyHeaderIndices}
      showsVerticalScrollIndicator={withScrollIndicator}
      ListEmptyComponent={
        <Text
          style={{
            marginTop: 20,
            ...theme.fonts.textStyle16,
            textAlign: 'center',
          }}
        >
          Không tìm thấy kết quả.
        </Text>
      }
      style={{backgroundColor: theme.colors.grey2, flex: 1, ...containerStyle}}
    />
  );
};

export default forwardRef(FlatListLayout);
