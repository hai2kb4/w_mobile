import {View, Text, StyleSheet, Linking} from 'react-native';
import React, {memo} from 'react';
import {useNavigation} from '@react-navigation/native';
import {theme, categories} from '../constants';
import {components} from '../components';
import {useSelector} from 'react-redux';

const Categories = () => {
  const navigation = useNavigation();
  const setting = [];
  const renderCategory = (item, index) => {
    return (
      <components.Touchable
        key={index}
        containerStyle={{
          width: theme.sizes.width / 3.5,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 12,
        }}
        onPress={() =>
          index === 4
            ? setting.storeUrl && Linking.openURL(setting.storeUrl)
            : navigation.navigate(item.screenName, {type: index})
        }
        content={
          <>
            <item.icon />
            <Text
              style={{
                height: 30,
                width: 65,
                fontSize: 12,
                marginTop: 8,
                textAlign: 'center',
                color: theme.colors.darkBlue,
                ...theme.fonts.SFProText_600SemiBold,
              }}
              numberOfLines={2}
            >
              {item.name}
            </Text>
          </>
        }
      />
    );
  };

  return (
    <View style={styles.container}>
      {categories.map((item, index) => renderCategory(item, index + 1))}
    </View>
  );
};

export default memo(Categories);

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.white,
    marginHorizontal: 16,
    paddingBottom: 12,
    borderRadius: 12,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
});
