import {View, Text} from 'react-native';
import React from 'react';
import {theme} from '../constants';
import Touchable from './Touchable';

const BlockHeader = ({containerStyle, title, viewAll, onPress}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        ...containerStyle,
      }}
    >
      <Text style={theme.fonts.textStyle16}>{title}</Text>
      {viewAll && (
        <Touchable
          containerStyle={theme.styles.rowAlignItemsCenter}
          onPress={onPress}
          content={
            <Text
              style={{
                ...theme.fonts.textStyle16,
                color: theme.colors.primary,
                marginRight: 5,
              }}
            >
              Xem tất cả
            </Text>
          }
        />
      )}
    </View>
  );
};

export default BlockHeader;
