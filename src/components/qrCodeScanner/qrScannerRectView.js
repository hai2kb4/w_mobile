import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Animated, Image, Easing} from 'react-native';
import {theme} from '../../constants';
const defaultRectStyle = {
  height: theme.sizes.width - 128,
  width: theme.sizes.width - 128,
  borderWidth: 0,
  borderColor: theme.colors.black,
  marginBottom: 0,
};
const defaultCornerStyle = {
  height: 31,
  width: 31,
  borderWidth: 2,
  borderColor: theme.colors.primary,
};
const defaultScanBarStyle = {
  marginHorizontal: 8,
  borderRadius: 2,
  backgroundColor: theme.colors.primary,
};

const QRScannerRectView = (props) => {
  const {
    cornerStyle,
    isShowCorner = true,
    maskColor = 'transparent',
    rectStyle,
    cornerOffsetSize = 0,
    scanBarStyle,
    containerStyles,
    scanBarAnimateTime = 3000,
    isShowScanBar = true,
    hintText = '',
    scanBarAnimateReverse,
  } = props;
  const [animatedValue, setAnimatedValue] = useState(new Animated.Value(0));

  const innerScanBarStyle = Object.assign(defaultScanBarStyle, scanBarStyle);
  const innerRectStyle = Object.assign(defaultRectStyle, rectStyle);
  const innerCornerStyle = Object.assign(defaultCornerStyle, cornerStyle);
  const {borderWidth} = innerCornerStyle;

  useEffect(() => {
    scanBarMove();
  }, []);

  const scanBarMove = () => {
    const scanBarHeight = isShowScanBar ? innerScanBarStyle.height || 4 : 0;
    const startValue = innerCornerStyle.borderWidth;
    const endValue =
      innerRectStyle.height -
      (innerRectStyle.borderWidth +
        cornerOffsetSize +
        innerCornerStyle.borderWidth) -
      scanBarHeight;
    if (scanBarAnimateReverse) {
      Animated.sequence([
        Animated.timing(animatedValue, {
          toValue: endValue,
          duration: scanBarAnimateTime,
          easing: Easing.linear,
          isInteraction: false,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: startValue,
          duration: scanBarAnimateTime,
          easing: Easing.linear,
          isInteraction: false,
          useNativeDriver: true,
        }),
      ]).start(() => scanBarMove());
    } else {
      setAnimatedValue(startValue);
      Animated.timing(animatedValue, {
        toValue: endValue,
        duration: scanBarAnimateTime,
        easing: Easing.linear,
        isInteraction: false,
        useNativeDriver: true,
      }).start(() => scanBarMove());
    }
  };

  const getCornerStyle = () => {
    return {
      height: innerCornerStyle.height,
      width: innerCornerStyle.width,
      borderColor: innerCornerStyle.borderColor,
    };
  };

  const getBackgroundColor = () => {
    return {backgroundColor: maskColor};
  };

  const getRectSize = () => {
    return {
      height: innerRectStyle.height,
      width: innerRectStyle.width,
    };
  };

  const getBorderStyle = () => {
    return {
      height: innerRectStyle.height - cornerOffsetSize * 2,
      width: innerRectStyle.width - cornerOffsetSize * 2,
      borderWidth: innerRectStyle.borderWidth,
      borderColor: innerRectStyle.borderColor,
    };
  };

  const animatedStyle = {
    transform: [{translateY: animatedValue}],
  };

  const getScanImageWidth = () => {
    return innerRectStyle.width - innerScanBarStyle.marginHorizontal * 2;
  };

  const renderScanBar = () => {
    const {scanBarImage} = props;
    if (!isShowScanBar) {
      return;
    }
    return scanBarImage ? (
      <Image
        source={scanBarImage}
        style={[
          innerScanBarStyle,
          {
            resizeMode: 'contain',
            backgroundColor: 'transparent',
            width: getScanImageWidth(),
          },
        ]}
      />
    ) : (
      <View style={[{height: 4}, innerScanBarStyle]} />
    );
  };

  return (
    <View style={{...styles.container, containerStyles}}>
      <View
        style={{
          ...theme.styles.rowAlignItemsCenter,
          justifyContent: 'center',
          flex: 1,
        }}
      >
        {/* <View style={[getBackgroundColor(), {flex: 1}]} /> */}
        <View style={[styles.viewfinder, getRectSize()]}>
          <View style={getBorderStyle()}>
            <Animated.View style={animatedStyle}>
              {renderScanBar()}
            </Animated.View>
          </View>

          {isShowCorner && (
            <View
              style={[
                getCornerStyle(),
                styles.topLeftCorner,
                {borderLeftWidth: borderWidth, borderTopWidth: borderWidth},
              ]}
            />
          )}

          {isShowCorner && (
            <View
              style={[
                getCornerStyle(),
                styles.topRightCorner,
                {borderRightWidth: borderWidth, borderTopWidth: borderWidth},
              ]}
            />
          )}

          {isShowCorner && (
            <View
              style={[
                getCornerStyle(),
                styles.bottomLeftCorner,
                {
                  borderLeftWidth: borderWidth,
                  borderBottomWidth: borderWidth,
                },
              ]}
            />
          )}

          {isShowCorner && (
            <View
              style={[
                getCornerStyle(),
                styles.bottomRightCorner,
                {
                  borderRightWidth: borderWidth,
                  borderBottomWidth: borderWidth,
                },
              ]}
            />
          )}
        </View>

        {/* <View style={[getBackgroundColor(), {flex: 1}]} /> */}
      </View>
    </View>
  );
};
export default QRScannerRectView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: theme.sizes.width - 32,
  },
  viewfinder: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  topLeftCorner: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  topRightCorner: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  bottomLeftCorner: {
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  bottomRightCorner: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
});
