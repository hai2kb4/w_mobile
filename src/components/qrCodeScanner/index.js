import React, {useEffect, useState} from 'react';
import {
  Animated,
  PermissionsAndroid,
  Platform,
  StyleSheet,
  View,
  Easing,
  Vibration,
} from 'react-native';
import {theme} from '../../constants/theme';
import {RNCamera as Camera} from 'react-native-camera';
import {request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import PropTypes from 'prop-types';

const CAMERA_FLASH_MODE = Camera.Constants.FlashMode;
const CAMERA_FLASH_MODES = [
  CAMERA_FLASH_MODE.torch,
  CAMERA_FLASH_MODE.on,
  CAMERA_FLASH_MODE.off,
  CAMERA_FLASH_MODE.auto,
];

const QRCodeScanner = (props) => {
  const {
    containerStyle,
    notAuthorizedView,
    pendingAuthorizationView,
    topContent,
    onRead,
    reactivate = PropTypes.bool,
    showMarker,
    flashMode = PropTypes.oneOf(CAMERA_FLASH_MODES),
    customMarker,
    fadeIn = PropTypes.bool,
    vibrate = PropTypes.bool,
    reactivateTimeout = 3000,
    cameraProps,
    cameraType = Camera.Constants.Type.back,
    buttonPositive = '',
    permissionDialogMessage = '',
    permissionDialogTitle = '',
    checkAndroidPermissions = false,
    cameraStyle,
  } = props;
  const [isAuthorized, setIsAuthorized] = useState(false);
  const [isAuthorizationChecked, setIsAuthorizationChecked] = useState(false);
  const [fadeInOpacity, setFadeInOpacity] = useState(new Animated.Value(0));
  const [scanning, setScanning] = useState(false);
  const [disableVibrationByUser, setDisableVibrationByUser] = useState(false);

  let scannerTimeout = null;

  useEffect(() => {
    if (Platform.OS === 'ios') {
      request(PERMISSIONS.IOS.CAMERA).then((cameraStatus) => {
        setIsAuthorizationChecked(true);
        setIsAuthorized(!(cameraStatus === RESULTS.GRANTED));
      });
    } else if (Platform.OS === 'android' && checkAndroidPermissions) {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
        title: props.permissionDialogTitle,
        message: props.permissionDialogMessage,
      }).then((granted) => {
        const isAuthorized = granted === PermissionsAndroid.RESULTS.GRANTED;
        setIsAuthorizationChecked(true);
        setIsAuthorized(isAuthorized);
      });
    } else {
      setIsAuthorizationChecked(true);
      setIsAuthorized(true);
    }

    if (fadeIn) {
      Animated.sequence([
        Animated.delay(1000),
        Animated.timing(fadeInOpacity, {
          toValue: 1,
          easing: Easing.inOut(Easing.quad),
          useNativeDriver: true,
        }),
      ]).start();
    }
  }, []);

  useEffect(() => {
    if (scannerTimeout !== null) {
      clearTimeout(scannerTimeout);
    }
    scannerTimeout = null;
  }, []);

  const handleBarCodeRead = (e) => {
    if (!scanning && !disableVibrationByUser) {
      if (vibrate) {
        Vibration.vibrate();
      }
      setScanning(true);
      onRead(e);
      if (reactivate) {
        scannerTimeout = setTimeout(
          () => setScanning(false),
          reactivateTimeout,
        );
      }
    }
  };

  const renderTopContent = () => {
    if (topContent) {
      return topContent;
    }
    return null;
  };
  const renderCamera = () => {
    if (isAuthorized) {
      if (fadeIn) {
        return (
          <Animated.View
            style={{
              opacity: fadeInOpacity,
              backgroundColor: 'transparent',
              height: styles.camera.height,
            }}
          >
            {renderCameraComponent()}
          </Animated.View>
        );
      }
      return renderCameraComponent();
    } else if (!isAuthorizationChecked) {
      return pendingAuthorizationView;
    } else {
      return notAuthorizedView;
    }
  };

  const renderCameraComponent = () => {
    return (
      <Camera
        androidCameraPermissionOptions={{
          title: permissionDialogTitle,
          message: permissionDialogMessage,
          buttonPositive: buttonPositive,
        }}
        style={{...styles.camera, ...cameraStyle}}
        onBarCodeRead={handleBarCodeRead}
        type={cameraType}
        flashMode={flashMode}
        captureAudio={false}
        {...cameraProps}
      >
        {renderCameraMarker()}
      </Camera>
    );
  };

  const renderCameraMarker = () => {
    if (showMarker) {
      if (customMarker) {
        return customMarker;
      } else {
        return (
          <View style={styles.rectangleContainer}>
            <View
              style={[
                styles.rectangle,
                props.markerStyle ? props.markerStyle : null,
              ]}
            />
          </View>
        );
      }
    }
    return null;
  };

  return (
    <View style={{...styles.mainContainer, ...containerStyle}}>
      <View style={styles.infoView}>{renderTopContent()}</View>
      {renderCamera()}
    </View>
  );
};

export default QRCodeScanner;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  infoView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: theme,
  },

  camera: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    height: theme.sizes.height,
    width: theme.sizes.width,
  },

  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  rectangle: {
    height: 250,
    width: 250,
    borderWidth: 2,
    borderColor: '#00FF00',
    backgroundColor: 'transparent',
  },
});
