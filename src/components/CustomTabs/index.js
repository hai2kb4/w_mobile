import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import FlatListLayout from '../FlatListLayout/RefreshListView';
import {theme} from '../../constants';
import {components} from '../index';
import {svg} from '../../assets';

const CustomTabs = ({arrTab = [], renderItem, dataSource = []}) => {
  const [selectedTab, setSelectTab] = useState(1);
  const rederButtonTab = (name, index, length) => {
    return (
      <components.Touchable
        onPress={() => setSelectTab(index)}
        containerStyle={{
          ...styles.buttonTab,
          width: (theme.sizes.width - 36) / length,
          backgroundColor:
            selectedTab === index ? theme.colors.white : theme.colors.gray,
          borderWidth: selectedTab === index ? 1 : 0,
          borderColor: theme.colors.stroke,
        }}
        content={
          <Text
            style={{
              ...theme.fonts.textStyle13,
              fontWeight: selectedTab === index ? '700' : '400',
            }}
          >
            {name}
          </Text>
        }
      />
    );
  };

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <View style={styles.tab}>
          {arrTab.map((item, index, arr) =>
            rederButtonTab(item, index, arr.length),
          )}
        </View>
        <View style={styles.textInput}>
          <svg.SearchSvg width={20} height={20} color={theme.colors.darkBlue} />
          <TextInput style={{flex: 1}} placeholder='Tìm kiếm...' />
          <View
            style={{
              width: 2,
              height: 20,
              backgroundColor: theme.colors.gray,
              marginHorizontal: 8,
            }}
          />
          <components.Touchable
            content={<svg.FiltersSvg color={theme.colors.darkBlue} />}
          />
        </View>
      </View>
    );
  };

  const renderContent = () => {
    return <FlatListLayout dataSource={dataSource} renderItem={renderItem} />;
  };

  return (
    <View style={styles.container}>
      {renderHeader()}
      {renderContent()}
    </View>
  );
};

export default CustomTabs;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  header: {
    borderBottomWidth: 1,
    borderColor: theme.colors.stroke,
    paddingHorizontal: 16,
    paddingVertical: 8,
    backgroundColor: theme.colors.grey1,
  },
  tab: {
    backgroundColor: theme.colors.gray,
    flexDirection: 'row',
    width: theme.sizes.width - 32,
    padding: 2,
    borderRadius: 4,
  },
  buttonTab: {
    padding: 4,
    borderRadius: 4,
    ...theme.styles.justifyContentCenter,
  },
  textInput: {
    borderWidth: 1,
    borderColor: theme.colors.stroke,
    height: 40,
    borderRadius: 4,
    marginTop: 4,
    backgroundColor: theme.colors.white,
    ...theme.styles.rowAlignItemsCenter,
    paddingHorizontal: 12,
  },
});
