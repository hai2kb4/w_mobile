import React, {
  forwardRef,
  useEffect,
  useRef,
  useState,
  useImperativeHandle,
} from 'react';
import useDebounce from '../Debounce';
import {
  Platform,
  StyleSheet,
  TextInput,
  View,
  ActivityIndicator,
} from 'react-native';
import {theme} from '../../constants';
import {useIsMount} from '../../utils/checkFirstRender';
import {svg} from '../../assets';

const SearchBarCustom = (props, ref) => {
  const {
    onTextSearchChange,
    containerStyle = {},
    isFocus = false,
    ...rest
  } = props;
  const isMount = useIsMount();
  const [isSearching, setSearching] = useState(false);
  const [textSearch, setTextSearch] = useState('');
  const searchBarRef = useRef(false);
  const debouncedSearchKeyword = useDebounce(textSearch, 1000);

  useImperativeHandle(ref, () => ({
    toggleFocus(isFocus) {
      if (isFocus) {
        searchBarRef.current.focus();
      } else {
        searchBarRef.current.blur();
      }
    },
    setValue(valueSearch) {
      setTextSearch(valueSearch);
    },
    clearText() {
      clearTextSearch();
    },
  }));

  // useEffect(() => {
  //   if (isMount) {
  //     return;
  //   }
  //   onTextSearchChange(textSearch, () => {
  //     setSearching(false);
  //   });
  // }, [debouncedSearchKeyword]);

  const updateSearch = (value) => {
    setSearching(!!value);
    setTextSearch(value);
  };

  const clearTextSearch = () => {
    setSearching(false);
    setTextSearch('');
  };

  return (
    <View style={{...styles.search, ...containerStyle}}>
      <svg.SearchSvg
        width={20}
        height={20}
        color={theme.colors.placeholderTextColor}
      />
      <TextInput
        ref={searchBarRef}
        value={textSearch}
        onChangeText={updateSearch}
        style={{
          flex: 1,
          padding: 0,
          color: theme.colors.white,
          marginLeft: 8,
        }}
        autoFocus={isFocus}
        returnKeyType='done'
        placeholderTextColor={theme.colors.placeholderTextColor}
        {...rest}
      />
      {isSearching && (
        <ActivityIndicator sizes='small' color={theme.colors.black} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    top: 0,
    backgroundColor: theme.colors.transparent,
    paddingHorizontal: 10,
  },
  searchInputContainer: {
    paddingVertical: Platform.OS === 'ios' ? 3 : 0,
    backgroundColor: '#F2F2F2',
  },
  searchInput: {
    theme,
  },
  search: {
    borderRadius: 4,
    width: theme.sizes.width - 32,
    paddingVertical: 5,
    paddingHorizontal: 10,
    ...theme.styles.rowAlignItemsCenter,
    backgroundColor: theme.colors.transparent,
  },
});

export default forwardRef(SearchBarCustom);
