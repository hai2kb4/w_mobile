import React from 'react';
import DatePicker from 'react-native-date-picker';

const DatePickerModal = ({
  isOpen = false,
  title,
  date,
  mode = 'date',
  onConfirm,
  onCancel,
}) => {
  return (
    <DatePicker
      modal
      title={title}
      cancelText='Hủy'
      confirmText='Xác nhận'
      open={isOpen}
      date={date}
      mode={mode}
      androidVariant={'iosClone'}
      locale={'vi'}
      is24hourSource={'locale'}
      onConfirm={onConfirm}
      onCancel={onCancel}
    />
  );
};

export default DatePickerModal;
