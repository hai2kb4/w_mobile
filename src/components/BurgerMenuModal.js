import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React from 'react';
import Modal from 'react-native-modal';
import {useSelector, useDispatch} from 'react-redux';
import {closeModal} from '../store/modalSlice';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useNavigation} from '@react-navigation/native';
import {theme} from '../constants';
import {svg} from '../assets';
import {components} from '../components';
import {authSelector} from '../store/slices/authSlice';
import {SCREEN_NAME} from '../navigation/ScreenName';

export const accountImage = require('../assets/images/avatar.png');

const BurgerMenuModal = () => {
  const {profile} = useSelector(authSelector);
  const navigation = useNavigation();
  const insets = useSafeAreaInsets();
  const dispatch = useDispatch();
  const showModal = useSelector((state) => state.modal.modalIsOpen);

  return (
    <Modal
      isVisible={showModal}
      onBackdropPress={() => dispatch(closeModal())}
      hideModalContentWhileAnimating={true}
      backdropTransitionOutTiming={0}
      style={{margin: 0}}
      animationIn='slideInLeft'
      animationOut='slideOutLeft'
    >
      <View
        style={{
          width: '75%',
          height: '100%',
          backgroundColor: theme.colors.white,
        }}
      >
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            paddingTop: insets.top + 10,
            paddingBottom: insets.bottom,
          }}
        >
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
              paddingHorizontal: 14,
              marginBottom: 40,
            }}
            onPress={() => dispatch(closeModal())}
          >
            <svg.CloseSvg />
          </TouchableOpacity>
          <View
            style={{
              paddingHorizontal: 16,
              borderBottomWidth: 1,
              paddingBottom: 34,
              borderBottomColor: theme.colors.stroke,
              marginBottom: 16,
            }}
          >
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}
              onPress={() => {
                dispatch(closeModal());
                if (profile && !profile.isGuest) {
                  navigation.navigate(SCREEN_NAME.MyProfile);
                } else {
                  navigation.navigate(SCREEN_NAME.SignIn);
                }
              }}
            >
              <View
                style={{
                  width: 40,
                  height: 40,
                  marginRight: 10,
                  backgroundColor: '#F6F8FB',
                  borderRadius: 20,
                }}
              >
                <components.ImageLoader
                  imageUrl={profile?.avatarUrl}
                  source={accountImage}
                  style={{width: '100%', height: '100%', marginRight: 10}}
                />
              </View>
              <View>
                <Text
                  style={{
                    ...theme.fonts.H6,
                    color: theme.colors.darkBlue,
                    textTransform: 'capitalize',
                  }}
                >
                  {profile && !profile.isGuest
                    ? profile?.fullName
                    : 'Đăng nhập/Đăng ký'}
                </Text>
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    color: theme.colors.textColor,
                  }}
                >
                  {profile && !profile.isGuest
                    ? profile?.phone
                    : 'Trải nghiệm hệ thống Medic'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginHorizontal: 16,
              height: 50,
              borderWidth: 1,
              borderColor: theme.colors.stroke,
              paddingHorizontal: 16,
              justifyContent: 'center',
              marginBottom: 16,
            }}
          >
            <TextInput
              placeholder='Tìm kiếm'
              style={{flex: 1}}
              placeholderTextColor={theme.colors.textColor}
            />
          </View>
          <components.BurgerMenuItem
            title={'FAQ'}
            onPress={() => {
              dispatch(closeModal());
              navigation.navigate(SCREEN_NAME.Faq);
            }}
          />
          <components.BurgerMenuItem
            title={'Chính sách bảo mật'}
            onPress={() => {
              dispatch(closeModal());
              navigation.navigate(SCREEN_NAME.PrivacyPolicy);
            }}
          />
          <components.BurgerMenuItem
            title={'Trợ giúp & Hỗ trợ'}
            onPress={() => {}}
          />
        </ScrollView>
      </View>
    </Modal>
  );
};

export default BurgerMenuModal;
