import {View, FlatList} from 'react-native';
import React from 'react';

import {components} from '../components';
import {svg} from '../assets';

const DiagnosticsAndTests = ({version}) => {
  const data = [
    {
      id: 1,
      title: 'Cellular and chemical analysis',
      description:
        'Blood analysis, amniocentesis, gastric fluid analysis, kidney function test, prenatal testing, protein-bou...',
      color: '#34B6FF',
      icon: <svg.VirusSvg />,
    },
    {
      id: 2,
      title: 'Cellular and chemical analysis',
      description:
        'Blood analysis, amniocentesis, gastric fluid analysis, kidney function test, prenatal testing, protein-bou...',
      color: '#18DF80',
      icon: <svg.DnaStructureSvg />,
    },
    {
      id: 3,
      title: 'Cellular and chemical analysis',
      description:
        'Blood analysis, amniocentesis, gastric fluid analysis, kidney function test, prenatal testing, protein-bou...',
      color: '#F5B715',
      icon: <svg.UltrasoundSvg />,
    },
    {
      id: 4,
      title: 'Cellular and chemical analysis',
      description:
        'Blood analysis, amniocentesis, gastric fluid analysis, kidney function test, prenatal testing, protein-bou...',
      color: '#F84C6B',
      icon: <svg.HeartBeatSvg />,
    },
  ];

  return (
    <View style={{marginTop: 20}}>
      <components.BlockHeader
        title='Dịch vụ'
        containerStyle={{marginHorizontal: 16, marginBottom: 20}}
      />
      <FlatList
        horizontal={false}
        numColumns={3}
        showsHorizontalScrollIndicator={false}
        style={{paddingLeft: 16}}
        keyExtractor={(item) => item.id}
        data={data}
        renderItem={({item}) => (
          <components.DiagnosticItem
            title={item.title}
            icon={item.icon}
            version={version}
            boxColor={item.color}
            last={true}
          />
        )}
      />
    </View>
  );
};

export default DiagnosticsAndTests;
