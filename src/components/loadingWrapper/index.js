import React, {useImperativeHandle, useState, memo, forwardRef} from 'react';
import ProgressLoader from './progressLoader';
import {theme} from '../../constants';

const LoadingWrapper = (props, ref) => {
  const [isLoading, setLoading] = useState(false);

  useImperativeHandle(ref, () => ({
    showLoading() {
      setLoading(true);
    },
    hideLoading() {
      setLoading(false);
    },
  }));

  return (
    <>
      <ProgressLoader
        visible={isLoading}
        isModal={true}
        color={theme.colors.white}
      />
    </>
  );
};

export default memo(forwardRef(LoadingWrapper), () => true);
