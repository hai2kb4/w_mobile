import React, {forwardRef, useImperativeHandle, useRef, memo} from 'react';
import {ActivityIndicator, Animated, StyleSheet} from 'react-native';

let duration = 100;
const LoadingView = (props, ref) => {
  useImperativeHandle(ref, () => ({
    toggleLoading() {
      toggleModal();
    },
    showModal() {
      isOpening.current = true;
      Animated.spring(stateY, {
        toValue: 100,
        duration: duration,
        useNativeDriver: true,
      }).start();
    },
    hideLoading() {
      isOpening.current = false;
      Animated.spring(stateY, {
        toValue: -100,
        duration: duration,
        useNativeDriver: true,
      }).start();
    },
  }));

  const stateY = new Animated.Value(-100);
  const isOpening = useRef(false);

  const toggleModal = () => {
    isOpening.current = !isOpening.current;

    if (isOpening.current) {
      Animated.spring(stateY, {
        toValue: 100,
        duration: duration,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.spring(stateY, {
        toValue: -10,
        duration: duration,
        useNativeDriver: true,
      }).start();
    }
  };

  return (
    <Animated.View
      style={[styles.productContainer, {transform: [{translateY: stateY}]}]}
    >
      <ActivityIndicator color='red' animating size='large' />
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  productContainer: {
    backgroundColor: 'red',
    flex: 1,
    minHeight: -10,
    position: 'absolute',
    width: '100%',
    zIndex: 9999,
  },
});
export default memo(forwardRef(LoadingView), () => true);
