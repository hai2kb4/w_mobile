import {Text, TouchableOpacity} from 'react-native';
import React from 'react';

import {theme} from '../constants';

const GenderButton = ({
  gender,
  onPress,
  backgroundColor,
  textColor,
  containerStyle,
}) => {
  return (
    <TouchableOpacity
      style={{
        paddingHorizontal: 12,
        paddingVertical: 8,
        borderWidth: 1,
        ...containerStyle,
      }}
      onPress={onPress}
    >
      <Text
        style={{
          color: textColor,
          ...theme.fonts.SFProText_600SemiBold,
          textTransform: 'uppercase',
        }}
      >
        {gender}
      </Text>
    </TouchableOpacity>
  );
};

export default GenderButton;
