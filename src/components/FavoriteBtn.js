import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {svg} from '../assets';
import {theme} from '../constants';
import {success} from '../utils/flashMessage';
import {authSelector} from '../store/slices/authSlice';
import {useNavigation} from '@react-navigation/native';
import {components} from '.';
import {SCREEN_NAME} from '../navigation/ScreenName';

const FavoriteBtn = ({data, color}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {profile} = useSelector(authSelector);
  const [like, setLike] = useState(false);
  useEffect(() => {
    setLike(data?.isFavorite);
  }, [data]);
  return (
    <components.Touchable
      containerStyle={{
        ...theme.styles.rowJustifyContentSpaceAround,
        width: 48,
        height: 48,
      }}
      onPress={() => {
        if (profile.isGuest) {
          return navigation.navigate(SCREEN_NAME.SignIn);
        }
        const payload = {
          objectId: data.id,
          type: data.type,
          isFavorite: !like,
          onSuccess: () => {
            let message = `Đã thêm ${data.name} vào danh sách quan tâm!`;
            if (like) {
              message = `Đã xóa ${data.name} trong danh sách quan tâm!`;
            }
            success(message, 'Thành công');
            setLike(!like);
          },
        };
      }}
      content={
        <svg.Heart1Svg
          color={like ? theme.colors.red : theme.colors.black}
          fill={like ? theme.colors.red : 'transparent'}
        />
      }
    />
  );
};

export default FavoriteBtn;
