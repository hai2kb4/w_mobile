import React from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const KeyboardAwareScroll = ({children, other}) => {
  return (
    <KeyboardAwareScrollView
      enableOnAndroid
      keyboardShouldPersistTaps='handled'
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{flexGrow: 1, paddingHorizontal: 16}}
      {...other}
    >
      {children}
    </KeyboardAwareScrollView>
  );
};

export default KeyboardAwareScroll;
