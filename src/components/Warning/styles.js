import {StyleSheet} from 'react-native';
import {theme} from '../../constants';

const styles = StyleSheet.create({
  txtSuccess: {
    paddingVertical: 5,
    color: theme.colors.white,
    backgroundColor: '#4caf50',
    textAlign: 'center',
  },
  txtError: {
    paddingVertical: 5,
    color: theme.colors.white,
    backgroundColor: '#f44336',
    textAlign: 'center',
  },
});

export default styles;
