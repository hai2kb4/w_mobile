import React, {useLayoutEffect, memo, useState, useRef, useEffect} from 'react';
import {useNetInfo} from '@react-native-community/netinfo';
import styles from './styles';
import {Text} from 'react-native';
import {NetworkServiceUtil} from '../../utils/networkService';

const getNetworkString = (netInfo) => {
  let result = 'Kết nối mạng lỗi!';

  if (netInfo.isConnected) {
    result = netInfo.isInternetReachable
      ? 'Kết nối thành công'
      : 'Kết nối mạng lỗi!';
  }

  return result;
};

let netInfoTimeOut = null;

const NetworkState = ({onNetWorkStateFunc}) => {
  const netInfo = useNetInfo();

  const [visibleNetwork, setVisibleNetwork] = useState(true);
  const availableNetWordRef = useRef(true);

  useEffect(() => {
    return () => {
      if (netInfoTimeOut) {
        clearTimeout(netInfoTimeOut);
        netInfoTimeOut = null;
      }
    };
  }, []);

  useLayoutEffect(() => {
    netInfoTimeOut = setTimeout(() => {
      const hasInternet = netInfo.isConnected && netInfo.isInternetReachable;
      onNetWorkStateFunc && onNetWorkStateFunc({netWorkState: hasInternet});
      NetworkServiceUtil.setNetworkState(netInfo.isConnected);

      availableNetWordRef.current = hasInternet;
      setVisibleNetwork(hasInternet);
    }, 1000);
  }, [onNetWorkStateFunc, netInfo.isConnected, netInfo.isInternetReachable]);

  if (netInfo.isConnected && netInfo.isInternetReachable) {
    return null;
  }

  if (netInfo.type === 'unknown' || visibleNetwork) {
    return null;
  }

  return (
    <>
      {!visibleNetwork &&
        (!netInfo.isConnected || !netInfo.isInternetReachable) && (
          <Text
            style={[
              netInfo.isConnected && netInfo.isInternetReachable
                ? styles.txtSuccess
                : styles.txtError,
            ]}
          >
            {getNetworkString(netInfo)}
          </Text>
        )}
    </>
  );
};

export default memo(NetworkState, () => true);
