import {Text} from 'react-native';
import React from 'react';
import {theme} from '../../constants';
import {components} from '..';

const TextButton = ({title, containerStyle, onPress, disabled}) => {
  return (
    <components.Touchable
      onPress={onPress}
      disabled={disabled}
      content={
        <Text
          style={{
            ...theme.fonts.textStyle16,
            color: theme.colors.white,
            ...containerStyle,
          }}
        >
          {title}
        </Text>
      }
    />
  );
};

export default TextButton;
