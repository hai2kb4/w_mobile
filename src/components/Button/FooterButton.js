import {Text, ActivityIndicator} from 'react-native';
import React from 'react';
import {components} from '..';
import {theme} from '../../constants';

const FooterButton = ({
  text,
  bgColor,
  onPress,
  loading = false,
  titleColor,
  containerStyle,
}) => {
  return (
    <components.Touchable
      containerStyle={{
        width: '48%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: bgColor,
        flexDirection: 'row',
        borderRadius: 6,
        ...containerStyle,
      }}
      onPress={onPress}
      content={
        <>
          {loading && <ActivityIndicator color={theme.colors.stroke} />}
          <Text
            style={{
              color: titleColor || theme.colors.white,
              ...theme.fonts.SFProText_700Bold,
              fontSize: 14,
              lineHeight: 14 * 1.7,
            }}
          >
            {text}
          </Text>
        </>
      }
    />
  );
};

export default FooterButton;
