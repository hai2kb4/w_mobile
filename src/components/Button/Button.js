import {ActivityIndicator, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {theme} from '../../constants';

const Button = ({
  title,
  containerStyle,
  onPress,
  disabled = false,
  loading = false,
  titleStyle,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      disabled={disabled}
      style={{
        backgroundColor: disabled ? theme.colors.stroke : theme.colors.primary,
        height: 36,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 6,
        ...containerStyle,
      }}
      onPress={onPress}
    >
      {loading && (
        <ActivityIndicator
          color={theme.colors.stroke}
          style={{marginRight: 5}}
        />
      )}
      <Text
        style={{
          ...theme.fonts.SFProText_700Bold,
          ...theme.fonts.textStyle14,
          color: theme.colors.white,
          ...titleStyle,
        }}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default Button;
