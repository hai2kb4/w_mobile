import React from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import {theme} from '../../constants';
export default function RadioButton({
  selectValue,
  label,
  onPress,
  value,
  containerStyle,
  textStyle,
  version = 'v1',
}) {
  const v1 = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={{
          ...styles.container,
          ...containerStyle,
          borderColor:
            value === selectValue ? theme.colors.primary : theme.colors.white,
        }}
        onPress={onPress}
      >
        <View
          style={{
            ...styles.radioCircle,
            borderColor:
              value === selectValue
                ? theme.colors.primary
                : theme.colors.stroke,
          }}
        >
          {value === selectValue && <View style={styles.selectedRb} />}
        </View>
        <Text
          style={{
            color:
              value === selectValue
                ? theme.colors.primary
                : theme.colors.darkBlue,
            ...theme.fonts.textStyle13,
            ...textStyle,
          }}
        >
          {label}
        </Text>
      </TouchableOpacity>
    );
  };

  const v2 = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={{
          ...styles.container,
          borderWidth: 0,
          backgroundColor: theme.colors.stroke,
          justifyContent: 'space-between',
          marginBottom: 4,
          ...containerStyle,
        }}
        onPress={onPress}
      >
        <Text
          style={{
            ...theme.fonts.textStyle16,
            ...textStyle,
          }}
        >
          {label}
        </Text>
        <View
          style={{
            ...styles.radioCircle,
            borderColor:
              value === selectValue
                ? theme.colors.primary
                : theme.colors.textColor,
          }}
        >
          {value === selectValue && <View style={styles.selectedRb} />}
        </View>
      </TouchableOpacity>
    );
  };

  return version === 'v1' ? v1() : v2();
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 12,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 16,
    borderRadius: 6,
  },
  radioCircle: {
    height: 16,
    width: 16,
    borderRadius: 100,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginEnd: 12,
  },
  selectedRb: {
    width: 8,
    height: 8,
    borderRadius: 50,
    backgroundColor: theme.colors.primary,
  },
});
