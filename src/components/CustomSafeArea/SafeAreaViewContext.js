import React from 'react';
import {theme} from '../../constants';
import DismissKeyboard from '../DismissKeyboard';
import {SafeAreaView} from 'react-native-safe-area-context';

export default function SafeAreaViewContext({headerColor, content}) {
  return (
    <>
      <SafeAreaView
        edges={['top']}
        style={{flex: 0, backgroundColor: headerColor}}
      />
      <SafeAreaView
        edges={['right', 'bottom', 'left']}
        style={{
          flex: 1,
          backgroundColor: theme.colors.white,
        }}
      >
        <DismissKeyboard>{content}</DismissKeyboard>
      </SafeAreaView>
    </>
  );
}
