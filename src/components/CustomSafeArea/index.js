import React, {Fragment} from 'react';
import {theme} from '../../constants';
import ProgressLoader from '../loadingWrapper/progressLoader';
import DismissKeyboard from '../DismissKeyboard';
import SafeAreaView from 'react-native-safe-area-view';
import SafeAreaViewContext from './SafeAreaViewContext';

const CustomSafeArea = ({
  children,
  isLoading = false,
  isContext = true,
  headerColor = theme.colors.backgroundColor,
}) => {
  return (
    <Fragment>
      {isContext ? (
        <SafeAreaViewContext headerColor={headerColor} content={children} />
      ) : (
        <SafeAreaView
          edges={['right', 'bottom', 'left']}
          style={{
            flex: 1,
            backgroundColor: headerColor,
          }}
        >
          <DismissKeyboard>{children}</DismissKeyboard>
        </SafeAreaView>
      )}
      <ProgressLoader visible={isLoading} />
    </Fragment>
  );
};

export default CustomSafeArea;
