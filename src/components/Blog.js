import React, {memo} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {theme} from '../constants';
import {components} from '.';
import {useSelector} from 'react-redux';
import moment from 'moment';
import {accountImage} from './BurgerMenuModal';
import {useNavigation} from '@react-navigation/native';
import {SCREEN_NAME} from '../navigation/ScreenName';
const Blog = () => {
  const navigation = useNavigation();

  const blogList = [];

  const renderItem = ({item, index}) => {
    return (
      <components.Touchable
        key={index}
        onPress={() => navigation.navigate(SCREEN_NAME.BlogDetail, {item})}
        containerStyle={{
          padding: 12,
          borderWidth: 1,
          borderColor: theme.colors.stroke,
          borderRadius: 6,
          width:
            blogList.slice(1).length === 1
              ? theme.sizes.width - 32
              : theme.sizes.width * 0.8,
          marginLeft: 16,
          marginRight: blogList.slice(1).length === index + 1 ? 16 : 0,
        }}
        content={
          <>
            <View
              style={{
                flexDirection: 'row',
                marginBottom: 4,
              }}
            >
              <Text
                numberOfLines={3}
                style={{
                  width:
                    blogList.slice(1).length === 1
                      ? theme.sizes.width - 125
                      : theme.sizes.width * 0.8 - 95,
                  color: theme.colors.black,
                  ...theme.fonts.textStyle13,
                  marginRight: 8,
                }}
              >
                {item.shortDescription}
              </Text>
              <components.ImageLoader
                imageUrl={item.avatarUrl}
                style={styles.imageItem}
                resizeMode='cover'
              />
            </View>
            {personNameCreate(item)}
          </>
        }
      />
    );
  };

  const personNameCreate = (item) => {
    return (
      <View style={theme.styles.rowJustifyContentSpaceBetween}>
        <View style={theme.styles.rowAlignItemsCenter}>
          <components.ImageLoader
            imageUrl={item.avatarUserUrl}
            source={accountImage}
            style={styles.avatar}
          />
          <Text
            style={{
              ...theme.fonts.textStyle13,
              marginLeft: 8,
            }}
          >
            {item.createdByName}
          </Text>
        </View>
        <Text style={{...theme.fonts.textStyle13}}>
          {moment(item.createdTime).fromNow()}
        </Text>
      </View>
    );
  };

  return (
    blogList.length > 0 && (
      <View style={styles.conainer}>
        <components.BlockHeader
          title='Tin tức'
          containerStyle={{margin: 16}}
          viewAll
          onPress={() => navigation.navigate(SCREEN_NAME.ViewAllBlog)}
        />
        <components.Touchable
          onPress={() =>
            navigation.navigate(SCREEN_NAME.BlogDetail, {
              item: blogList[0],
            })
          }
          containerStyle={{
            paddingHorizontal: 16,
            paddingBottom: 16,
          }}
          content={
            <>
              <components.ImageLoader
                imageUrl={blogList[0].avatarUrl}
                style={styles.img}
                resizeMode='cover'
              />
              <Text
                style={{
                  ...theme.fonts.textStyle16,
                  paddingVertical: 8,
                }}
              >
                {blogList[0].name}
              </Text>
              {personNameCreate(blogList[0])}
            </>
          }
        />
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal
          data={blogList.slice(1)}
          renderItem={renderItem}
        />
      </View>
    )
  );
};

export default memo(Blog);

const styles = StyleSheet.create({
  conainer: {
    backgroundColor: theme.colors.white,
    marginTop: 8,
    paddingBottom: 16,
  },
  img: {
    width: theme.sizes.width - 32,
    height: theme.sizes.width / 2,
    borderRadius: 12,
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 100,
  },
  imageItem: {
    width: 60,
    height: 60,
    borderRadius: 4,
  },
});
