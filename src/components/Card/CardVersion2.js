import React, {memo} from 'react';
import {Text, View} from 'react-native';
import {components} from '../../components';
import {theme} from '../../constants';
import {svg} from '../../assets';
import {formatDate} from '../../utils';
import {SCREEN_NAME} from '../../navigation/ScreenName';

const v2 = (props) => {
  const {array, index, navigation, item, screenName, isGuest = false} = props;
  return (
    <components.Touchable
      onPress={() => navigation.navigate(screenName, {id: item.id})}
      containerStyle={{
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: array.length - 1 === index ? 0 : 12,
        borderWidth: 1,
        paddingBottom: 14,
        borderColor: theme.colors.stroke,
        padding: 12,
        backgroundColor: theme.colors.white,
      }}
      content={
        <>
          <View style={{width: 100, height: 100, marginRight: 14}}>
            <components.ImageLoader
              imageUrl={item.avatarUrl}
              style={{
                width: '100%',
                height: '100%',
                backgroundColor: '#F6F8FB',
              }}
              resizeMode='cover'
            />
            <View
              style={{
                backgroundColor: theme.colors.white,
                paddingHorizontal: 6,
                paddingVertical: 2,
                flexDirection: 'row',
                alignItems: 'center',
                position: 'absolute',
                margin: 4,
                bottom: 0,
                left: 0,
              }}
            >
              <svg.DoctorRatingStar />
              <Text
                style={{
                  ...theme.fonts.textStyle13,
                  color: theme.colors.textColor,
                  marginLeft: 4,
                }}
              >
                {item.rating?.toFixed(1)}
              </Text>
            </View>
          </View>
          <View style={{flex: 1}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 2,
                justifyContent: 'space-between',
              }}
            >
              <Text
                style={{
                  ...theme.fonts.textStyle14,
                }}
              >
                {item.name}
              </Text>
              <components.FavoriteBtnSmall
                data={item}
                color={theme.colors.primary}
              />
            </View>
            <Text
              style={{
                ...theme.fonts.textStyle13,
                marginBottom: 2,
              }}
            >
              CK: {item.specialityName}
            </Text>
            <Text
              style={{
                ...theme.fonts.textStyle13,
                flex: 1,
              }}
            >
              Đ/c: {item.address}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <svg.ClockSvg />
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    marginLeft: 4,
                    color: theme.colors.textColor,
                  }}
                  numberOfLines={1}
                >
                  {formatDate(item.startTime, true) +
                    ' - ' +
                    formatDate(item.endtime, true)}
                </Text>
              </View>
              <components.Button
                onPress={() =>
                  isGuest
                    ? navigation.navigate(SCREEN_NAME.SignIn)
                    : navigation.navigate(SCREEN_NAME.MakeAnAppointment, {
                        item,
                        isHospital: false,
                      })
                }
                title='Đặt lịch khám'
                containerStyle={{width: 110}}
              />
            </View>
          </View>
        </>
      }
    />
  );
};

export default memo(v2);
