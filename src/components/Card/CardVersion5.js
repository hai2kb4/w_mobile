import React, {memo} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {components} from '..';
import {theme} from '../../constants';
import {svg} from '../../assets';

const v5 = (props) => {
  const {array, index, item} = props;
  return (
    <View
      style={{
        borderBottomWidth: 1,
        paddingBottom: 20,
        marginBottom: array.length - 1 === index ? 0 : 20,
        borderBottomColor: theme.colors.stroke,
      }}
    >
      <View
        style={{flexDirection: 'row', alignItems: 'center', marginBottom: 7}}
      >
        <View
          style={{
            width: 50,
            height: 50,
            backgroundColor: '#F6F8FB',
            marginRight: 10,
          }}
        >
          <components.ImageLoader
            imageUrl={item.photo}
            style={{width: '100%', height: '100%'}}
            indicatorStyle={{backgroundColor: '#F6F8FB'}}
          />
        </View>
        <View style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <Text
              style={{
                ...theme.fonts.H6,
                color: theme.colors.darkBlue,
                marginBottom: 2,
              }}
            >
              {item.name}
            </Text>
            <Text style={{...theme.fonts.H6, color: theme.colors.darkBlue}}>
              ${item.price}
            </Text>
          </View>
          <Text
            style={{
              ...theme.fonts.SFProText_400Regular,
              fontSize: 12,
              color: theme.colors.textColor,
              lineHeight: 12 * 1.5,
            }}
          >
            {item.speciality}
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <svg.ClockSvg />
          <Text
            style={{
              ...theme.fonts.SFProText_400Regular,
              fontSize: 12,
              color: theme.colors.textColor,
              lineHeight: 12 * 1.7,
              marginLeft: 6,
            }}
          >
            {item.date}
          </Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity
            style={{
              paddingHorizontal: 14,
              paddingVertical: 8,
              backgroundColor: theme.colors.mainGreen,
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 10,
            }}
          >
            <Text
              style={{
                color: theme.colors.white,
                textTransform: 'capitalize',
                ...theme.fonts.SFProText_700Bold,
                fontSize: 10,
              }}
            >
              Lieve review
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              paddingHorizontal: 14,
              paddingVertical: 8,
              backgroundColor: theme.colors.darkBlue,
              justifyContent: 'center',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <svg.DownloadSvg />
            <Text
              style={{
                color: theme.colors.white,
                textTransform: 'capitalize',
                ...theme.fonts.SFProText_700Bold,
                fontSize: 10,
                marginLeft: 4,
              }}
            >
              Prescription
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default memo(v5);
