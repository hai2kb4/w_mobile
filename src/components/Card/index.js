import React from 'react';
import CardVersion1 from './CardVersion1';
import CardVersion2 from './CardVersion2';

const CardItem = ({
  version,
  item,
  array,
  index,
  navigation,
  screenName,
  isFetched = true,
}) => {
  return version === 'v2' ? (
    <CardVersion2
      item={item}
      array={array}
      index={index}
      navigation={navigation}
      screenName={screenName}
      isFetched={isFetched}
    />
  ) : (
    <CardVersion1
      item={item}
      array={array}
      index={index}
      navigation={navigation}
      screenName={screenName}
      isFetched={isFetched}
    />
  );
};

export default CardItem;
