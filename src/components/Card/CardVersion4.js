import React, {memo} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {components} from '..';
import {theme} from '../../constants';
import {svg} from '../../assets';
import {SCREEN_NAME} from '../../navigation/ScreenName';

const v3 = (props) => {
  const {array, index, navigation, item, screenName} = props;
  return (
    <View
      style={{
        flexDirection: 'row',
        marginBottom: array.length - 1 === index ? 0 : 14,
        borderBottomWidth: 1,
        paddingBottom: 14,
        borderBottomColor: theme.colors.stroke,
      }}
      s
    >
      <TouchableOpacity
        style={{
          width: 110,
          height: 110,
          marginRight: 14,
          backgroundColor: '#F6F8FB',
        }}
        onPress={() => navigation.navigate(screenName, {item})}
      >
        <components.ImageLoader
          imageUrl={item.photo}
          style={{width: '100%', height: '100%'}}
          resizeMode='contain'
          indicatorStyle={{backgroundColor: '#F6F8FB'}}
        />
        <View
          style={{
            backgroundColor: theme.colors.white,
            paddingHorizontal: 6,
            paddingVertical: 2,
            flexDirection: 'row',
            alignItems: 'center',
            position: 'absolute',
            margin: 4,
            bottom: 0,
            left: 0,
          }}
        >
          <svg.DoctorRatingStar />
          <Text
            style={{
              ...theme.fonts.SFProText_400Regular,
              fontSize: 10,
              color: theme.colors.textColor,
              marginLeft: 4,
            }}
          >
            {item.rating.toFixed(1)}
          </Text>
        </View>
      </TouchableOpacity>
      <View
        style={{
          flex: 1,
          justifyContent: 'space-between',
        }}
      >
        {/* 01 */}
        <View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 2,
              justifyContent: 'space-between',
            }}
          >
            <Text
              style={{
                ...theme.fonts.H6,
                color: theme.colors.darkBlue,
              }}
            >
              {item.name}
            </Text>
            <components.FavoriteBtnSmall doctor={item} />
          </View>
          <Text
            style={{
              ...theme.fonts.SFProText_400Regular,
              color: theme.colors.textColor,
              fontSize: 12,
              lineHeight: 12 * 1.5,
            }}
          >
            {item.speciality}
          </Text>
        </View>
        {/* 02 */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: 6,
          }}
        >
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <svg.ClockSvg />
            <Text
              style={{
                fontSize: 10,
                marginLeft: 4,
                color: theme.colors.textColor,
              }}
              numberOfLines={1}
            >
              {item.availableTime[0] +
                ' - ' +
                item.availableTime[item.availableTime.length - 1]}
            </Text>
          </View>
          <Text
            style={{
              ...theme.fonts.SFProText_600SemiBold,
              fontSize: 12,
              color: theme.colors.darkBlue,
              lineHeight: 12 * 1.5,
            }}
          >
            ${item.price}
          </Text>
        </View>
        {/* 03 */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <TouchableOpacity
            style={{
              backgroundColor: theme.colors.mainGreen,
              width: '48%',
              height: 33,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text
              style={{
                color: theme.colors.white,
                textTransform: 'capitalize',
                fontSize: 10,
                lineHeight: 10 * 1.7,
              }}
            >
              send message
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: theme.colors.darkBlue,
              width: '48%',
              height: 33,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() =>
              navigation.navigate(SCREEN_NAME.MakeAnAppointment, {item})
            }
          >
            <Text
              style={{
                color: theme.colors.white,
                textTransform: 'capitalize',
                fontSize: 10,
                lineHeight: 10 * 1.7,
              }}
            >
              appointment
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default memo(v3);
