import React, {memo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {components} from '../../components';
import {theme} from '../../constants';
import {svg} from '../../assets';
import {formatDate} from '../../utils';

const widthItem = 130;

const v1 = (props) => {
  const {array, index, navigation, item, screenName, isFetched} = props;

  return isFetched ? (
    <View style={{marginBottom: 16}}>
      <components.Touchable
        containerStyle={{
          ...styles.container,
          marginRight: array.length - 1 === index ? 20 : 14,
        }}
        onPress={() => navigation.navigate(screenName, {id: item.id})}
        content={
          <>
            <components.ImageLoader
              imageUrl={item.avatarUrl}
              style={styles.image}
              resizeMode='cover'
            />

            <View
              style={{
                flex: 1,
                justifyContent: 'space-between',
                paddingLeft: 4,
                paddingTop: 5,
                paddingBottom: 4,
                paddingRight: 10,
              }}
            >
              <View style={theme.styles.rowJustifyContentSpaceBetween}>
                <View
                  style={{
                    backgroundColor: theme.colors.white,
                    paddingHorizontal: 6,
                    paddingVertical: 2,
                    ...theme.styles.rowAlignItemsCenter,
                  }}
                >
                  <svg.DoctorRatingStar />
                  <Text
                    style={{
                      ...theme.fonts.SFProText_400Regular,
                      fontSize: 10,
                      color: theme.colors.textColor,
                      marginLeft: 4,
                    }}
                  >
                    {item.rating?.toFixed(1)}
                  </Text>
                </View>
                <components.FavoriteBtnSmall data={item} />
              </View>
              <Text
                style={{
                  ...theme.fonts.SFProText_400Regular,
                  fontSize: 10,
                  color: theme.colors.textColor,
                  lineHeight: 10 * 1.7,
                  backgroundColor: theme.colors.white,
                  paddingVertical: 2,
                  paddingHorizontal: 6,
                  alignSelf: 'flex-start',
                }}
              >
                {`${formatDate(item.startTime, true)} - ${formatDate(
                  item.endTime,
                  true,
                )}`}
              </Text>
            </View>
          </>
        }
      />

      <Text
        numberOfLines={2}
        style={{
          ...theme.fonts.textStyle13,
          marginBottom: 2,
          width: widthItem,
        }}
      >
        {item.name}
      </Text>

      <Text
        numberOfLines={2}
        style={{
          ...theme.fonts.SFProText_400Regular,
          color: theme.colors.textColor,
          fontSize: 12,
          lineHeight: 12 * 1.5,
          width: widthItem,
        }}
      >
        {item.address}
      </Text>
    </View>
  ) : (
    <View>
      <components.CustomShimmerPlaceHolder containerStyle={styles.container} />
      <components.CustomShimmerPlaceHolder
        containerStyle={{
          marginBottom: 2,
          width: widthItem,
        }}
      />
      <components.CustomShimmerPlaceHolder
        containerStyle={{
          width: widthItem - 32,
          marginBottom: 16,
        }}
      />
    </View>
  );
};

export default memo(v1);

const styles = StyleSheet.create({
  container: {
    width: widthItem,
    height: 162,
    backgroundColor: '#F6F8FB',
    marginRight: 14,
    marginBottom: 12,
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
});
