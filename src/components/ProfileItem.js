import {View, Text} from 'react-native';
import React from 'react';

import {svg} from '../assets';
import {theme} from '../constants';
import {components} from './index';

const ProfileItem = ({title, onPress}) => {
  return (
    <components.Touchable
      onPress={onPress}
      containerStyle={{
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: theme.colors.placeholderTextColor,
      }}
      content={
        <>
          {title === 'Thông tin cá nhân' && (
            <svg.UserSvg color={theme.colors.white} />
          )}
          {title === 'Danh sách quan tâm' && <svg.Heart1Svg />}
          {title === 'QR của tôi' && (
            <svg.QRCodeSvg color={theme.colors.black} />
          )}
          {title === 'Địa chỉ quản lý' && <svg.AddressSvg />}
          {title === 'Đăng xuất' && <svg.LogOutSvg />}
          {title === 'Chia sẻ ứng dụng' && <svg.ShareSvg />}
          {title === 'Trung tâm trợ giúp' && <svg.HeadsetSvg />}
          {title === 'Điều khoản và quy định' && <svg.RecipeSvg />}
          {title === 'Cài đặt chung' && (
            <svg.SettingSvg color={theme.colors.white} />
          )}
          {title === 'Liên hệ & hỗ trợ' && <svg.HeadsetSvg />}
          <Text
            style={{
              marginLeft: 14,
              ...theme.fonts.H6,
              color: theme.colors.white,
            }}
          >
            {title}
          </Text>
          {onPress && !['Đăng xuất', 'Liên hệ & hỗ trợ'].includes(title) && (
            <View style={{marginLeft: 'auto'}}>
              <svg.Arrow version={'v1'} />
            </View>
          )}
        </>
      }
    />
  );
};

export default ProfileItem;
