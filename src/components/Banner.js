import {StyleSheet, Platform, View, Linking} from 'react-native';
import React, {memo} from 'react';
import {theme} from '../constants';
import {useSelector} from 'react-redux';
import {components} from '../components';
import {useNavigation} from '@react-navigation/native';
import Carousel, {ParallaxImage} from './Carousel';
import {SCREEN_NAME} from '../navigation/ScreenName';

const ITEM_HEIGHT = (theme.sizes.width - 60) / 2;
const ITEM_WIDTH = theme.sizes.width - 60;
const bannerType = {
  linkUrl: 1,
  popup: 2,
  hospital: 3,
  doctor: 4,
};

const Banner = () => {
  const navigation = useNavigation();

  const renderItem = ({item, index}, parallaxProps) => {
    return (
      <components.Touchable
        key={index}
        activeOpacity={1}
        onPress={() => {
          switch (item.type) {
            case bannerType.linkUrl:
              Linking.openURL(item.linkUrl);
              break;
            case bannerType.popup:
              navigation.navigate(SCREEN_NAME.BannerDetail, {item});
              break;
            case bannerType.hospital:
              navigation.navigate(SCREEN_NAME.HospitalDetails, {
                id: item.hospitalById,
              });
              break;
            case bannerType.doctor:
              navigation.navigate(SCREEN_NAME.DoctorDetails, {
                id: item.doctorById,
              });
              break;
            default:
              break;
          }
        }}
        containerStyle={styles.item}
        content={
          <ParallaxImage
            source={{uri: item.bannerUrl.replace('http:', 'https:')}}
            containerStyle={styles.imageContainer}
            style={styles.image}
            parallaxFactor={0.4}
            {...parallaxProps}
          />
        }
      />
    );
  };

  return (
    <View style={{marginVertical: 12}}>
      <Carousel
        autoplay
        loop
        sliderWidth={theme.sizes.width}
        itemWidth={ITEM_WIDTH}
        data={[]}
        renderItem={renderItem}
        hasParallaxImages={true}
      />
    </View>
  );
};

export default memo(Banner);

const styles = StyleSheet.create({
  item: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
  },
  imageContainer: {
    flex: 1,
    marginBottom: Platform.select({ios: 0, android: 1}),
    backgroundColor: theme.colors.stroke,
    borderRadius: 8,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    height: ITEM_HEIGHT,
    width: ITEM_WIDTH,
    resizeMode: 'contain',
  },
});
