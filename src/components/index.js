import CustomSafeArea from './CustomSafeArea';
import Header from './Header';
import ImageLoader from './ImageLoader';
import MyAppointments from './MyAppointments';
import DiagnosticsAndTests from './DiagnosticsAndTests';
import Banner from './Banner';
import TopRateItems from './TopRateItems';
import Categories from './Categories';
import BlockHeader from './BlockHeader';
import DiagnosticItem from './DiagnosticItem';
import Button from './Button/Button';
import InputField from './InputField';
import FavoriteBtn from './FavoriteBtn';
import ReviewItem from './ReviewItem';
import FavoriteBtnSmall from './FavoriteBtnSmall';
import FooterButton from './Button/FooterButton';
import GenderButton from './GenderButton';
import BurgerMenuModal from './BurgerMenuModal';
import BurgerMenuItem from './BurgerMenuItem';
import ProfileItem from './ProfileItem';
import CheckBox from './CheckBox';
import TestsDiagnosticsItem from './TestsDiagnosticsItem';
import SquareCard from './SquareCard';
import TextButton from './Button/TextButton';
import DatePickerModal from './DatePickerModal';
import CardItem from './Card';
import ExaminationBySpecialty from './ExaminationBySpecialty';
import Blog from './Blog';
import RadioButton from './Button/RadioButton';
import FlatListLayout from './FlatListLayout';
import CustomViewModal from './Modal/CustomViewModal';
import Touchable from './Touchable';
import CalendarPicker from './CalendarPicker';
import ScrollViewLayout from './ScrollViewLayout';
import QRCodeScanner from './qrCodeScanner';
import QRScannerRectView from './qrCodeScanner/qrScannerRectView';
import CustomTabs from './CustomTabs';
import FloatingAction from './FloatingAction';
import ProgressLoader from './loadingWrapper/progressLoader';
import SearchBar from './SearchBar';
import AvailableItem from './AvailableItem';
import CustomShimmerPlaceHolder from './CustomShimmerPlaceHolder';
import NotificationBadge from './NotificationBadge';
import ExpertDoctors from './ExpertDoctors';
import DismissKeyboard from './DismissKeyboard';
import KeyboardAwareScroll from './KeyboardAvoidingView/KeyboardAwareScroll';
import KeyboardAvoidingView from './KeyboardAvoidingView';

const allComponents = {
  KeyboardAvoidingView,
  KeyboardAwareScroll,
  FloatingAction,
  ProgressLoader,
  CustomShimmerPlaceHolder,
  AvailableItem,
  SearchBar,
  NotificationBadge,
  ExpertDoctors,
  CustomTabs,
  QRScannerRectView,
  QRCodeScanner,
  ScrollViewLayout,
  CalendarPicker,
  Touchable,
  CustomViewModal,
  FlatListLayout,
  RadioButton,
  Blog,
  CustomSafeArea,
  Header,
  ImageLoader,
  MyAppointments,
  DiagnosticsAndTests,
  Banner,
  TopRateItems,
  Categories,
  BlockHeader,
  DiagnosticItem,
  Button,
  InputField,
  FavoriteBtn,
  ReviewItem,
  FavoriteBtnSmall,
  FooterButton,
  GenderButton,
  BurgerMenuModal,
  BurgerMenuItem,
  ProfileItem,
  CheckBox,
  TestsDiagnosticsItem,
  SquareCard,
  TextButton,
  DatePickerModal,
  CardItem,
  ExaminationBySpecialty,
  DismissKeyboard,
};

const components = Object.assign({}, allComponents);

export {components};
