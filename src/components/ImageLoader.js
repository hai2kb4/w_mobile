import {ActivityIndicator, StyleSheet, View} from 'react-native';
import React, {useState, memo} from 'react';

import {theme} from '../constants';
import FastImage from 'react-native-fast-image';
import {isValueEmpty} from '../utils';

const ImageLoader = (
  {
    imageUrl = '',
    source = '',
    style,
    resizeMode = FastImage.resizeMode.contain,
    indicatorStyle,
  },
  props,
) => {
  const [loading, setLoading] = useState(false);

  const onloading = (value) => {
    setLoading(value);
  };

  const renderActivityIndicator = () => {
    return (
      <ActivityIndicator
        size='small'
        color={theme.colors.mainColor}
        style={{
          zIndex: 0,
          opacity: 1,
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          ...indicatorStyle,
        }}
      />
    );
  };

  return (
    <View style={{...styles.container, ...style}}>
      {loading && renderActivityIndicator()}
      <FastImage
        style={{...styles.container, ...style}}
        source={
          !isValueEmpty(imageUrl)
            ? {uri: imageUrl.replace('http:', 'https:')}
            : source
        }
        resizeMode={resizeMode}
        onLoadStart={() => onloading(true)}
        onLoadEnd={() => onloading(false)}
        {...props}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
});

export default memo(ImageLoader);
