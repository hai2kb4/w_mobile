import {View, Text} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {svg} from '../assets';
import {theme} from '../constants';
import {components} from '../components';
import {authSelector} from '../store/slices/authSlice';
import {generateGreetings, guidEmpty} from '../utils';
import {SCREEN_NAME} from '../navigation/ScreenName';
import {accountImage} from './BurgerMenuModal';

const Header = ({
  border,
  user,
  title,
  goBack,
  backHandle,
  textStyle,
  notify,
  color,
  filter,
  customRight,
  filterName = SCREEN_NAME.Filters,
  filterParams = {},
}) => {
  const {profile} = useSelector(authSelector);
  const navigation = useNavigation();

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 52,
        position: 'relative',
        borderBottomWidth: 1,
        borderColor: border ? theme.colors.white : 'transparent',
        backgroundColor: color ?? 'transparent',
      }}
    >
      {title && (
        <Text
          style={{
            textAlign: 'center',
            ...theme.fonts.textStyle16,
            color: color ? theme.colors.white : theme.colors.white,
            ...textStyle,
          }}
          numberOfLines={1}
        >
          {title}
        </Text>
      )}
      {user && (
        <View
          style={{
            left: 0,
            height: 48,
            marginLeft: 20,
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <components.Touchable
            containerStyle={{...theme.styles.rowAlignItemsCenter, height: 48}}
            onPress={() => {
              if (profile && !profile.isGuest) {
                profile.customerId !== guidEmpty &&
                  navigation.navigate(SCREEN_NAME.RelativesProfile, {
                    id: profile.customerId,
                  });
              } else {
                navigation.navigate(SCREEN_NAME.SignIn);
              }
            }}
            content={
              <>
                <components.ImageLoader
                  style={{
                    height: 40,
                    width: 40,
                    borderRadius: 50,
                  }}
                  imageUrl={profile?.avatarUrl}
                  source={accountImage}
                  resizeMode='cover'
                />

                <View style={{marginLeft: 8}}>
                  <Text
                    style={{
                      ...theme.fonts.textStyle13,
                      color: theme.colors.white,
                    }}
                  >
                    {generateGreetings()}
                  </Text>
                  <Text
                    style={{
                      ...theme.fonts.textStyle16,
                      color: theme.colors.white,
                    }}
                  >
                    {profile?.name}
                  </Text>
                </View>
              </>
            }
          />
        </View>
      )}
      {notify && (
        <components.Touchable
          containerStyle={{
            right: 0,
            height: 48,
            width: 64,
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => navigation.navigate(SCREEN_NAME.Notifications)}
          content={<svg.BellSvg color={theme.colors.white} />}
        />
      )}
      {filter && (
        <components.Touchable
          containerStyle={{
            right: 0,
            height: 48,
            width: 48,
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => navigation.navigate(filterName, {...filterParams})}
          content={<svg.FiltersSvg color={theme.colors.white} />}
        />
      )}
      {goBack && (
        <View
          style={{
            position: 'absolute',
            left: 0,
            alignItems: 'center',
          }}
        >
          <components.Touchable
            containerStyle={{
              ...theme.styles.rowJustifyContentSpaceAround,
              width: 48,
              height: 48,
            }}
            onPress={() => (backHandle ? backHandle() : navigation.goBack())}
            content={<svg.GoBackSvg color={theme.colors.white} />}
          />
        </View>
      )}
      {customRight && (
        <View
          style={{
            position: 'absolute',
            right: 0,
            alignItems: 'center',
          }}
        >
          {customRight}
        </View>
      )}
    </View>
  );
};

export default Header;
