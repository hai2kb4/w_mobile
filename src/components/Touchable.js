import React from 'react';
import {TouchableOpacity} from 'react-native';

const Touchable = ({
  content,
  containerStyle,
  onPress,
  activeOpacity = 0.8,
  ...rest
}) => {
  return (
    <TouchableOpacity
      style={containerStyle}
      activeOpacity={activeOpacity}
      onPress={onPress}
      {...rest}
    >
      {content}
    </TouchableOpacity>
  );
};

export default Touchable;
