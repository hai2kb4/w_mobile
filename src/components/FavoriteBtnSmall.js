import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {svg} from '../assets';
import {theme} from '../constants';
import {components} from '.';
import {success} from '../utils/flashMessage';
import {useNavigation} from '@react-navigation/native';
import {authSelector} from '../store/slices/authSlice';
import {SCREEN_NAME} from '../navigation/ScreenName';

const FavoriteBtnSmall = ({data, color}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {profile} = useSelector(authSelector);
  const [like, setLike] = useState(false);
  useEffect(() => {
    setLike(data?.isFavorite);
  }, [data]);
  return (
    <components.Touchable
      onPress={() => {
        if (profile.isGuest) {
          return navigation.navigate(SCREEN_NAME.SignIn);
        }
        const payload = {
          objectId: data.id,
          type: data.type,
          isFavorite: !like,
          onSuccess: () => {
            let message = `Đã thêm ${data.name} vào danh sách quan tâm!`;
            if (like) {
              message = `Đã xóa ${data.name} trong danh sách quan tâm!`;
            }
            success(message, 'Thành công');
            setLike(!like);
          },
        };
      }}
      content={
        <svg.Heart1Svg
          color={like ? theme.colors.red : color || theme.colors.textColor}
          fill={like ? theme.colors.red : 'transparent'}
        />
      }
    />
  );
};

export default FavoriteBtnSmall;
