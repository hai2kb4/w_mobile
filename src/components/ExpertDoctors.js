import React, {memo} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {theme} from '../constants';
import {components} from '.';
import Carousel from './Carousel';
import {useSelector} from 'react-redux';
import {formatDate} from '../utils';
import {useNavigation} from '@react-navigation/native';
import moment from 'moment';
import {authSelector} from '../store/slices/authSlice';
import {SCREEN_NAME} from '../navigation/ScreenName';
import {svg} from '../assets';

const ExpertDoctors = () => {
  const navigation = useNavigation();
  const expertDoctorList = [];
  const {profile} = useSelector(authSelector);
  const renderItem = ({item, index}) => {
    return (
      <components.Touchable
        key={index}
        activeOpacity={1}
        onPress={() =>
          profile.isGuest
            ? navigation.navigate(SCREEN_NAME.SignIn)
            : navigation.navigate(SCREEN_NAME.MakeAnAppointment, {
                item: {...item, isHospital: false},
                dateValue: moment(item.availableDates[0].value).date(),
              })
        }
        containerStyle={styles.item}
        content={
          <View style={theme.styles.rowAlignItemsCenter}>
            <View
              style={{
                width: 80,
                minHeight: 90,
              }}
            >
              <components.ImageLoader
                imageUrl={item.avatarUrl}
                style={styles.image}
                resizeMode='cover'
              />
              <Text style={styles.time}>
                {`${formatDate(item.startTime, true)} - ${formatDate(
                  item.endTime,
                  true,
                )}`}
              </Text>
            </View>

            <View style={{marginLeft: 8, flex: 1}}>
              <Text
                style={{
                  ...theme.fonts.textStyle14,
                  fontWeight: 'bold',
                  flex: 1,
                }}
              >
                {item.name}{' '}
                <Text style={{fontWeight: 'normal'}}>
                  ({item.specialityName})
                </Text>
              </Text>
              <View style={{...theme.styles.rowAlignItemsCenter, marginTop: 4}}>
                <svg.CallSvg width={14} height={15} />
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    marginLeft: 4,
                  }}
                >
                  {item.phone}
                </Text>
              </View>
              <View style={{...theme.styles.rowAlignItemsCenter, marginTop: 4}}>
                <svg.CalendarSvg
                  width={14}
                  height={17}
                  strokeWidth={1.7}
                  color={theme.colors.black}
                />
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    marginLeft: 4,
                    flex: 1,
                  }}
                >
                  {item.dayOfWeek}
                </Text>
              </View>
              <View style={{...theme.styles.rowAlignItemsCenter, marginTop: 4}}>
                <svg.DashboardSvg
                  strokeWidth={1.7}
                  width={15}
                  height={15}
                  color={theme.colors.black}
                />
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    marginLeft: 4,
                    flex: 1,
                  }}
                >
                  {item.hospitalName}
                </Text>
              </View>
              <View style={{...theme.styles.rowAlignItemsCenter, marginTop: 4}}>
                <svg.AddressSvg width={14} height={18} />
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    marginLeft: 4,
                    flex: 1,
                  }}
                >
                  {item.address}
                </Text>
              </View>
            </View>
          </View>
        }
      />
    );
  };
  return (
    expertDoctorList.length > 0 && (
      <View
        style={{
          marginTop: 8,
          paddingBottom: 16,
          backgroundColor: theme.colors.white,
        }}
      >
        <components.BlockHeader
          title={'Bác sỹ chuyên gia'}
          containerStyle={{margin: 16}}
          viewAll={true}
          onPress={() => navigation.navigate(SCREEN_NAME.ScreenExpertDoctors)}
        />
        <Carousel
          autoplay
          loop
          sliderWidth={theme.sizes.width}
          sliderHeight={theme.sizes.width}
          itemWidth={theme.sizes.width - 32}
          data={expertDoctorList}
          renderItem={renderItem}
          hasParallaxImages={true}
        />
      </View>
    )
  );
};

export default memo(ExpertDoctors);

const styles = StyleSheet.create({
  item: {
    width: theme.sizes.width - 32,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: theme.colors.stroke,
    padding: 8,
  },
  imageContainer: {
    flex: 1,
    marginBottom: Platform.select({ios: 0, android: 1}),
    backgroundColor: theme.colors.stroke,
    borderRadius: 8,
  },
  image: {
    width: '100%',
    height: 74,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
  },
  time: {
    ...theme.fonts.SFProText_400Regular,
    fontSize: 10,
    color: theme.colors.textColor,
    backgroundColor: theme.colors.white,
    paddingVertical: 2,
    paddingHorizontal: 6,
    borderWidth: 1,
    borderTopWidth: 0,
    borderColor: theme.colors.stroke,
    textAlign: 'center',
  },
});
