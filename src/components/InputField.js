import {View, TextInput, TouchableOpacity, Text} from 'react-native';
import React from 'react';

import {theme} from '../constants';
import {svg} from '../assets';

const InputField = ({
  title,
  placeholder,
  icon,
  containerStyle,
  secureTextEntry = false,
  keyboardType,
  checkIcon,
  eyeOffIcon = false,
  value,
  onChangeText,
  onPressEyeOffIcon,
  editable = true,
  isRequire = false,
  autoFocus = false,
  ...rest
}) => {
  return (
    <View style={{flexDirection: 'column', ...containerStyle}}>
      {title && (
        <Text style={{...theme.fonts.textStyle14}}>
          {title}
          {isRequire && <Text style={{color: theme.colors.red}}>*</Text>}
        </Text>
      )}
      <View
        style={{
          paddingLeft: 8,
          height: 40,
          width: '100%',
          borderWidth: 1,
          borderColor: theme.colors.stroke,
          justifyContent: 'center',
          flexDirection: 'row',
          alignItems: 'center',
          borderRadius: 4,
          marginTop: 4,
        }}
      >
        <TextInput
          style={{
            flex: 1,
            height: '100%',
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            ...theme.fonts.textStyle14,
          }}
          editable={editable}
          keyboardType={keyboardType}
          placeholder={placeholder}
          placeholderTextColor={theme.colors.placeholderTextColor}
          secureTextEntry={secureTextEntry}
          focusable={true}
          autoFocus={autoFocus}
          value={value}
          onChangeText={onChangeText}
          {...rest}
        />
        {checkIcon && (
          <View style={{paddingHorizontal: 20}}>{<svg.InputCheckSvg />}</View>
        )}
        {eyeOffIcon && (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPressEyeOffIcon}
            style={{paddingHorizontal: 20}}
          >
            <svg.EyeOffSvg isShow={secureTextEntry} />
          </TouchableOpacity>
        )}
        {icon && (
          <TouchableOpacity
            activeOpacity={0.8}
            style={{
              paddingHorizontal: 16,
              paddingVertical: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {icon}
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default InputField;
