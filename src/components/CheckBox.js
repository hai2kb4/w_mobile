import {View, Text} from 'react-native';
import React from 'react';

import {theme} from '../constants';
import {components} from '.';

const CheckBox = ({isChecked, onPress, label}) => {
  return (
    <components.Touchable
      containerStyle={{flexDirection: 'row', alignItems: 'center'}}
      onPress={onPress}
      content={
        <>
          <View
            style={{
              width: 18,
              height: 18,
              borderWidth: 1,
              borderColor: theme.colors.stroke,
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 10,
            }}
          >
            {isChecked && (
              <View
                style={{
                  width: 12,
                  height: 12,
                  backgroundColor: theme.colors.primary,
                }}
              />
            )}
          </View>
          <Text
            style={{
              ...theme.fonts.textStyle13,
              color: theme.colors.darkBlue,
            }}
          >
            {label}
          </Text>
        </>
      }
    />
  );
};

export default CheckBox;
