import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Modalize} from 'react-native-modalize';
import {theme} from '../../constants';
import ImageLoader from '../ImageLoader';
import {svg} from '../../assets';
import {searchType} from '../../constants/constants';
import {components} from '..';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {SCREEN_NAME} from '../../navigation/ScreenName';

export default function ModalExaminationBySoecialty(props) {
  const {modalRef, onBackButtonPress, navigationRef} = props;
  const inset = useSafeAreaInsets();
  const renderItem = ({item}) => {
    return (
      <components.Touchable
        content={
          <>
            <View
              style={{
                borderRadius: 100,
                padding: 5,
              }}
            >
              <ImageLoader source={item.icon} style={{width: 30, height: 30}} />
            </View>
            <Text
              style={{
                width: 62,
                fontSize: 12,
                marginTop: 8,
                textAlign: 'center',
                color: theme.colors.darkBlue,
                ...theme.fonts.SFProText_600SemiBold,
              }}
              numberOfLines={2}
            >
              {item.name}
            </Text>
          </>
        }
        containerStyle={{
          width: theme.sizes.width / 4,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onPress={() => {
          modalRef.current.close();
          navigationRef.current.navigate(SCREEN_NAME.SearchScreen, {
            type: searchType.all,
            speciality: item.id,
          });
        }}
      />
    );
  };

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <View style={{width: 40}} />
        <Text style={styles.title}>Tất cả chuyên khoa</Text>
        <components.Touchable
          style={{width: 40}}
          onPress={onBackButtonPress}
          content={<svg.CloseSvg />}
        />
      </View>
    );
  };

  return (
    <Modalize
      handleStyle={styles.handle}
      ref={modalRef}
      modalTopOffset={inset.top}
      flatListProps={{
        // data: specialityList,
        renderItem: renderItem,
        keyExtractor: (_, index) => index,
        numColumns: 4,
        showsVerticalScrollIndicator: false,
      }}
      HeaderComponent={renderHeader}
    />
  );
}

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 16,
    paddingTop: 16,
    paddingBottom: 6,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.line1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 16,
  },
  title: {
    ...theme.fonts.textStyle13,
    ...theme.fonts.SFProText_600SemiBold,
    color: theme.colors.black,
  },
  handle: {
    backgroundColor: theme.colors.line1,
    top: 28,
  },
});
