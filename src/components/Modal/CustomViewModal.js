import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Modalize} from 'react-native-modalize';
import {theme} from '../../constants';
import {svg} from '../../assets';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

export default function CustomViewModal(props) {
  const {
    title,
    modalRef,
    snapPoint,
    adjustToContentHeight,
    onBackButtonPress,
    showHeader,
    styleModal,
    flatListProps,
    footerComponent,
    headerButon,
    content,
    ...rest
  } = props;
  const inset = useSafeAreaInsets();
  const renderHeader = (
    <View style={styles.header}>
      <View style={{flex: 1}}>{headerButon}</View>
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity
        style={{flex: 1, alignItems: 'flex-end'}}
        onPress={onBackButtonPress}
      >
        <svg.CloseSvg />
      </TouchableOpacity>
    </View>
  );
  return (
    <Modalize
      overlayStyle={{backgroundColor: 'rgba(24, 25, 24, 0.3)'}}
      rootStyle={styleModal}
      handleStyle={styles.handle}
      modalTopOffset={inset.top}
      snapPoint={snapPoint}
      panGestureComponentEnabled={true}
      panGestureEnabled={true}
      adjustToContentHeight={adjustToContentHeight}
      ref={modalRef}
      HeaderComponent={showHeader && renderHeader}
      FooterComponent={footerComponent}
      flatListProps={flatListProps}
      {...rest}
    >
      {content}
    </Modalize>
  );
}

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 16,
    paddingTop: 16,
    paddingBottom: 6,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.line1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    ...theme.fonts.textStyle16,
    color: theme.colors.black,
    textAlign: 'center',
    flex: 2,
  },
  handle: {
    backgroundColor: theme.colors.line1,
    top: 28,
  },
});
