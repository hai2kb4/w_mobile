import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

import {theme} from '../constants';
import {svg} from '../assets';
import {SCREEN_NAME} from '../navigation/ScreenName';

const DiagnosticItem = ({
  icon,
  title,
  description,
  last,
  version,
  boxColor,
  onPress,
}) => {
  const navigation = useNavigation();

  const v1 = () => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          borderBottomWidth: 1,
          borderBottomColor: theme.colors.stroke,
          paddingBottom: 20,
          marginBottom: 16,
          borderRadius: 6,
        }}
        onPress={() =>
          navigation.navigate(SCREEN_NAME.DiagnosticsAndTests, {
            title: title,
          })
        }
      >
        <View
          style={{
            width: theme.sizes.width / 3,
            height: 53,
            backgroundColor: boxColor,
            marginRight: 14,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          {icon}
        </View>
        <View style={{flex: 1, marginRight: 12}}>
          <Text
            style={{
              ...theme.fonts.H6,
              color: theme.colors.darkBlue,
              marginBottom: 2,
            }}
          >
            {title}
          </Text>
          <Text
            style={{
              fontSize: 10,
              ...theme.fonts.SFProText_400Regular,
              lineHeight: 10 * 1.5,
              color: theme.colors.textColor,
            }}
            numberOfLines={2}
          >
            {description}
          </Text>
        </View>
        <svg.DiagnosticArrowSvg />
      </TouchableOpacity>
    );
  };

  const v2 = () => {
    return (
      <TouchableOpacity
        style={{
          width: theme.sizes.width / 3.7,
          height: theme.sizes.width / 3.7,
          backgroundColor: boxColor,
          marginRight: last ? 20 : 14,
          marginBottom: 16,
          paddingHorizontal: 15,
          paddingVertical: 20,
          alignItems: 'center',
          borderRadius: 6,
        }}
        onPress={() =>
          navigation.navigate(SCREEN_NAME.DiagnosticsAndTests, {
            title: title,
          })
        }
      >
        {icon}
        <Text
          style={{
            fontSize: 10,
            marginTop: 8,
            textAlign: 'center',
            color: theme.colors.white,
            ...theme.fonts.SFProText_600SemiBold,
            lineHeight: 10 * 1.3,
          }}
          numberOfLines={2}
        >
          {title}
        </Text>
      </TouchableOpacity>
    );
  };

  return version === 'v1' ? v1() : version === 'v2' ? v2() : null;
};

export default DiagnosticItem;
