import {View, Text} from 'react-native';
import React from 'react';
import {theme} from '../constants';
import {components} from '../components';
import {svg} from '../assets';
import {accountImage} from './BurgerMenuModal';
import moment from 'moment';

const ReviewItem = ({review, index, number, isRating = false}) => {
  return (
    <View
      style={{
        borderBottomWidth: 1,
        marginBottom: 12,
        paddingBottom: 8,
        borderBottomColor:
          number === index + 1 ? theme.colors.white : theme.colors.stroke,
      }}
    >
      <View
        style={{
          ...theme.styles.rowAlignItemsCenter,
          marginBottom: 6,
        }}
      >
        <components.ImageLoader
          imageUrl={review.imageUrl}
          source={accountImage}
          style={{width: 30, height: 30, marginRight: 12, borderRadius: 50}}
          resizeMode='cover'
        />
        <View>
          <View style={theme.styles.rowAlignItemsCenter}>
            <Text style={{...theme.fonts.textStyle13, marginRight: 4}}>
              {review.createdName}
            </Text>
            {review.isTrusted && <svg.VerifySvg />}
          </View>
          <Text
            style={{
              ...theme.fonts.textStyle13,
              fontSize: 12,
            }}
          >
            {moment(review.createdAt).fromNow()}
          </Text>
        </View>
        {isRating && (
          <View
            style={{
              marginLeft: 'auto',
              ...theme.styles.rowAlignItemsCenter,
            }}
          >
            <svg.StarSvg />
            <Text
              style={{
                marginLeft: 4,
                ...theme.fonts.textStyle13,
              }}
            >
              {review.rate?.toFixed(1)}
            </Text>
          </View>
        )}
      </View>
      <Text style={theme.fonts.textStyle13}>{review.content}</Text>
    </View>
  );
};

export default ReviewItem;
