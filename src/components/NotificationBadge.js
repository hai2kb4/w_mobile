import React, {memo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {notificationSelector} from '../store/slices/notificationSlice';
import {theme} from '../constants';
import {svg} from '../assets';

const NotificationBadge = ({focused}) => {
  const {countNotRead} = useSelector(notificationSelector);
  const valueFormat = countNotRead > 9 ? '9+' : String(countNotRead);
  return (
    <View style={styles.container}>
      <svg.BellSvg
        color={focused ? theme.colors.primary : theme.colors.black}
      />
      {countNotRead > 0 && (
        <View style={styles.badgeContainer}>
          <Text style={styles.textStyle}>{valueFormat}</Text>
        </View>
      )}
    </View>
  );
};

export default memo(NotificationBadge);

const styles = StyleSheet.create({
  container: {position: 'relative'},
  badgeContainer: {
    position: 'absolute',
    width: 22,
    height: 18,
    borderRadius: 12,
    marginLeft: 6,
    backgroundColor: '#DF4604',
    borderWidth: 1,
    borderColor: theme.colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 10,
  },
  textStyle: {
    textAlign: 'center',
    fontSize: 10,
    lineHeight: 12,
    color: theme.colors.white,
  },
});
