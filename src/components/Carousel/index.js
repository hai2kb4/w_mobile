import Carousel from './Carousel';
import Pagination from './pagination/Pagination';
import ParallaxImage from './parallaximage/ParallaxImage';
import {getInputRangeFromIndexes} from './utils/animations';

export {
  Carousel as default,
  Pagination,
  ParallaxImage,
  getInputRangeFromIndexes,
};
