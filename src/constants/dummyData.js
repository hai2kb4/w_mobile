const dummyData = {
  taiSanListDefault: [
    {
      id: 1,
      name: 'Số dư khả dụng',
      amount: 0,
      tag: 'Thẻ chi tiêu',
      children: [
        {id: 1, name: 'Thu nhập', amount: 0},
        {id: 2, name: 'Chi tiêu', amount: 0},
      ],
    },
    {
      id: 2,
      name: 'Tài sản ròng',
      amount: 0,
      tag: 'Thẻ tài sản',
      children: [
        {id: 1, name: 'Tài sản', amount: 0},
        {id: 2, name: 'Khoản nợ', amount: 0},
      ],
    },
  ],
};

export {dummyData};
