import {Dimensions, StyleSheet} from 'react-native';

const {width, height} = Dimensions.get('window');

const colors = {
  white: '#FFFFFF',
  black: '#181918',
  textColor: '#4C6780',
  primary: 'rgb(103, 78, 167)',
  darkBlue: '#2D3C49',
  mainGreen: '#149B4F',
  stroke: '#D3E3F1',
  transparent: 'transparent',
  secondary: '#DF3089',
  yellow: '#F5B715',
  red: 'red',
  error: '#ff190c',
  line1: '#E6E9EC',
  line6: '#F4F6F7',
  placeholderTextColor: '#838490',
  backgroundColor: '#17222F',
  gray: '#E5E5E5',
  grey1: '#f5f5f5',
  grey2: '#F3F3F3',
  grey3: '#DADADA',
  grey4: '#BDBDBD',
  grey5: '#8D8D8D',
  grey6: '#616161',
  grey7: '#8C939F',
  grey8: '#F1F3F8',
  grey9: '#C2C2C2',
  grey10: '#f0f0f0',
  grey11: '#A7A7A7',
  grey12: '#e7e7e7',
  grey13: '#899093',
  grey14: '#D9D9D9',
};

const fonts = {
  H1: {
    fontFamily: 'SFProText-Bold',
    fontSize: 32,
    lineHeight: 32 * 1.3,
    color: colors.white,
  },
  H2: {
    fontFamily: 'SFProText-Bold',
    fontSize: 22,
    lineHeight: 22 * 1.4,
    color: colors.white,
  },
  H3: {
    fontFamily: 'SFProText-Bold',
    fontSize: 20,
    lineHeight: 20 * 1.5,
    color: colors.white,
  },
  H4: {
    fontFamily: 'SFProText-Bold',
    fontSize: 16,
    lineHeight: 16 * 1.2,
    color: colors.white,
  },
  H5: {
    fontFamily: 'SFProText-Bold',
    fontSize: 14,
    lineHeight: 14 * 1.2,
    color: colors.white,
  },
  H6: {
    fontFamily: 'SFProText-Semibold',
    fontSize: 14,
    lineHeight: 14 * 1.5,
    color: colors.white,
  },
  SFProText_400Regular: {
    fontFamily: 'SFProText-Regular',
    fontWeight: '400',
  },
  SFProText_600SemiBold: {
    fontFamily: 'SFProText-Semibold',
    fontWeight: '600',
  },
  SFProText_700Bold: {
    fontFamily: 'SFProText-Bold',
    fontWeight: '700',
  },
  textStyle13: {
    color: colors.white,
    fontSize: 13,
    fontFamily: 'SFProText-Regular',
  },
  textStyle14: {
    color: colors.white,
    fontSize: 14,
    fontFamily: 'SFProText-Regular',
  },
  textStyle16: {
    color: colors.white,
    fontSize: 16,
    fontFamily: 'SFProText-Regular',
  },
};

const sizes = {
  width,
  height,
};

const screens = {
  TabNavigator: 'TabNavigator',
  Onboarding: 'Onboarding',
  AddANewAddress: 'AddANewAddress',
  AddANewCard: 'AddANewCard',
  Categories: 'Categories',
  Checkout: 'Checkout',
  ConfirmationCode: 'ConfirmationCode',
  Description: 'Description',
  EditProfile: 'EditProfile',
  Filter: 'Filter',
  ForgotPassword: 'ForgotPassword',
  ForgotPasswordSentEmail: 'ForgotPasswordSentEmail',
  Home: 'Home',
  LeaveAReview: 'LeaveAReview',
  MyAddress: 'MyAddress',
  MyPromocodes: 'MyPromocodes',
  NewPassword: 'NewPassword',
  Notifications: 'Notifications',
  Order: 'Order',
  OrderFailed: 'OrderFailed',
  OrderHistory: 'OrderHistory',
  OrderHistoryOptions: 'OrderHistoryOptions',
  OrderSuccessful: 'OrderSuccessful',
  PaymentMethod: 'PaymentMethod',
  ProductLong: 'ProductLong',
  ProductShort: 'ProductShort',
  Profile: 'Profile',
  Reviews: 'Reviews',
  Shop: 'Shop',
  SignIn: 'SignIn',
  SignUp: 'SignUp',
  SignUpAccountCreated: 'SignUpAccountCreated',
  TrackYourOrder: 'TrackYourOrder',
  VerifyYourPhoneNumber: 'VerifyYourPhoneNumber',
  Wishlist: 'Wishlist',
};

const styles = StyleSheet.create({
  input: {
    paddingLeft: 16,
    height: 48,
    width: '100%',
    borderWidth: 1,
    borderColor: colors.stroke,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 6,
  },
  buttonfooter: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    backgroundColor: colors.white,
    borderTopWidth: 1,
    borderColor: colors.stroke,
  },
  shadow: {
    borderColor: colors.stroke,
    shadowColor: colors.darkBlue,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  shadowTop: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowColor: '#181918',
    elevation: 4,
  },
  line: {
    height: 6,
    width: sizes.width,
    backgroundColor: colors.line6,
  },
  rowAlignItemsCenter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowJustifyContentSpaceBetween: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rowJustifyContentSpaceAround: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  justifyContentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const theme = {
  colors,
  fonts,
  sizes,
  screens,
  styles,
};

export {theme};
