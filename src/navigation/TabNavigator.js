import React from 'react';
import {theme} from '../constants';
import {svg} from '../assets';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {SCREEN_NAME} from './ScreenName';
import {Platform} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {screens} from '../screens';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRoutename={SCREEN_NAME.Home}
      screenOptions={{
        tabBarActiveBackgroundColor: '#17222F',
        tabBarActiveTintColor: theme.colors.white,
        tabBarInactiveTintColor: theme.colors.placeholderTextColor,
        tabBarLabelStyle: {
          fontSize: 12,
          marginBottom: 2,
          fontWeight: '500',
        },
        tabBarInactiveBackgroundColor: '#17222F',
      }}
    >
      <Tab.Screen
        name={SCREEN_NAME.Home}
        options={{
          headerShown: false,
          title: 'Trang chủ',
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({focused}) => (
            <svg.DashboardSvg
              color={
                focused ? theme.colors.white : theme.colors.placeholderTextColor
              }
            />
          ),
        }}
        component={screens.Home}
      />
      <Tab.Screen
        name={SCREEN_NAME.TAISANRONG}
        options={{
          headerShown: false,
          title: 'Ròng tài sản',
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({focused}) => (
            <svg.WalletSvg
              color={
                focused ? theme.colors.white : theme.colors.placeholderTextColor
              }
            />
          ),
        }}
        component={screens.TaiSanRong}
      />
      <Tab.Screen
        name={SCREEN_NAME.TRANSACTION}
        options={{
          title: '',
          headerShown: false,
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({focused}) => (
            <LinearGradient
              colors={['#56B8FE', '#B30FD6']}
              locations={[0, 0.7, 0.5]}
              style={{
                width: Platform.OS === 'ios' ? 40 : 50,
                height: Platform.OS === 'ios' ? 40 : 50,
                top: -15,
                borderRadius: 100,
                ...theme.styles.justifyContentCenter,
              }}
            >
              <svg.PlusSvg
                color={focused ? theme.colors.white : theme.colors.white}
              />
            </LinearGradient>
          ),
        }}
        component={screens.Transaction}
      />
      <Tab.Screen
        name={SCREEN_NAME.EXPERT}
        options={{
          headerShown: false,
          title: 'Chuyên gia',
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({focused}) => (
            <svg.ChatSvg
              version='v1'
              color={
                focused ? theme.colors.white : theme.colors.placeholderTextColor
              }
            />
          ),
        }}
        component={screens.Expert}
      />
      <Tab.Screen
        name={SCREEN_NAME.AccountScreen}
        options={{
          headerShown: false,
          title: 'Tài khoản',
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({focused}) => (
            <svg.UserSvg
              color={
                focused ? theme.colors.white : theme.colors.placeholderTextColor
              }
            />
          ),
        }}
        component={screens.Account}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
