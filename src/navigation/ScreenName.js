export const SCREEN_NAME = {
  Home: 'Home',
  TabNavigator: 'TabNavigator',
  SplashScreen: 'SplashScreen',
  Onboarding: 'Onboarding',
  SignIn: 'SignIn',
  SignInNumber: 'SignInNumber',
  SignInPassword: 'SignInPassword',
  SignUp: 'SignUp',
  AddNameSignUp: 'AddNameSignUp',
  DoctorsList: 'DoctorsList',
  DiagnosticsAndTests: 'DiagnosticsAndTests',
  DoctorDetails: 'DoctorDetails',
  HospitalDetails: 'HospitalDetails',
  ForgotPassword: 'ForgotPassword',
  NewPassword: 'NewPassword',
  ForgotPasswordSentPhone: 'ForgotPasswordSentPhone',
  VerifyYourPhoneNumber: 'VerifyYourPhoneNumber',
  ConfirmationCode: 'ConfirmationCode',
  SignUpAccountCreated: 'SignUpAccountCreated',
  Biography: 'Biography',
  Reviews: 'Reviews',
  MakeAnAppointment: 'MakeAnAppointment',
  AppointmentSuccess: 'AppointmentSuccess',
  Filters: 'Filters',
  Faq: 'Faq',
  PrivacyPolicy: 'PrivacyPolicy',
  AccountScreen: 'AccountScreen',
  EditMyProfile: 'EditMyProfile',
  MyAppointments: 'MyAppointments',
  MyDoctors: 'MyDoctors',
  MyTestsAndDiagnostics: 'MyTestsAndDiagnostics',
  LogOut: 'LogOut',
  ChangePassword: 'ChangePassword',
  InfoBranchScreen: 'InfoBranchScreen',
  Notifications: 'Notifications',
  HistoryKCB: 'HistoryKCB',
  RelativesProfile: 'RelativesProfile',
  MyProfile: 'MyProfile',
  SelectInfoProfile: 'SelectInfoProfile',
  SearchScreen: 'SearchScreen',
  AppointmentDetail: 'AppointmentDetail',
  QRScanScreen: 'QRScanScreen',
  CateMedicalExamination: 'CateMedicalExamination',
  CateEmergency24h: 'CateEmergency24h',
  CateCommunityQA: 'CateCommunityQA',
  CateCommunityQADetail: 'CateCommunityQADetail',
  MyQRCode: 'MyQRCode',
  CheckBHYTInfo: 'CheckBHYTInfo',
  BannerDetail: 'BannerDetail',
  BlogDetail: 'BlogDetail',
  AppointmentComfirm: 'AppointmentComfirm',
  AppointmentFilter: 'AppointmentFilter',
  SettingScreen: 'SettingScreen',
  NotificationDetail: 'NotificationDetail',
  AppointmentRatingSubmit: 'AppointmentRatingSubmit',
  Favorite: 'Favorite',
  DoctorByHospitalAll: 'DoctorByHospitalAll',
  ScreenExpertDoctors: 'ScreenExpertDoctors',
  CateMedicalExaminationDetail: 'CateMedicalExaminationDetail',
  ServiceDetail: 'ServiceDetail',
  Appointment: 'Appointment',
  ViewAllBlog: 'ViewAllBlog',

  //
  EXPERT: 'Expert',
  TAISANRONG: 'TaiSanRong',
  TRANSACTION: 'Transaction',
  EDIT_ACCOUNT: 'EditAccount',
  ThemTaiSan: 'ThemTaiSan',
};
