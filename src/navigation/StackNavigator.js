import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

import {SCREEN_NAME} from './ScreenName';
import {screens} from '../screens';

const StackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={SCREEN_NAME.SplashScreen}>
      <Stack.Screen
        name={SCREEN_NAME.SplashScreen}
        component={screens.SplashScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={SCREEN_NAME.SignIn}
        component={screens.SignIn}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={SCREEN_NAME.TabNavigator}
        component={screens.TabNavigator}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={SCREEN_NAME.EDIT_ACCOUNT}
        component={screens.EditAccount}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={SCREEN_NAME.ThemTaiSan}
        component={screens.ThemTaiSan}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default StackNavigator;
