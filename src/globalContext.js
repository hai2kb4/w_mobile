import {createRef, createContext} from 'react';

const global = {
  loadingRef: createRef(),
  imagePreviewRef: createRef(),
  uploadFileRef: createRef(),
  modalRef: createRef(),
};

export default createContext(global);
