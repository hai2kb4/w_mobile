import React from 'react';
import {Text, View} from 'react-native';
import {components} from '../../../components';
import {theme} from '../../../constants';
import {formatVND} from '../../../utils';

const TaiSanList = ({data = []}) => {
  return (
    <View style={{padding: 16, paddingBottom: 0}}>
      <components.BlockHeader title={'Danh sách tài sản'} />
      <View
        style={{
          backgroundColor: theme.colors.darkBlue,
          borderRadius: 12,
          marginTop: 16,
        }}
      >
        {data.map((item) => {
          return (
            <>
              <View style={{padding: 12}}>
                <View style={{...theme.styles.rowAlignItemsCenter}}>
                  <View
                    style={{
                      width: 40,
                      height: 40,
                      backgroundColor: item.color,
                      borderRadius: 12,
                    }}
                  />
                  <View style={{flex: 1, marginLeft: 12}}>
                    <View
                      style={{
                        ...theme.styles.rowJustifyContentSpaceBetween,
                      }}
                    >
                      <Text style={theme.fonts.textStyle14}>{item.ten}</Text>
                      <Text style={theme.fonts.textStyle14}>
                        {formatVND(item.soTien)} đ
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </>
          );
        })}
      </View>
    </View>
  );
};
export default TaiSanList;
