import React, {useRef, useState} from 'react';
import {Text, View} from 'react-native';
import {components} from '../../../components';
import {theme} from '../../../constants';
import {TextInput} from 'react-native-gesture-handler';
import {formatVND} from '../../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {
  createTaiSanRong,
  taiSanSelector,
} from '../../../store/slices/taiSanSlice';
import moment from 'moment';

const loaiTaiSan = ['Tiền mặt', 'khác'];

const ThemTaiSan = ({route, navigation}) => {
  const dispatch = useDispatch();
  const {isLoading} = useSelector(taiSanSelector);
  const modalRef = useRef(null);
  const {id} = route.params;
  const [name, setName] = useState();
  const [price, setPrice] = useState('0');
  const [type, setType] = useState(0);
  const [donVi, setDonVi] = useState(null);
  const [thapPhan, setThapPhan] = useState('0');
  const [soLuong, setSoLuong] = useState('0');
  const [markedDates, setMarkedDates] = useState();

  const onFinish = () => {
    const payload = {
      soTien: price,
      ten: name,
      loai: id,
      soLuong,
      donVi,
      ngay: markedDates ? moment(markedDates) : null,
      onSuccess: () => navigation.goBack(),
    };
    dispatch(createTaiSanRong(payload));
  };

  const renderHeader = () => {
    return (
      <components.Header
        title={id === 1 ? 'Thêm tài sản' : 'Thêm khoản nợ'}
        goBack={true}
        customRight={
          <components.Touchable
            containerStyle={{
              height: 48,
              width: 48,
              ...theme.styles.justifyContentCenter,
            }}
            onPress={() => onFinish()}
            content={<Text style={theme.fonts.textStyle14}>Lưu</Text>}
          />
        }
      />
    );
  };
  const renderContent = () => {
    return (
      <components.ScrollViewLayout
        content={
          <View style={{padding: 16}}>
            <Text style={theme.fonts.textStyle14}>
              {id === 1 ? 'THÔNG TIN TÀI SẢN' : 'THÔNG TIN KHOẢN NỢ'}
            </Text>
            <View
              style={{
                padding: 16,
                backgroundColor: theme.colors.darkBlue,
                marginTop: 12,
                borderRadius: 12,
              }}
            >
              <components.InputField
                value={name}
                onChangeText={(text) => setName(text)}
                title={id === 1 ? 'Tên tài sản' : 'Tên khoản nợ'}
                placeholder={'Nhập tên'}
              />
              {id === 1 && (
                <View style={{marginTop: 16}}>
                  <Text style={theme.fonts.textStyle14}>Loại tài sản</Text>
                  <View
                    style={{
                      ...theme.styles.rowJustifyContentSpaceBetween,
                      marginTop: 4,
                    }}
                  >
                    {loaiTaiSan.map((label, index) => (
                      <components.RadioButton
                        selectValue={type}
                        onPress={() => setType(index)}
                        value={index}
                        version='v1'
                        key={index}
                        label={label}
                        containerStyle={{width: '48%'}}
                      />
                    ))}
                  </View>
                  <Text
                    style={{
                      ...theme.fonts.textStyle14,
                      marginTop: 8,
                      color: theme.colors.placeholderTextColor,
                    }}
                  >
                    Là tài sản thể hiển dưới dạng tiền mặt như: tiền trong ví,
                    số tiết kiệm,...
                  </Text>
                </View>
              )}
              {type === 1 && (
                <View
                  style={{
                    ...theme.styles.rowJustifyContentSpaceBetween,
                    marginTop: 16,
                  }}
                >
                  <Text style={theme.fonts.textStyle14}>Số lượng</Text>
                  <View style={theme.styles.rowAlignItemsCenter}>
                    <components.Touchable
                      onPress={() => {
                        if (Number(soLuong) > 0) {
                          const number = Number(soLuong) - 1;
                          setSoLuong(number + '');
                        }
                      }}
                      containerStyle={{
                        width: 30,
                        height: 30,
                        ...theme.styles.justifyContentCenter,
                      }}
                      content={
                        <Text
                          style={{...theme.fonts.textStyle16, fontSize: 20}}
                        >
                          -
                        </Text>
                      }
                    />
                    <TextInput
                      value={soLuong}
                      onChangeText={(text) => setSoLuong(text)}
                      keyboardType='numeric'
                      style={{
                        borderWidth: 1,
                        width: 30,
                        height: 30,
                        marginHorizontal: 8,
                        padding: 0,
                        borderColor: theme.colors.stroke,
                        borderRadius: 4,
                        textAlign: 'center',
                        color: theme.colors.white,
                        fontSize: 14,
                      }}
                    />
                    <components.Touchable
                      onPress={() => {
                        const number = Number(soLuong) + 1;
                        setSoLuong(number + '');
                      }}
                      containerStyle={{
                        width: 30,
                        height: 30,
                        ...theme.styles.justifyContentCenter,
                      }}
                      content={
                        <Text
                          style={{...theme.fonts.textStyle16, fontSize: 20}}
                        >
                          +
                        </Text>
                      }
                    />
                  </View>
                </View>
              )}
              {type === 1 && (
                <components.InputField
                  value={donVi}
                  onChangeText={(text) => setDonVi(text)}
                  containerStyle={{marginTop: 16}}
                  title={'Đơn vị tài sản'}
                  placeholder='$, VND, BTC, ...'
                />
              )}
              <components.InputField
                value={formatVND(price)}
                onChangeText={(text) => setPrice(text)}
                containerStyle={{marginTop: 16}}
                title={id === 1 ? 'Giá trị ban đầu' : 'Số tiền nợ'}
                keyboardType='numeric'
              />
              {type === 1 && (
                <components.InputField
                  value={formatVND(thapPhan)}
                  onChangeText={(text) => setThapPhan(text)}
                  containerStyle={{marginTop: 16}}
                  title={'Hiển thị chữ số thập phân'}
                  keyboardType='numeric'
                />
              )}
              <View style={{marginTop: 16}}>
                <Text style={theme.fonts.textStyle14}>Chọn thời gian</Text>
                <components.Touchable
                  onPress={() => modalRef.current.open()}
                  containerStyle={{
                    height: 40,
                    borderWidth: 1,
                    ...theme.styles.rowAlignItemsCenter,
                    borderColor: theme.colors.white,
                    borderRadius: 4,
                    marginTop: 4,
                  }}
                  content={
                    <Text
                      style={{
                        ...theme.fonts.textStyle14,
                        color: markedDates
                          ? theme.colors.white
                          : theme.colors.placeholderTextColor,
                        marginLeft: 12,
                      }}
                    >
                      {markedDates ? markedDates : 'Chọn thời gian'}
                    </Text>
                  }
                />
              </View>
            </View>
          </View>
        }
      />
    );
  };
  return (
    <components.CustomSafeArea isContext={false} isLoading={isLoading}>
      {renderHeader()}
      <components.ScrollViewLayout content={renderContent()} />
      <components.CustomViewModal
        onBackButtonPress={() => modalRef.current.close()}
        adjustToContentHeight
        showHeader
        title='Chọn thời gian'
        modalRef={modalRef}
        content={
          <components.CalendarPicker
            markedDates={markedDates}
            onDayPress={(day) => {
              setMarkedDates(day.dateString);
              modalRef.current.close();
            }}
          />
        }
      />
    </components.CustomSafeArea>
  );
};

export default ThemTaiSan;
