import React, {useEffect} from 'react';
import {ActivityIndicator, Image, StyleSheet, View} from 'react-native';
import {theme} from '../../../constants';
import {useDispatch, useSelector} from 'react-redux';
import Storage, {STORAGE_APP} from '../../../utils/storage';
import {isValueEmpty} from '../../../utils';
import {
  authSelector,
  profileApi,
  refreshToken,
} from '../../../store/slices/authSlice';
import {useNetInfo} from '@react-native-community/netinfo';
import {useNavigation} from '@react-navigation/native';
import {SCREEN_NAME} from '../../../navigation/ScreenName';

const LogoMedic = require('../../../assets/images/logo_wingroup.png');

const SplashScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {firebaseToken} = useSelector(authSelector);
  const {isConnected} = useNetInfo();

  useEffect(() => {
    if (isConnected) {
      getTokenPayLoad();
    }
  }, [isConnected]);

  const getTokenPayLoad = async () => {
    const payload = {
      firebaseToken,
      onSuccess: () => {
        navigation.replace(SCREEN_NAME.TabNavigator);
      },
    };
    Storage.getItem(STORAGE_APP.AUTH).then((result) => {
      if (!isValueEmpty(result)) {
        payload.token = result.refreshToken;
        dispatch(refreshToken(payload));
      } else {
        navigation.replace(SCREEN_NAME.SignIn);
      }
    });
  };

  return (
    <View style={styles.container}>
      <Image source={LogoMedic} style={{height: 200}} resizeMode={'contain'} />
      <ActivityIndicator color={theme.colors.darkBlue} style={{marginTop: 4}} />
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.white,
    paddingHorizontal: 24,
  },
});
