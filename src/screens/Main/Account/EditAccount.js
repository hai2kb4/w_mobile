import React from 'react';
import {theme} from '../../../constants';
import {components} from '../../../components';
import {Text} from 'react-native';

const EditAccount = () => {
  const renderHeader = () => {
    return (
      <components.Header
        title={'Sửa thông tin'}
        goBack={true}
        color={'#2d2753'}
      />
    );
  };

  return (
    <components.CustomSafeArea isContext={false}>
      {renderHeader()}
    </components.CustomSafeArea>
  );
};

export default EditAccount;
