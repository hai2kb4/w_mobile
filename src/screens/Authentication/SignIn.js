import React, {useState} from 'react';
import {components} from '../../components';
import {View} from 'react-native';
import {theme} from '../../constants';
import {SCREEN_NAME} from '../../navigation/ScreenName';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useDispatch, useSelector} from 'react-redux';
import {setUserName, signInApi} from '../../store/slices/authSlice';

const SignIn = ({navigation}) => {
  const dispatch = useDispatch();
  const {isLoading, userName} = useSelector((state) => state.auth);
  const [password, setPassword] = useState('');
  const inset = useSafeAreaInsets();

  const onSignIn = () => {
    const payload = {
      userName,
      password,
      onSuccess: () => {
        navigation.navigate(SCREEN_NAME.TabNavigator);
      },
    };
    dispatch(signInApi(payload));
  };

  const renderContent = () => {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <components.ImageLoader
          source={require('../../assets/images/logo_wingroup.png')}
          style={{height: 150, marginBottom: 30}}
        />
        <components.InputField
          value={userName}
          onChangeText={(text) => dispatch(setUserName(text))}
          placeholder={'Tài khoản'}
        />
        <components.InputField
          value={password}
          onChangeText={(text) => setPassword(text.trim())}
          placeholder={'Mật khẩu'}
          containerStyle={{marginTop: 12}}
          secureTextEntry
        />
        <components.Button
          loading={isLoading}
          onPress={() => onSignIn()}
          title={'Đăng nhập'}
          containerStyle={{marginTop: 12}}
        />
        <components.TextButton
          title={'Quên mật khẩu?'}
          containerStyle={{marginTop: 4, textAlign: 'right'}}
        />
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: theme.colors.backgroundColor,
        paddingHorizontal: 16,
      }}
    >
      {renderContent()}
      <components.Button
        onPress={() => navigation.navigate(SCREEN_NAME.TabNavigator)}
        title='Đăng ký thành viên'
        titleStyle={{color: theme.colors.white}}
        containerStyle={{
          marginBottom: inset.bottom,
          backgroundColor: theme.colors.backgroundColor,
        }}
      />
    </View>
  );
};

export default SignIn;
