import SignIn from './SignIn';

const Authentication = {
  SignIn,
};

export const authScreens = Object.assign({}, Authentication);
