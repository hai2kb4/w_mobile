import React from 'react';
import {Text, View} from 'react-native';
import {components} from '../../components';
import {theme} from '../../constants';

const HuList = () => {
  return (
    <View style={{padding: 16, paddingBottom: 0}}>
      <components.BlockHeader title={'Danh sách hũ'} />
      <View
        style={{
          backgroundColor: theme.colors.darkBlue,
          borderRadius: 12,
          marginTop: 16,
        }}
      >
        {[{}, {}].map((item) => {
          return (
            <>
              <View style={{padding: 12}}>
                <View style={{...theme.styles.rowAlignItemsCenter}}>
                  <View
                    style={{
                      width: 40,
                      height: 40,
                      backgroundColor: 'red',
                      borderRadius: 12,
                    }}
                  />
                  <View style={{flex: 1, marginLeft: 12}}>
                    <View
                      style={{
                        ...theme.styles.rowJustifyContentSpaceBetween,
                      }}
                    >
                      <Text style={theme.fonts.textStyle14}>Thiết yếu</Text>
                      <Text style={theme.fonts.textStyle14}>0 đ</Text>
                    </View>
                    <View
                      style={{
                        ...theme.styles.rowJustifyContentSpaceBetween,
                      }}
                    >
                      <Text style={theme.fonts.textStyle13}>Khả năng</Text>
                      <Text style={theme.fonts.textStyle13}>0%</Text>
                    </View>
                  </View>
                </View>
              </View>
            </>
          );
        })}
      </View>
    </View>
  );
};

export default HuList;
