import React from 'react';
import {Text, View} from 'react-native';
import {theme} from '../../constants';
import {components} from '../../components';
import {PieChart} from 'react-native-chart-kit';

const CoCauTaiSan = ({data = [], showButton = false}) => {
  const renderLegend = (text, color) => {
    return (
      <View style={{flexDirection: 'row', marginBottom: 12}}>
        <View
          style={{
            height: 18,
            width: 18,
            marginRight: 10,
            borderRadius: 4,
            backgroundColor: color || 'white',
          }}
        />
        <Text style={{color: 'white', fontSize: 16}}>{text || ''}</Text>
      </View>
    );
  };
  return (
    <View style={{paddingHorizontal: 16, marginBottom: 32}}>
      <components.BlockHeader title={'Cơ cấu các hũ'} />
      <View
        style={{
          backgroundColor: theme.colors.darkBlue,
          borderRadius: 12,
          padding: 16,
          marginTop: 16,
        }}
      >
        <View style={{...theme.styles.justifyContentCenter, marginBottom: 24}}>
          <PieChart
            data={data}
            width={theme.sizes.width - 32}
            height={150}
            accessor={'population'}
            backgroundColor={'transparent'}
            chartConfig={{
              color: (opacity = 1) => theme.colors.white,
            }}
          />
        </View>
        {showButton && <components.Button title={'Thêm hũ mới'} />}
      </View>
    </View>
  );
};

export default CoCauTaiSan;
