import React from 'react';
import {View} from 'react-native';
import {theme} from '../../constants';
import {components} from '../../components';
import {LineChart} from 'react-native-chart-kit';
import {formatVND} from '../../utils';

const BaoCaoThuChi = ({data, showButton = false}) => {
  var datasets = data?.datasets || [];
  return (
    <View style={{padding: 16}}>
      <components.BlockHeader title={'Báo cáo thu chi'} />
      <View
        style={{
          backgroundColor: theme.colors.darkBlue,
          borderRadius: 12,
          marginTop: 16,
          paddingVertical: 16,
        }}
      >
        <LineChart
          bezier
          formatYLabel={(label) => formatVND(Number(label) / 1000) + 'k'}
          data={{
            labels: data?.labels || [],
            datasets: datasets?.map((x) => ({
              data: x.data,
              strokeWidth: 2,
              color: (opacity = 1) => x?.color,
            })),
            legend: data?.legend || [],
          }}
          // yAxisSuffix='k'
          withInnerLines={false}
          width={theme.sizes.width - 32}
          height={220}
          withShadow={true}
          // yAxisLabel='K'
          chartConfig={{
            backgroundColor: theme.colors.darkBlue,
            backgroundGradientFrom: theme.colors.darkBlue,
            backgroundGradientTo: theme.colors.darkBlue,
            color: (opacity = 1) => theme.colors.white,
            backgroundGradientToOpacity: 1,
            decimalPlaces: 0,
            strokeWidth: 1,
          }}
        />
        {showButton && (
          <components.Button
            title={'Xem tất cả'}
            containerStyle={{margin: 16, marginBottom: 0}}
          />
        )}
      </View>
    </View>
  );
};

export default BaoCaoThuChi;
