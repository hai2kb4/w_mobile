import React from 'react';
import {Text, View} from 'react-native';
import {theme} from '../../constants';
import {components} from '../../components';
import {LineChart} from 'react-native-chart-kit';

const ThongKeTaiSan = ({data}) => {
  return (
    <View style={{paddingHorizontal: 16, marginTop: 16}}>
      <components.BlockHeader title={'Thống kê tài sản'} />
      <View
        style={{
          backgroundColor: theme.colors.darkBlue,
          borderRadius: 12,
          padding: 16,
          marginTop: 16,
        }}
      >
        <View style={{...theme.styles.justifyContentCenter}}>
          <LineChart
            data={{
              datasets: [
                {
                  data: [20, 45, 28, 80, 99, 43],
                  color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optioal
                },
              ],
            }}
            width={theme.sizes.width - 32}
            height={220}
            chartConfig={{
              backgroundColor: theme.colors.darkBlue,
              backgroundGradientFrom: theme.colors.darkBlue,
              backgroundGradientTo: theme.colors.darkBlue,
              color: (opacity = 1) => theme.colors.white,
              //   strokeWidth: 2,
              //   barPercentage: 0.5,
              //   useShadowColorFromDataset: false,
            }}
            bezier
          />
        </View>
      </View>
    </View>
  );
};

export default ThongKeTaiSan;
