import React, {useEffect, useState} from 'react';
import {components} from '../../components';
import {ELoaiTien, theme} from '../../constants';
import {Text, View} from 'react-native';
import {svg} from '../../assets';
import {SCREEN_NAME} from '../../navigation/ScreenName';
import BaoCaoThuChi from '../Module/BaoCaoThuChi';
import CoCauTaiSan from '../Module/CoCauTaiSan';
import LinearGradient from 'react-native-linear-gradient';
import {useDispatch, useSelector} from 'react-redux';
import {
  GetAllHuTien,
  GetTaiSan,
  getAllHistory,
  getBaoCaoThuChi,
  mobileSelector,
} from '../../store/slices/mobileSlice';
import {formatVND} from '../../utils';
import moment from 'moment';
import * as Progress from 'react-native-progress';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const {huTienList, historyList, taiSanList, baoCao} =
    useSelector(mobileSelector);
  const [scrollIndex, setScrollIndex] = useState(0);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      dispatch(GetAllHuTien());
      dispatch(getAllHistory());
      dispatch(GetTaiSan());
      dispatch(getBaoCaoThuChi());
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {}, [scrollIndex]);

  const renderHeader = () => {
    return (
      <>
        <components.Header
          notify={true}
          user={true}
          color={theme.colors.backgroundColor}
        />
      </>
    );
  };

  const featureAsset = () => {
    return (
      <>
        <components.ScrollViewLayout
          onScroll={(x) =>
            x.nativeEvent.contentOffset.x > 100
              ? setScrollIndex(1)
              : setScrollIndex(0)
          }
          horizontal={true}
          content={taiSanList?.map((item, index) => (
            <>
              <LinearGradient
                colors={['#56B8FE', '#B30FD6', '#B008D5']}
                start={{x: 0, y: 0}}
                locations={[0, 0.7, 0.5]}
                end={{x: 1, y: 0}}
                style={{
                  width: theme.sizes.width - 100,
                  marginLeft: 16,
                  marginTop: 12,
                  borderRadius: 12,
                  padding: 16,
                  marginRight: taiSanList?.length === index + 1 ? 16 : 0,
                  backgroundColor: theme.colors.darkBlue,
                }}
              >
                <View
                  style={{
                    ...theme.styles.rowAlignItemsCenter,
                    borderBottomWidth: 1,
                    borderColor: theme.colors.white,
                    paddingBottom: 16,
                  }}
                >
                  <Progress.Circle
                    borderColor={theme.colors.placeholderTextColor}
                    color={theme.colors.white}
                    progress={item.percent}
                    size={40}
                    showsText
                    textStyle={{
                      color: theme.colors.white,
                      fontSize: 13,
                    }}
                  />
                  <View style={{marginLeft: 12}}>
                    <Text style={{...theme.fonts.textStyle14}}>
                      {item.name}
                    </Text>
                    <Text style={{...theme.fonts.textStyle16}}>
                      {formatVND(item.amount)} đ
                    </Text>
                  </View>
                </View>
                <Text
                  style={{
                    ...theme.fonts.textStyle13,
                    paddingTop: 16,
                  }}
                >
                  {item.tag}
                </Text>
              </LinearGradient>
            </>
          ))}
        />
        <View
          style={{
            ...theme.styles.rowJustifyContentSpaceAround,
            paddingHorizontal: 12,
            marginTop: 12,
          }}
        >
          {taiSanList?.length > 0 &&
            taiSanList[scrollIndex].children.map((item, index) =>
              featureAssetItem(item, index),
            )}
        </View>
      </>
    );
  };

  const featureAssetItem = (item, index) => {
    return (
      <components.Touchable
        onPress={() =>
          navigation.navigate(
            scrollIndex === 0
              ? SCREEN_NAME.TRANSACTION
              : SCREEN_NAME.ThemTaiSan,
            {type: item.id},
          )
        }
        containerStyle={{
          padding: 12,
          backgroundColor: theme.colors.darkBlue,
          width: '48%',
          borderRadius: 8,
        }}
        content={
          <>
            <View style={theme.styles.rowJustifyContentSpaceBetween}>
              <Text style={theme.fonts.textStyle16}>{item.name}</Text>
              <Text
                style={{
                  fontSize: 20,
                  color: theme.colors.white,
                }}
              >
                {index === 0 ? '+' : '-'}
              </Text>
            </View>
            <Text style={theme.fonts.textStyle16}>
              {formatVND(item.amount)} đ
            </Text>
          </>
        }
      />
    );
  };

  const featureHuList = () => {
    return (
      <View style={{padding: 16}}>
        <components.BlockHeader title={'Danh sách hũ'} />
        <View
          style={{
            backgroundColor: theme.colors.darkBlue,
            borderRadius: 12,
            marginTop: 16,
          }}
        >
          {huTienList?.map((item) => {
            return (
              <>
                <View style={{padding: 12}}>
                  <View style={{...theme.styles.rowAlignItemsCenter}}>
                    <View
                      style={{
                        width: 40,
                        height: 40,
                        backgroundColor: item.color,
                        borderRadius: 12,
                      }}
                    />
                    <View style={{flex: 1, marginLeft: 12}}>
                      <View
                        style={{
                          ...theme.styles.rowJustifyContentSpaceBetween,
                        }}
                      >
                        <Text style={theme.fonts.textStyle14}>
                          {item.tenHu}
                        </Text>
                        <Text style={theme.fonts.textStyle14}>
                          {formatVND(item.soTien)} đ
                        </Text>
                      </View>
                      <View
                        style={{
                          ...theme.styles.rowJustifyContentSpaceBetween,
                        }}
                      >
                        <Text
                          style={{
                            ...theme.fonts.textStyle13,
                            color: theme.colors.gray,
                          }}
                        >
                          Khả năng
                        </Text>
                        <Text style={theme.fonts.textStyle13}>
                          {item.phanTram}%
                        </Text>
                      </View>
                      <Progress.Bar
                        borderColor={theme.colors.placeholderTextColor}
                        color={theme.colors.primary}
                        progress={item.phanTram / 100}
                        width={theme.sizes.width - 110}
                      />
                    </View>
                  </View>
                </View>
              </>
            );
          })}
        </View>
      </View>
    );
  };

  const featureHistory = () => {
    return (
      <View style={{paddingHorizontal: 16}}>
        <components.BlockHeader title={'Lịch sử'} />
        <View
          style={{
            padding: 16,
            backgroundColor: theme.colors.darkBlue,
            borderRadius: 12,
            marginTop: 12,
            height: 300,
          }}
        >
          <View style={theme.styles.rowJustifyContentSpaceBetween}>
            <Text style={theme.fonts.textStyle13}>
              {moment().format('MM/YYYY')}
            </Text>
          </View>
          <View style={{height: 200, marginVertical: 6}}>
            {historyList?.slice(0, 3).map((item, index) => (
              <View key={index}>
                <View style={{paddingVertical: 12}}>
                  <View style={{...theme.styles.rowAlignItemsCenter}}>
                    <Text style={theme.fonts.textStyle14}>{item.ngay}</Text>
                    <View
                      style={{
                        width: 40,
                        height: 40,
                        backgroundColor: item?.color || 'red',
                        borderRadius: 12,
                        marginLeft: 8,
                      }}
                    />
                    <View style={{flex: 1, marginLeft: 12}}>
                      <View
                        style={{
                          ...theme.styles.rowJustifyContentSpaceBetween,
                        }}
                      >
                        <View>
                          <Text style={theme.fonts.textStyle14}>
                            {item.ten}
                          </Text>
                          <Text
                            style={{
                              ...theme.fonts.textStyle13,
                              color: theme.colors.placeholderTextColor,
                            }}
                          >
                            {item.noiDung}
                          </Text>
                        </View>
                        <Text
                          style={{
                            ...theme.fonts.textStyle14,
                            color: [1, 3].includes(item.loai)
                              ? theme.colors.mainGreen
                              : theme.colors.error,
                          }}
                        >
                          {[1, 3].includes(item.loai) ? '+' : '-'}{' '}
                          {formatVND(item.soTien)} đ
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            ))}
          </View>

          <components.Button title={'Xem tất cả'} />
        </View>
      </View>
    );
  };

  return (
    <components.CustomSafeArea isContext={false}>
      {renderHeader()}
      <components.ScrollViewLayout
        content={
          <>
            {featureAsset()}
            {featureHuList()}
            {featureHistory()}
            <BaoCaoThuChi data={baoCao} showButton />
            <CoCauTaiSan
              data={huTienList?.map((x) => ({
                name: x.tenHu,
                population: x.phanTram,
                color: x.color,
                legendFontColor: '#7F7F7F',
                legendFontSize: 15,
              }))}
              showButton
            />
          </>
        }
      />
    </components.CustomSafeArea>
  );
};

export default Home;
