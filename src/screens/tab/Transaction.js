import React, {useEffect, useRef, useState} from 'react';
import {components} from '../../components';
import {StyleSheet, View, Text} from 'react-native';
import {ELoaiTien, theme} from '../../constants';
import {TextInput} from 'react-native-gesture-handler';
import {formatDate, formatVND} from '../../utils';
import moment from 'moment';
import {useDispatch} from 'react-redux';
import {createTaiSanRong} from '../../store/slices/taiSanSlice';
import {warning} from '../../utils/flashMessage';
const dataTab = [
  {label: 'Thu nhập', id: ELoaiTien.thuNhap},
  {label: 'Chi tiêu', id: ELoaiTien.chiTieu},
  {label: 'Chuyển tiền', id: ELoaiTien.chuyenTien},
];
const tagList = ['food', 'lương', 'ăn', 'salary', 'annuong', 'xăng', 'ansang'];
const Transaction = ({route, navigation}) => {
  const dispatch = useDispatch();
  const [selectedTab, setSelectTab] = useState(
    route.params?.type || ELoaiTien.thuNhap,
  );
  const [markedDates, setMarkedDates] = useState();
  const [amount, setAmount] = useState('0');
  const [moTa, setMoTa] = useState();
  const [tag, setTag] = useState();
  const modalRef = useRef(null);

  useEffect(() => {
    if (route.params.type) {
      setSelectTab(route.params.type);
    }
  }, [route.params?.type]);

  const onSubmit = () => {
    if (Number(amount) <= 0) {
      return warning('Số tiền phải lớn hơn 0');
    }
    const payload = {
      soTien: amount.replace('.', ''),
      loai: selectedTab,
      moTa: moTa,
      moTaKhac: tag,
      ngay: moment(markedDates),
      onSuccess: () => navigation.goBack(),
    };
    dispatch(createTaiSanRong(payload));
  };

  const renderHeader = () => {
    return (
      <components.Header
        title={'Thêm giao dịch'}
        goBack={true}
        customRight={
          <components.Touchable
            onPress={() => onSubmit()}
            containerStyle={{
              width: 54,
              height: 48,
              ...theme.styles.justifyContentCenter,
            }}
            content={<Text style={theme.fonts.textStyle14}>Lưu</Text>}
          />
        }
      />
    );
  };

  const rederButtonTab = (item, _, length) => {
    return (
      <components.Touchable
        key={item.id}
        onPress={() => {
          // dispatch(setLoading(true));
          setSelectTab(item.id);
          // formSearch.current.query.isCreateByUser = !(index === 0);
          // reloadData();
        }}
        containerStyle={{
          ...styles.buttonTab,
          width: (theme.sizes.width - 32) / length,
          // backgroundColor:
          //   selectedTab === index ? theme.colors.white : theme.colors.gray,
          borderWidth: selectedTab === item.id ? 1 : 0,
          borderColor: theme.colors.stroke,
        }}
        content={
          <Text
            style={{
              ...theme.fonts.textStyle13,
              fontWeight: selectedTab === item.id ? '700' : '400',
            }}
          >
            {item.label}
          </Text>
        }
      />
    );
  };

  return (
    <components.CustomSafeArea isContext={false}>
      {renderHeader()}
      <View style={styles.container}>
        <View style={styles.tab}>
          {dataTab.map((item, index, arr) =>
            rederButtonTab(item, index, arr.length),
          )}
        </View>
        <components.ScrollViewLayout
          content={
            <>
              <View style={{marginTop: 24}}>
                <Text style={theme.fonts.textStyle13}>Số tiền</Text>
                <View style={theme.styles.rowJustifyContentSpaceBetween}>
                  <View style={{paddingHorizontal: 15}} />
                  <TextInput
                    value={amount}
                    onChangeText={(text) => setAmount(formatVND(text))}
                    style={{
                      color: theme.colors.mainGreen,
                      fontSize: 30,
                      textAlign: 'center',
                      fontWeight: 'bold',
                      flex: 1,
                    }}
                    keyboardType='numeric'
                  />
                  <Text
                    style={{
                      backgroundColor: theme.colors.red,
                      paddingHorizontal: 15,
                      paddingVertical: 4,
                      borderRadius: 100,
                      color: theme.colors.white,
                    }}
                  >
                    đ
                  </Text>
                </View>
              </View>
              <components.Touchable
                containerStyle={{
                  padding: 16,
                  backgroundColor: theme.colors.darkBlue,
                  borderRadius: 12,
                  marginTop: 24,
                }}
                content={
                  <View style={theme.styles.rowAlignItemsCenter}>
                    <View
                      style={{backgroundColor: 'red', width: 40, height: 40}}
                    />
                    <View style={{marginLeft: 16}}>
                      <Text style={theme.fonts.textStyle16}>Thiết yếu</Text>
                      <Text
                        style={{
                          ...theme.fonts.textStyle13,
                          color: theme.colors.placeholderTextColor,
                          lineHeight: 24,
                        }}
                      >
                        Nhập để thay đổi
                      </Text>
                    </View>
                  </View>
                }
              />
              <View
                style={{
                  padding: 16,
                  backgroundColor: theme.colors.darkBlue,
                  borderRadius: 12,
                  marginTop: 12,
                }}
              >
                <components.Touchable
                  onPress={() => modalRef.current.open()}
                  containerStyle={{
                    height: 48,
                    justifyContent: 'center',
                    borderBottomWidth: 1,
                    borderColor: theme.colors.placeholderTextColor,
                  }}
                  content={
                    <Text style={theme.fonts.textStyle14}>
                      {formatDate(markedDates)}
                    </Text>
                  }
                />
                <TextInput
                  value={moTa}
                  onChangeText={(text) => setMoTa(text)}
                  placeholder='Thêm mô tả'
                  placeholderTextColor={theme.colors.placeholderTextColor}
                  style={{
                    ...theme.fonts.textStyle14,
                    borderBottomWidth: 1,
                    borderColor: theme.colors.placeholderTextColor,
                  }}
                />
                <TextInput
                  value={tag}
                  onChangeText={(text) => setTag(text)}
                  placeholder='#luong #thuong...'
                  placeholderTextColor={theme.colors.placeholderTextColor}
                  style={{
                    ...theme.fonts.textStyle14,
                    borderBottomWidth: 1,
                    borderColor: theme.colors.placeholderTextColor,
                  }}
                />
                <View
                  style={{
                    ...theme.styles.rowAlignItemsCenter,
                    marginTop: 12,
                    flexWrap: 'wrap',
                  }}
                >
                  {tagList.map((label, index) => (
                    <components.Touchable
                      onPress={() => setTag((prev) => prev + `#${label} `)}
                      containerStyle={{marginRight: 12, padding: 2}}
                      key={index}
                      content={
                        <Text
                          style={{
                            ...theme.fonts.textStyle13,
                          }}
                        >
                          #{label}
                        </Text>
                      }
                    />
                  ))}
                </View>
              </View>
            </>
          }
        />
      </View>
      <components.CustomViewModal
        onBackButtonPress={() => modalRef.current.close()}
        adjustToContentHeight
        showHeader
        title='Chọn thời gian'
        modalRef={modalRef}
        content={
          <components.CalendarPicker
            markedDates={markedDates}
            onDayPress={(day) => {
              setMarkedDates(day.dateString);
              modalRef.current.close();
            }}
          />
        }
      />
    </components.CustomSafeArea>
  );
};

export default Transaction;

const styles = StyleSheet.create({
  container: {
    flex: 1,

    padding: 16,
  },
  tab: {
    ...theme.styles.justifyContentCenter,
    backgroundColor: theme.colors.darkBlue,
    flexDirection: 'row',
    padding: 2,
    borderRadius: 4,
  },
  buttonTab: {
    padding: 4,
    borderRadius: 4,
    height: 40,
    ...theme.styles.justifyContentCenter,
  },
});
