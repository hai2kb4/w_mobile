import React, {useEffect} from 'react';
import {components} from '../../components';
import {theme} from '../../constants';
import {View, Text} from 'react-native';
import {svg} from '../../assets';
import CoCauTaiSan from '../Module/CoCauTaiSan';
import BaoCaoThuChi from '../Module/BaoCaoThuChi';
import ThongKeTaiSan from '../Module/ThongKeTaiSan';
import LinearGradient from 'react-native-linear-gradient';
import {useDispatch, useSelector} from 'react-redux';
import {
  getBaoCaoThuChi,
  getTaiSanRong,
  taiSanSelector,
} from '../../store/slices/taiSanSlice';
import {formatVND} from '../../utils';
import TaiSanList from '../Main/TaiSan/TaiSanList';
import {SCREEN_NAME} from '../../navigation/ScreenName';
import * as Progress from 'react-native-progress';

const TaiSanRong = ({navigation}) => {
  const dispatch = useDispatch();
  const {taiSan, baoCao} = useSelector(taiSanSelector);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      dispatch(getTaiSanRong());
      dispatch(getBaoCaoThuChi());
    });
    return unsubscribe;
  }, [navigation]);

  const renderHeader = () => {
    return <components.Header title={'Tài sản'} />;
  };
  const renderContent = () => {
    return (
      <>
        <LinearGradient
          colors={['#56B8FE', '#B30FD6', '#B008D5']}
          start={{x: 0, y: 0}}
          locations={[0, 0.7, 0.5]}
          end={{x: 1, y: 0}}
          style={{
            width: theme.sizes.width - 32,
            borderRadius: 12,
            padding: 16,
            marginHorizontal: 16,
            backgroundColor: theme.colors.darkBlue,
          }}
        >
          <View
            style={{
              ...theme.styles.rowAlignItemsCenter,
              borderBottomWidth: 1,
              borderColor: theme.colors.white,
              paddingBottom: 16,
            }}
          >
            <Progress.Circle
              borderColor={theme.colors.placeholderTextColor}
              color={theme.colors.white}
              progress={taiSan.phanTram}
              size={40}
              showsText
              textStyle={{
                color: theme.colors.white,
                fontSize: 13,
              }}
            />
            <View style={{marginLeft: 12}}>
              <Text style={{...theme.fonts.textStyle14}}>Tài sản ròng</Text>
              <Text style={{...theme.fonts.textStyle16}}>
                {formatVND(taiSan.taiSanRong)} đ
              </Text>
            </View>
          </View>
          <Text
            style={{
              ...theme.fonts.textStyle13,
              paddingTop: 16,
            }}
          >
            Tài sản
          </Text>
        </LinearGradient>
        <View
          style={{
            ...theme.styles.rowJustifyContentSpaceAround,
            paddingHorizontal: 12,
            marginTop: 12,
          }}
        >
          {featureAssetItem('Tài sản', formatVND(taiSan.taiSan), 1)}
          {featureAssetItem('Khoản nợ', formatVND(taiSan.khoanNo), 2)}
        </View>
        <TaiSanList data={taiSan?.taiSanList || []} />
        <ThongKeTaiSan data={[]} />
        <BaoCaoThuChi data={baoCao} />
        <CoCauTaiSan
          data={taiSan?.taiSanList?.map((x) => ({
            name: x.ten,
            population: 10,
            color: x.color,
            legendFontColor: '#7F7F7F',
            legendFontSize: 15,
          }))}
        />
      </>
    );
  };

  const featureAssetItem = (title, value, id) => {
    return (
      <components.Touchable
        onPress={() => navigation.navigate(SCREEN_NAME.ThemTaiSan, {id})}
        containerStyle={{
          padding: 12,
          backgroundColor: theme.colors.darkBlue,
          width: '48%',
          borderRadius: 8,
        }}
        content={
          <>
            <View style={theme.styles.rowJustifyContentSpaceBetween}>
              <Text style={theme.fonts.textStyle14}>{title}</Text>
              <svg.PlusSvg width={20} height={20} color={theme.colors.white} />
            </View>
            <Text style={theme.fonts.textStyle16}>{value} đ</Text>
          </>
        }
      />
    );
  };
  return (
    <components.CustomSafeArea isContext={false}>
      {renderHeader()}
      <components.ScrollViewLayout content={renderContent()} />
    </components.CustomSafeArea>
  );
};

export default TaiSanRong;
