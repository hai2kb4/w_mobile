import {Text, View, StyleSheet} from 'react-native';
import React from 'react';
import {theme} from '../../constants';
import {components} from '../../components';
import {useDispatch, useSelector} from 'react-redux';
import {
  authSelector,
  setProfile,
  updateProfileApi,
} from '../../store/slices/authSlice';
import {accountImage} from '../../components/BurgerMenuModal';
import {useNavigation} from '@react-navigation/native';
import ImageCropPicker from 'react-native-image-crop-picker';
import {localVersion} from '../../utils/checkVersionApp';
import {SCREEN_NAME} from '../../navigation/ScreenName';

const AccountScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {profile} = useSelector(authSelector);
  const onPickFromGallery = () => {
    ImageCropPicker.openPicker({
      mediaType: 'photo',
      waitAnimationEnd: true,
      // compressImageQuality: 1,
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
      cropping: true,
    })
      .then((image) => {
        const arr = image.path.split('/');
        const fileName = arr[arr.length - 1];
        const savedData = {
          type: image.mime,
          name: fileName,
          uri: image.path,
          size: image.size,
        };
        const formData = new FormData();
        formData.append('fileUpload', savedData);
        const payload = {
          formData,
          onSuccess: (result) => {
            dispatch(
              updateProfileApi({
                avatarUrl: result.data.url,
                onSuccess: () => {
                  dispatch(
                    setProfile({...profile, avatarUrl: result.data.url}),
                  );
                },
              }),
            );
          },
        };
        // dispatch(uploadFile(payload));
      })
      .catch((e) => console.log(e));
  };

  const renderHeader = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          padding: 16,
          borderBottomWidth: 1,
          borderColor: theme.colors.placeholderTextColor,
        }}
      >
        <components.ImageLoader
          imageUrl={profile?.avatarUrl}
          source={accountImage}
          style={styles.avatar}
          resizeMode='cover'
        />
        <components.Touchable
          activeOpacity={profile && !profile.isGuest ? 1 : 0.8}
          onPress={() => {
            if (profile && profile.isGuest) {
              navigation.navigate(SCREEN_NAME.SignIn);
            }
          }}
          containerStyle={{
            marginLeft: 12,
          }}
          content={
            <>
              <Text
                style={{
                  textAlign: 'center',
                  ...theme.fonts.H3,
                  color: theme.colors.white,
                  textTransform: 'capitalize',
                  marginBottom: 4,
                }}
              >
                Tên tài khoản
              </Text>
              <Text
                style={{
                  ...theme.fonts.H4,
                  color: theme.colors.stroke,
                }}
              >
                Loại tài khoản
              </Text>
            </>
          }
        />
      </View>
    );
  };

  const renderContent = () => {
    return (
      <components.ScrollViewLayout
        containerStyle={{...styles.container}}
        content={
          <View style={{marginBottom: 32}}>
            <View style={styles.menu}>
              <components.ProfileItem
                title='Thông tin cá nhân'
                onPress={() => navigation.navigate(SCREEN_NAME.EDIT_ACCOUNT)}
              />
              <components.ProfileItem
                title='Mã của bạn'
                onPress={() =>
                  navigation.navigate(
                    !profile.isGuest
                      ? SCREEN_NAME.MyQRCode
                      : SCREEN_NAME.SignIn,
                    {
                      screenName: SCREEN_NAME.AccountScreen,
                    },
                  )
                }
              />
              <components.ProfileItem
                title='Nhập mã giới thiệu'
                onPress={() => navigation.navigate(SCREEN_NAME.Favorite)}
              />
              <components.ProfileItem
                title='Giao dịch định kỳ'
                onPress={() => navigation.navigate(SCREEN_NAME.PrivacyPolicy)}
              />
              <components.ProfileItem title='Thiết lập các hũ' />
            </View>
            <View style={styles.menu}>
              <components.ProfileItem
                title='Cài đặt chung'
                onPress={() => navigation.navigate(SCREEN_NAME.SettingScreen)}
              />
              <components.ProfileItem
                title='Giải đáp từ App'
                onPress={() => navigation.navigate(SCREEN_NAME.SettingScreen)}
              />
              <components.ProfileItem
                title='Kết nối'
                onPress={() => navigation.navigate(SCREEN_NAME.PrivacyPolicy)}
              />
              <components.ProfileItem title='Trở thành chuyên gia của App' />
              {profile && !profile.isGuest && (
                <components.ProfileItem
                  title='Đăng xuất'
                  onPress={() => navigation.navigate(SCREEN_NAME.LogOut)}
                />
              )}
            </View>
            <Text
              style={{
                ...theme.fonts.textStyle13,
                color: theme.colors.white,
                textAlign: 'center',
                marginTop: 12,
              }}
            >
              Version {localVersion} - Prod - PUBLISHED
            </Text>
          </View>
        }
      />
    );
  };

  return (
    <components.CustomSafeArea isContext={false}>
      {renderHeader()}
      {renderContent()}
    </components.CustomSafeArea>
  );
};

export default AccountScreen;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },
  menu: {
    padding: 16,
    backgroundColor: theme.colors.darkBlue,
    marginTop: 16,
    borderRadius: 12,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 50,
  },
  cameraIcon: {
    position: 'absolute',
    right: -5,
    bottom: -5,
    backgroundColor: theme.colors.stroke,
    padding: 5,
    borderRadius: 100,
  },
});
