import React, {useState} from 'react';
import {components} from '../../components';
import {StyleSheet, Text, View} from 'react-native';
import {theme} from '../../constants';
const dataTab = ['Chuyên gia', 'Tài liệu'];

const Expert = () => {
  const [selectedTab, setSelectTab] = useState(0);
  const renderHeader = () => {
    return (
      <>
        <components.Header title={'Chuyên gia quản lý TCCN'} />
        <View
          style={{
            alignItems: 'center',
            backgroundColor: theme.colors.darkBlue,
            borderRadius: 8,
          }}
        >
          <components.SearchBar
            // ref={searchFreeTextRef}
            // onTextSearchChange={onTextSearchChange}
            placeholder='Tìm kiếm...'
          />
        </View>
        <View
          style={{
            ...theme.styles.rowJustifyContentSpaceBetween,
            marginVertical: 24,
          }}
        >
          {featureBox()}
          {featureBox()}
          {featureBox()}
          {featureBox()}
        </View>

        <View style={styles.tab}>
          {dataTab.map((item, index, arr) =>
            rederButtonTab(item, index, arr.length),
          )}
        </View>
      </>
    );
  };

  const featureBox = () => {
    return (
      <components.Touchable
        content={
          <View style={theme.styles.justifyContentCenter}>
            <View
              style={{
                width: 70,
                height: 70,
                backgroundColor: 'red',
                borderRadius: 12,
              }}
            />
            <Text style={{...theme.fonts.textStyle13, lineHeight: 24}}>
              Chứng khoán
            </Text>
          </View>
        }
      />
    );
  };

  const rederButtonTab = (name, index, length) => {
    return (
      <components.Touchable
        key={index}
        onPress={() => {
          // dispatch(setLoading(true));
          setSelectTab(index);
          // formSearch.current.query.isCreateByUser = !(index === 0);
          // reloadData();
        }}
        containerStyle={{
          ...styles.buttonTab,
          width: (theme.sizes.width - 32) / length,
          // backgroundColor:
          //   selectedTab === index ? theme.colors.white : theme.colors.gray,
          borderWidth: selectedTab === index ? 1 : 0,
          borderColor: theme.colors.stroke,
        }}
        content={
          <Text
            style={{
              ...theme.fonts.textStyle13,
              fontWeight: selectedTab === index ? '700' : '400',
            }}
          >
            {name}
          </Text>
        }
      />
    );
  };
  return (
    <components.CustomSafeArea isContext={false}>
      <View style={styles.container}>{renderHeader()}</View>
    </components.CustomSafeArea>
  );
};

export default Expert;

const styles = StyleSheet.create({
  container: {
    flex: 1,

    padding: 16,
  },
  tab: {
    ...theme.styles.justifyContentCenter,
    backgroundColor: theme.colors.darkBlue,
    flexDirection: 'row',
    padding: 2,
    borderRadius: 4,
  },
  buttonTab: {
    padding: 4,
    borderRadius: 4,
    height: 36,
    ...theme.styles.justifyContentCenter,
  },
});
