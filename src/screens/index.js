import {authScreens} from './Authentication';
import TabNavigator from '../navigation/TabNavigator';
import Home from './tab/Home';
import TaiSanRong from './tab/TaiSanRong';
import Account from './tab/Account';
import Expert from './tab/Expert';
import Transaction from './tab/Transaction';
import EditAccount from './Main/Account/EditAccount';
import SplashScreen from './Main/SplashScreen';
import ThemTaiSan from './Main/TaiSan/ThemTaiSan';

const allScreens = {
  ...authScreens,
  TabNavigator,
  Home,
  TaiSanRong,
  Account,
  Expert,
  Transaction,
  EditAccount,
  SplashScreen,
  ThemTaiSan,
};

const screens = Object.assign({}, allScreens);

export {screens};
