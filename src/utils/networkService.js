let networkState = false,
  socketConnected = false;

function setNetworkState(isConnected) {
  networkState = isConnected;
}

function hasInternet() {
  return networkState;
}

function setSocketConnected(isConnected) {
  socketConnected = isConnected;
}
function isSocketConnected() {
  return socketConnected;
}

export const NetworkServiceUtil = {
  setNetworkState,
  setSocketConnected,
  hasInternet,
  isSocketConnected,
};
