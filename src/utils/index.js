import {addToWishlist, removeFromWishlist} from '../store/myDoctorsSlice';
import store from '../store/store';
import {setScreen as setScreenAction} from '../store/tabSlice';
import moment from 'moment';
import {Image} from 'react-native';
import {success} from './flashMessage';

const removeVietnameseTones = (str) => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
  str = str.replace(/Đ/g, 'D');
  // Some system encode vietnamese combining accent as individual utf-8 characters
  // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ''); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
  // Remove extra spaces
  // Bỏ các khoảng trắng liền nhau
  str = str.replace(/ + /g, ' ');
  str = str.trim();
  // Remove punctuations
  // Bỏ dấu câu, kí tự đặc biệt
  str = str.replace(
    /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
    ' ',
  );
  return str;
};

function generateGreetings() {
  var currentHour = moment().format('HH');

  if (currentHour >= 3 && currentHour < 12) {
    return 'Chào buổi sáng!';
  } else if (currentHour >= 12 && currentHour < 15) {
    return 'Chào buổi trưa!';
  } else if (currentHour >= 15 && currentHour < 20) {
    return 'Chào buổi chiều!';
  } else if (currentHour >= 20 || currentHour < 3) {
    return 'Chào buổi tối!';
  } else {
    return 'Xin Chào!';
  }
}

const setScreen = (screen) => {
  store.dispatch(setScreenAction(screen));
};

const favoriteDoctorExist = (doctor, favoriteDoctorList) => {
  return favoriteDoctorList.find((i) => i.id === doctor.id); // Возвращает true или false
};

const addFavoriteDoctor = (data, dispatch, message = true) => {
  dispatch(addToWishlist(data));
  message &&
    success(`Đã thêm ${data.name} vào danh sách quan tâm!`, 'Thành công');
};

const removeFavoriteDoctor = (data, dispatch, message = true) => {
  dispatch(removeFromWishlist(data));
  message &&
    success(`Đã xóa ${data.name} trong danh sách quan tâm!`, 'Thành công');
};

const isValueEmpty = (value) => {
  let isInvalid = false;

  if (!value) {
    isInvalid = true;
  } else if (typeof value === 'object') {
    isInvalid = !value || Object.keys(value).length === 0;
  } else if (typeof value === 'string') {
    isInvalid = value.trim().length === 0;
  }

  return isInvalid;
};

const formatCurrency = (value) => {
  return new Intl.NumberFormat('it-IT', {
    style: 'currency',
    currency: 'VND',
  }).format(value);
};

const validatePhoneNumber = (input_str) => {
  const vnf_regex = /([\+84|84|0]+(2|3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;
  return vnf_regex.test(input_str);
};

export const validateEmail = (email) => {
  let re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return re.test(email);
};

const EnterInputDate = (text) => {
  var value = text
    .replace(/[^0-9\/]/g, '')
    .replace(/\/\/+/g, '/')
    .trim();

  if (value.length > 1) {
    const parts = value.split('/');
    const month = parts[0];
    const day = parts[1];
    const year = parts[2];

    if (value.length === 2 && value.indexOf('/') === -1) {
      // "10" -> "10/"
      text = value + '/';
    } else if (value.length === 5 && parts.length !== 3) {
      // "12/34" -> "12/34/"
      text = value + '/';
    } else if (
      value.length === 4 &&
      value.charAt(1) === '/' &&
      !value.charAt(3) === '/'
    ) {
      // "1/15" -> "1/15/"
      // but not "1/1/" -> "1/1//"
      text = value + '/';
    } else if (parts.length > 3 && value[value.length - 1] === '/') {
      // "1/15/1950/" -> "1/15/1950"
      text = value.slice(0, -1);
    } else if (parts.length === 3 && year.length > 4) {
      // "1/15/19501" -> "1/15/1950"
      // avoiding slicing month and day w/ .slice(0, 2) for now...
      text = month + '/' + day + '/' + year.slice(0, 4);
    } else if (value !== text) {
      text = value;
    }
  }
  return text;
};

const formatDate = (
  dateStr = new Date(),
  isTime = false,
  ignoreDay = false,
) => {
  const date = moment(dateStr);
  let dateFormatted = date.format('DD/MM/YYYY');
  if (ignoreDay) {
    dateFormatted = date.format('DD/MM/YYYY HH:mm');
  }
  if (isTime) {
    dateFormatted = date.format('HH:mm');
  }
  const sliceIndex = 5;
  return dateFormatted.slice(0, sliceIndex) + dateFormatted.slice(sliceIndex);
};

const formatDayAndMonth = (dateStr) => {
  const date = moment(dateStr);
  let dateFormatted = date.format('DD/MM');
  const sliceIndex = 5;
  return dateFormatted.slice(0, sliceIndex) + dateFormatted.slice(sliceIndex);
};

const formatVND = (n) => {
  let val = (n / 1).toFixed(0).replace(',', '.');
  return val.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

const scaleHeight = async ({source, maxWidth, maxHeight}) => {
  let height = 0;
  let width = 0;
  await Image.getSize(source, (srcWidth, srcHeight) => {
    const ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    height = srcHeight * ratio;
    width = srcWidth * ratio;
  });
  return {height, width};
};

export const guidEmpty = '00000000-0000-0000-0000-000000000000';

// const SetBadgeNotification = (notifyCount = -1) => {
//   if (notifyCount >= 0) {
//     if (Platform.OS === 'ios') {
//       firebase.notifications().setBadge(notifyCount);
//     } else {
//       BadgeAndroid.setBadge(notifyCount);
//     }
//   } else {
//     firebase
//       .notifications()
//       .getBadge()
//       .then((count) => {
//         count++;

//         if (Platform.OS === 'ios') {
//           firebase.notifications().setBadge(count);
//         } else {
//           BadgeAndroid.setBadge(count);
//         }
//       })
//       .then(() => {
//         // console.log('Doing great');
//       })
//       .catch((error) => {
//         // console.log('fail to count');
//       });
//   }
// };

export {
  scaleHeight,
  setScreen,
  favoriteDoctorExist,
  addFavoriteDoctor,
  removeFavoriteDoctor,
  isValueEmpty,
  formatCurrency,
  validatePhoneNumber,
  formatDate,
  formatVND,
  removeVietnameseTones,
  formatDayAndMonth,
  // SetBadgeNotification,
  EnterInputDate,
  generateGreetings,
};
