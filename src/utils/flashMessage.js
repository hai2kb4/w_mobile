import {showMessage} from 'react-native-flash-message';

const FlashMessage = ({
  message = 'Thông báo',
  description,
  type = 'success',
  duration = 3000,
  ...rest
}) => {
  showMessage({
    message,
    description,
    type,
    duration,
    ...rest,
  });
};

const success = (description, message) => FlashMessage({description, message});

const warning = (description, message) =>
  FlashMessage({description, message, type: 'warning'});

const error = (description, message) =>
  FlashMessage({description, message, type: 'error'});

export {success, warning, error};
