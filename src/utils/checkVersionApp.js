import {compareVersions} from 'compare-versions';
import DeviceInfo from 'react-native-device-info';
import remoteConfig from '@react-native-firebase/remote-config';
import {Platform} from 'react-native';
export const localVersion = DeviceInfo.getVersion();
export const checkVersionApp = async () => {
  let remoteValues = null;
  await remoteConfig()
    .fetch(300)
    .then(() => remoteConfig().activate())
    .then(() => remoteConfig().getValue('app_version'))
    .then((snapshot) => {
      const app = JSON.parse(snapshot._value);
      const version = Platform.OS === 'ios' ? app.ios : app.android;
      if (version && compareVersions(version, localVersion) === 1) {
        remoteValues = Platform.OS === 'ios' ? app.ios_url : app.android_url;
      }
    })
    .catch((error) => null);
  return remoteValues;
};
