import {combineReducers} from '@reduxjs/toolkit';
import authReducer from '../slices/authSlice';
import tabReducer from '../tabSlice';
import modalReducer from '../modalSlice';
import appSlice from '../slices/appSlice';
import notificationSlice from '../slices/notificationSlice';
import mobileSlice from '../slices/mobileSlice';
import taiSanSlice from '../slices/taiSanSlice';

const rootReducer = combineReducers({
  app: appSlice,
  auth: authReducer,
  tab: tabReducer,
  modal: modalReducer,
  notification: notificationSlice,
  mobile: mobileSlice,
  taiSan: taiSanSlice,
});

export default rootReducer;
