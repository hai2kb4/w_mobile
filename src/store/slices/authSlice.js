import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import AuthService from '../services/authService';
import Storage, {STORAGE_APP} from '../../utils/storage';
import {cloneDeep} from 'lodash';
import {showMessage} from 'react-native-flash-message';

export const profileApi = createAsyncThunk(
  'auth/profileApi',
  async (_, {rejectWithValue}) => {
    try {
      const response = await AuthService.getProfile();
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const updateProfileApi = createAsyncThunk(
  'auth/updateProfileApi',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      delete data.onSuccess;
      const response = await AuthService.updateProfile(data);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const loginAsGuest = createAsyncThunk(
  'user/loginAsGuest',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.loginAsGuest(payload);
      Storage.setItem(STORAGE_APP.AUTH, response.data.tokenData);
      Storage.setItem(STORAGE_APP.FIREBASE_TOKEN, response.data.firebaseToken);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const signInApi = createAsyncThunk(
  'auth/signInApi',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.login(payload);
      Storage.setItem(STORAGE_APP.AUTH, response.data.tokenData);
      Storage.setItem(STORAGE_APP.FIREBASE_TOKEN, response.data.firebaseToken);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const signInApiSms = createAsyncThunk(
  'auth/signInApiSms',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.loginSms(payload);
      Storage.setItem(STORAGE_APP.AUTH, response.data.tokenData);
      Storage.setItem(STORAGE_APP.FIREBASE_TOKEN, response.data.firebaseToken);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch ({response}) {
      if (!response.data) {
        throw response;
      }
      return rejectWithValue(response?.data || response);
    }
  },
);

export const signUpValidateApi = createAsyncThunk(
  'auth/signUpValidateApi',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.signUpValidate(payload);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const signupApi = createAsyncThunk(
  'auth/signupApi',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.signUp(payload);
      if (onSuccess) {
        onSuccess(response);
      }
      Storage.setItem(STORAGE_APP.AUTH, response.data.tokenData);
      Storage.setItem(STORAGE_APP.FIREBASE_TOKEN, response.data.firebaseToken);
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const changePasswordApi = createAsyncThunk(
  'auth/changePasswordApi',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.changePassword(payload);
      if (onSuccess) {
        onSuccess(response);
      }
      showMessage({
        message: 'Thông báo',
        description: 'Thay đổi mật khẩu thành công!',
        type: 'success',
        duration: 3000,
      });
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const resetPasswordApi = createAsyncThunk(
  'auth/resetPasswordApi',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.resetPassword(payload);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const logoutApi = createAsyncThunk(
  'auth/logoutApi',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.logout(payload);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const refreshToken = createAsyncThunk(
  'auth/refreshToken',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.refreshToken(payload);
      if (onSuccess) {
        onSuccess(response.data);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const sendSMSOtp = createAsyncThunk(
  'auth/sendSMSOtp',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.sendOtp(payload);
      if (onSuccess) {
        onSuccess(response.data);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const verifySMSOtp = createAsyncThunk(
  'auth/verifySMSOtp',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.verifyOtp(payload);
      if (onSuccess) {
        onSuccess(response.data);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const deleteUser = createAsyncThunk(
  'auth/deleteUser',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await AuthService.delete();
      if (onSuccess) {
        onSuccess(response.data);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

const initialState = {
  isLoading: false,
  profile: null,
  firebaseToken: null,
  userName: '',
  fullName: '',
  isGuest: false,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUserName: (state, {payload}) => {
      state.userName = payload;
    },
    setLoading: (state, {payload}) => {
      state.isLoading = payload;
    },
    setFullName: (state, {payload}) => {
      state.fullName = payload;
    },
    setFirebaseToken: (state, {payload}) => {
      state.firebaseToken = payload;
    },
    setProfile: (state, {payload}) => {
      state.profile = payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(profileApi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(profileApi.fulfilled, (state, action) => {
      state.isLoading = false;
      state.profile = action.payload;
      state.userName = '';
      state.fullName = '';
    });
    builder.addCase(profileApi.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(loginAsGuest.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(loginAsGuest.fulfilled, (state, action) => {
      state.isLoading = false;
    });
    builder.addCase(loginAsGuest.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(signUpValidateApi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(signUpValidateApi.fulfilled, (state, action) => {
      state.isLoading = false;
    });
    builder.addCase(signUpValidateApi.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(signInApi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(signInApi.fulfilled, (state, action) => {
      state.isLoading = false;
      state.profile = action.payload;
    });
    builder.addCase(signInApi.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(signInApiSms.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(signInApiSms.fulfilled, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(signInApiSms.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(signupApi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(signupApi.fulfilled, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(signupApi.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(refreshToken.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(refreshToken.fulfilled, (state, {payload}) => {
      state.isLoading = false;
      Storage.setItem(STORAGE_APP.AUTH, payload.tokenData);
      Storage.setItem(STORAGE_APP.FIREBASE_TOKEN, payload.firebaseToken);
      state.profile = payload;
    });
    builder.addCase(refreshToken.rejected, (state, _action) => {
      state.isLoading = false;
      Storage.removeItem(STORAGE_APP.AUTH);
      Storage.removeItem(STORAGE_APP.FIREBASE_TOKEN);
    });
    builder.addCase(resetPasswordApi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(resetPasswordApi.fulfilled, (state, {payload}) => {
      state.isLoading = false;
      Storage.setItem(STORAGE_APP.AUTH, payload.tokenData);
      Storage.setItem(STORAGE_APP.FIREBASE_TOKEN, payload.firebaseToken);
      state.profile = payload;
    });
    builder.addCase(resetPasswordApi.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(changePasswordApi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(changePasswordApi.fulfilled, (state, {payload}) => {
      state.isLoading = false;
      Storage.setItem(STORAGE_APP.AUTH, payload.tokenData);
      Storage.setItem(STORAGE_APP.FIREBASE_TOKEN, payload.firebaseToken);
    });
    builder.addCase(changePasswordApi.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(logoutApi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(logoutApi.fulfilled, (state, _action) => {
      state.isLoading = false;
      state.profile = null;
    });
    builder.addCase(logoutApi.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(sendSMSOtp.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(sendSMSOtp.fulfilled, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(sendSMSOtp.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(verifySMSOtp.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(verifySMSOtp.fulfilled, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(verifySMSOtp.rejected, (state, _action) => {
      state.isLoading = false;
    });
  },
});

export const {
  setUserName,
  setLoading,
  setFullName,
  setFirebaseToken,
  setProfile,
} = authSlice.actions;

export const authSelector = (state) => state.auth;

export default authSlice.reducer;
