import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import mobileService from '../services/MobileService';
import {dummyData} from '../../constants';

const initialState = {
  isLoading: false,
  taiSanList: dummyData.taiSanListDefault,
  huTienList: [],
  historyList: [],
  baoCao: {},
  countNotRead: 0,
  totalCount: 0,
  hasNextPage: false,
};

export const GetTaiSan = createAsyncThunk(
  'mobile/GetTaiSan',
  async (_, {rejectWithValue}) => {
    try {
      const response = await mobileService.getTaiSan();
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const GetAllHuTien = createAsyncThunk(
  'mobile/GetAllHuTien',
  async (_, {rejectWithValue}) => {
    try {
      const response = await mobileService.getAllHuTien();
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const getAllHistory = createAsyncThunk(
  'mobile/getAllHistory',
  async (_, {rejectWithValue}) => {
    try {
      const response = await mobileService.getAllHistory();
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const getBaoCaoThuChi = createAsyncThunk(
  'mobile/getBaoCaoThuChi',
  async (_, {rejectWithValue}) => {
    try {
      const response = await mobileService.baoCaoThuChi();
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

const mobileSlice = createSlice({
  name: 'mobile',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => {
      state.isLoading = payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(GetAllHuTien.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(GetAllHuTien.fulfilled, (state, action) => {
      state.isLoading = false;
      state.huTienList = action.payload;
    });
    builder.addCase(GetAllHuTien.rejected, (state, _action) => {
      state.isLoading = false;
    });

    builder.addCase(getAllHistory.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(getAllHistory.fulfilled, (state, action) => {
      state.isLoading = false;
      state.historyList = action.payload;
    });
    builder.addCase(getAllHistory.rejected, (state, _action) => {
      state.isLoading = false;
    });

    builder.addCase(GetTaiSan.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(GetTaiSan.fulfilled, (state, action) => {
      state.isLoading = false;
      state.taiSanList = action.payload;
    });
    builder.addCase(GetTaiSan.rejected, (state, _action) => {
      state.isLoading = false;
    });

    builder.addCase(getBaoCaoThuChi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(getBaoCaoThuChi.fulfilled, (state, action) => {
      state.isLoading = false;
      state.baoCao = action.payload;
    });
    builder.addCase(getBaoCaoThuChi.rejected, (state, _action) => {
      state.isLoading = false;
    });
  },
});

export const {setLoading, setAllowOnBoarding, setCoords} = mobileSlice.actions;

export const mobileSelector = (state) => state.mobile;

export default mobileSlice.reducer;
