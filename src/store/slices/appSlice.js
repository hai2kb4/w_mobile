import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  isLoading: false,
  longitude: '',
  latitude: '',
  deviceId: '',
  allowOnBoarding: true,
  hostConfig: null,
};

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => {
      state.isLoading = payload;
    },
    setAllowOnBoarding: (state, {payload}) => {
      state.allowOnBoarding = payload;
    },
    setCoords: (state, {payload}) => {
      state.longitude = payload.longitude;
      state.latitude = payload.latitude;
      state.deviceId = payload.deviceId;
    },
    setHostConfig: (state, {payload}) => {
      state.hostConfig = payload;
    },
  },
});

export const {setLoading, setAllowOnBoarding, setCoords, setHostConfig} =
  appSlice.actions;

export const appSelector = (state) => state.app;

export default appSlice.reducer;
