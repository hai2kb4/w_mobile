import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import NotificationService from '../services/NotificationService';

export const GetAllNotificatioin = createAsyncThunk(
  'notification/GetAllNotificatioin',
  async (data, {rejectWithValue}) => {
    try {
      const response = await NotificationService.getAllNotify(data);
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const GetNotificatioinDetail = createAsyncThunk(
  'notification/GetNotificatioinDetail',
  async (id, {rejectWithValue}) => {
    try {
      const response = await NotificationService.getNotifyDetail(id);
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const UpdateNotificationReadAll = createAsyncThunk(
  'notification/UpdateNotificationReadAll',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const response = await NotificationService.getNotifyReadAll();
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const UpdateNotificationReadById = createAsyncThunk(
  'notification/UpdateNotificationReadById',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess, id} = data;
      const response = await NotificationService.getNotifyMarkRead(id);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const DeleteNotificationAll = createAsyncThunk(
  'notification/DeleteNotificationAll',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const response = await NotificationService.deleteNotifyAll();
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

const initialState = {
  isLoading: false,
  notificationList: [],
  notificationDetail: {},
  countNotRead: 0,
  totalCount: 0,
  hasNextPage: false,
};

const notificationSlice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => {
      state.isLoading = payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(GetAllNotificatioin.pending, (state, _action) => {});
    builder.addCase(GetAllNotificatioin.fulfilled, (state, action) => {
      state.isLoading = false;
      state.notificationList = action.payload.data;
      state.countNotRead = action.payload.countNotRead;
      state.totalCount = action.payload.totalCount;
      state.hasNextPage = action.payload.hasNextPage;
    });
    builder.addCase(GetAllNotificatioin.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(GetNotificatioinDetail.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(GetNotificatioinDetail.fulfilled, (state, action) => {
      state.isLoading = false;
      state.notificationDetail = action.payload.data;
    });
    builder.addCase(GetNotificatioinDetail.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(UpdateNotificationReadAll.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(UpdateNotificationReadAll.fulfilled, (state, action) => {
      state.isLoading = false;
    });
    builder.addCase(UpdateNotificationReadAll.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(UpdateNotificationReadById.fulfilled, (state, action) => {
      state.isLoading = false;
    });
    builder.addCase(UpdateNotificationReadById.rejected, (state, _action) => {
      state.isLoading = false;
    });
    builder.addCase(DeleteNotificationAll.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(DeleteNotificationAll.fulfilled, (state, action) => {
      state.isLoading = false;
    });
    builder.addCase(DeleteNotificationAll.rejected, (state, _action) => {
      state.isLoading = false;
    });
  },
});

export const {setLoading, setAllowOnBoarding, setCoords} =
  notificationSlice.actions;

export const notificationSelector = (state) => state.notification;

export default notificationSlice.reducer;
