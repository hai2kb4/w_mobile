import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import TaiSanService from '../services/taiSanService';
import {cloneDeep} from 'lodash';

export const getTaiSanRong = createAsyncThunk(
  'auth/getTaiSanRong',
  async (_, {rejectWithValue}) => {
    try {
      const response = await TaiSanService.getTaiSanRong();
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const createTaiSanRong = createAsyncThunk(
  'auth/createTaiSanRong',
  async (data, {rejectWithValue}) => {
    try {
      const {onSuccess} = data;
      const payload = cloneDeep(data);
      delete payload.onSuccess;
      const response = await TaiSanService.create(payload);
      if (onSuccess) {
        onSuccess(response);
      }
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

export const getBaoCaoThuChi = createAsyncThunk(
  'taiSan/getBaoCaoThuChi',
  async (_, {rejectWithValue}) => {
    try {
      const response = await TaiSanService.baoCaoThuChi();
      return response.data;
    } catch (err) {
      return rejectWithValue(err.message || 'Error');
    }
  },
);

const initialState = {
  isLoading: false,
  taiSan: {},
  baoCao: {},
};

const taiSanSlice = createSlice({
  name: 'taiSan',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => {
      state.isLoading = payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getTaiSanRong.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(getTaiSanRong.fulfilled, (state, action) => {
      state.isLoading = false;
      state.taiSan = action.payload;
    });
    builder.addCase(getTaiSanRong.rejected, (state, _action) => {
      state.isLoading = false;
    });

    builder.addCase(createTaiSanRong.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(createTaiSanRong.fulfilled, (state, action) => {
      state.isLoading = false;
    });
    builder.addCase(createTaiSanRong.rejected, (state, _action) => {
      state.isLoading = false;
    });

    builder.addCase(getBaoCaoThuChi.pending, (state, _action) => {
      state.isLoading = true;
    });
    builder.addCase(getBaoCaoThuChi.fulfilled, (state, action) => {
      state.isLoading = false;
      state.baoCao = action.payload;
    });
    builder.addCase(getBaoCaoThuChi.rejected, (state, _action) => {
      state.isLoading = false;
    });
  },
});

export const {setLoading} = taiSanSlice.actions;

export const taiSanSelector = (state) => state.taiSan;

export default taiSanSlice.reducer;
