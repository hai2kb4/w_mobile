import auth from '@react-native-firebase/auth';
import {showMessage} from 'react-native-flash-message';

const verifyPhoneNumber = async (phoneNumber) => {
  return new Promise(async (resolve, reject) => {
    await auth()
      .verifyPhoneNumber(phoneNumber)
      .then((result) => resolve(result.verificationId))
      .catch((err) => {
        let message = '';
        if (err.code === 'auth/too-many-requests') {
          message =
            'Chúng tôi đã chặn tất cả các yêu cầu từ thiết bị này do hoạt động bất thường. Vui lòng Thử lại sau.';
        } else if (err.code === 'auth/invalid-phone-number') {
          message = 'Định dạng số điện thoại không hợp lệ';
        } else {
          message = err + '';
        }
        showMessage({
          message: 'Cảnh báo!',
          description: message,
          type: 'warning',
          duration: 3000,
        });

        reject(err);
      });
  });
};

const confirmCode = (verificationId, code) => {
  const credential = auth.PhoneAuthProvider.credential(verificationId, code);
  return new Promise(async (resolve, reject) => {
    await auth()
      .signInWithCredential(credential)
      .then(() => Promise.resolve(true))
      .catch((err) => {
        let message = '';
        if (err.code === 'auth/session-expired') {
          message =
            'Mã sms đã hết hạn. Vui lòng gửi lại mã xác minh để thử lại.';
        } else if (err.code === 'auth/invalid-verification-code') {
          message =
            'Mã xác minh sms được sử dụng để tạo thông tin xác thực điện thoại không hợp lệ. Vui lòng gửi lại sms mã xác minh và đảm bảo sử dụng mã xác minh do người dùng cung cấp.';
        } else {
          message = 'Mã xác minh không hợp lệ.';
        }
        showMessage({
          message: 'Cảnh báo!',
          description: message,
          type: 'danger',
          duration: 3000,
        });
        return Promise.reject(err);
      });
  });
};

const signInFirebaseToken = (token) => {
  return new Promise(async (resolve, reject) => {
    await auth()
      .signInWithCustomToken(token + '')
      .then((result) => resolve(result))
      .catch((err) => {
        let message = '';
        if (err.code === 'auth/invalid-custom-token') {
          message = 'Hết hạn đăng nhập';
        } else {
          message = 'Lỗi xác thực';
        }
        showMessage({
          message: 'Thông báo!',
          description: message,
          type: 'danger',
        });
        reject(err);
      });
  });
};

const signOutUser = () => {
  return new Promise(async (resolve, reject) => {
    try {
      await auth().signOut();
      resolve(true);
    } catch (e) {
      reject(e);
    }
  });
};

export const FirebaseAuthService = {
  verifyPhoneNumber,
  confirmCode,
  signInFirebaseToken,
  signOutUser,
};
