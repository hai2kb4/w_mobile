const PREFIX = '/api';

// Auth
export const USER_PREFIX = `${PREFIX}/User`;
export const SIGNUP_API = `${USER_PREFIX}/register/phone`;
export const LOGIN_API = `${USER_PREFIX}/login`;
export const LOGIN_API_SMS = `${USER_PREFIX}/login/sms`;
export const PROFILE_API = `${USER_PREFIX}/profile`;
export const CHANGE_PASSWORD_API = `${USER_PREFIX}/password/change`;
export const RESET_PASSWORD_API = `${USER_PREFIX}/password/reset-by-phone`;
export const GET_AUTH_METHOD = `${USER_PREFIX}/get-authen-method`;
export const LOGIN_AS_GUEST = `${USER_PREFIX}/login-as-guest`;
export const LOGOUT_API = `${USER_PREFIX}/logout`;
export const USER_DELETE_API = `${USER_PREFIX}/delete`;
export const SEND_OTP_API = `${USER_PREFIX}/send-otp`;
export const VERIFY_OTP_API = `${USER_PREFIX}/verify-otp`;
export const REFRESH_TOKEN_API = `${USER_PREFIX}/refresh-token`;

//Mobile
export const MOBILE_PREFIX = `${PREFIX}/Mobile`;
export const MOBILE_HU_TIEN_API = `${MOBILE_PREFIX}/Hutien`;

const NOTIFICATION_PREFIX = `${PREFIX}/Notification`;
export const GET_ALL_NOTIFICATION_API = `${NOTIFICATION_PREFIX}/mobile/search`;
export const NOTIFICATION_DETAIL_API = `${NOTIFICATION_PREFIX}/get-by-id`;
export const NOTIFICATION_READ_ALL = `${NOTIFICATION_PREFIX}/read-all`;
export const NOTIFICATION_MARK_READ = `${NOTIFICATION_PREFIX}/mark-read`;
export const NOTIFICATION_DELETE_ALL = `${NOTIFICATION_PREFIX}/delete-all`;

export const TAI_SAN_PREFIX = `${PREFIX}/TaiSan`;
