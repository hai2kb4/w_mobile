import axios from 'axios';
import axiosRetry from 'axios-retry';
import Storage, {STORAGE_APP} from '../../utils/storage';
import Config from 'react-native-config';
import {isValueEmpty} from '../../utils';
import {REFRESH_TOKEN_API} from './Constant';
import {useSelector} from 'react-redux';
import {appSelector} from '../slices/appSlice';
import {authSelector} from '../slices/authSlice';
import {showMessage} from 'react-native-flash-message';

const unauthorizedCode = [401, 403];

const service = axios.create({
  baseURL: Config.REACT_APP_LOCAL_ENDPOINT_URL,
  timeout: 60000,
});
// Config
const TOKEN_PAYLOAD_KEY = 'authorization';

axiosRetry(service, {
  retries: 3,
  retryDelay: (retryCount) => {
    return retryCount * 300;
  },
  retryCondition: (error) => {
    try {
      const errorCode = error.response.status;
      return (
        (errorCode >= 500 && errorCode < 600) ||
        errorCode === 401 ||
        errorCode === 403 ||
        errorCode === 405
      );
    } catch (_) {
      return true;
    }
  },
});

// API Request interceptor
service.interceptors.request.use(
  async (config) => {
    const auth = await Storage.getItem(STORAGE_APP.AUTH);
    if (!isValueEmpty(auth)) {
      config.headers[TOKEN_PAYLOAD_KEY] = `Bearer ${auth.token}`;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

// API respone interceptor
service.interceptors.response.use(
  (response) => {
    return response.data;
  },
  async (err) => {
    if (err.response.status === 601) {
      try {
        const {latitude, longitude} = useSelector(appSelector);
        const {firebaseToken} = useSelector(authSelector);
        const {refreshToken} = await Storage.getItem(STORAGE_APP.AUTH);
        const payload = {
          latitude,
          longitude,
          firebaseToken,
          token: refreshToken,
        };
        if (refreshToken) {
          const response = await axios.post(REFRESH_TOKEN_API, payload, {
            baseURL: Config.APP_IDENTITY_ENDPOINT_URL,
          });
          if (response.data && response.data.data) {
            Storage.setItem(STORAGE_APP.AUTH, response.data.tokenData);
            const originalConfig = err.config;
            originalConfig._retry = true;
            service.defaults.headers.common.Authorization = `Bearer ${response.data.tokenData.token}`;
            return await service(originalConfig);
          }
        }
      } catch (error) {}
    }
    if (err.response && err.response.status !== 601) {
      let msg = '';
      const errData = err.response.data;
      msg = errData.title || 'Đã có lỗi xảy ra!';
      // Remove token and redirect
      if (unauthorizedCode.includes(err.response.status)) {
      }

      if (err.response.status >= 500 && err.response.status < 600) {
        msg = 'Internal Server Error';
      }

      if (err.response.status === 600) {
        msg = `${err.response.data.description}`;
      }

      if (err.response.status === 404) {
        msg = 'Not Found';
      }

      if (err.response.status === 500) {
        msg = 'Internal Server Error';
      }

      if (err.response.status === 508) {
        msg = 'Time Out';
      }

      if (err.response.status === 400) {
        msg = `${err.response.data.title}`;
      }

      showMessage({
        message: 'Thông báo',
        description: msg,
        type: 'warning',
        duration: 3000,
      });
    }
    return Promise.reject(err);
  },
);

export default service;
