import {MOBILE_HU_TIEN_API, MOBILE_PREFIX} from '../api/Constant';
import fetch from '../api/FetchInterceptor';
const mobileService = {};

mobileService.getTaiSan = function () {
  return fetch.get(`${MOBILE_PREFIX}/tai-san`);
};

mobileService.getAllHuTien = function () {
  return fetch.get(`${MOBILE_HU_TIEN_API}/get-all`);
};

mobileService.getAllHistory = function () {
  return fetch.get(`${MOBILE_PREFIX}/history`);
};

mobileService.baoCaoThuChi = function () {
  return fetch.get(`${MOBILE_PREFIX}/bao-cao-thu-chi`);
};

export default mobileService;
