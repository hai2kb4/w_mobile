import {
  GET_ALL_NOTIFICATION_API,
  NOTIFICATION_DELETE_ALL,
  NOTIFICATION_DETAIL_API,
  NOTIFICATION_MARK_READ,
  NOTIFICATION_READ_ALL,
} from '../api/Constant';
import fetch from '../api/FetchInterceptor';
const NotificationService = {};

NotificationService.getAllNotify = function (data) {
  return fetch.post(GET_ALL_NOTIFICATION_API, data);
};

NotificationService.getNotifyDetail = function (id) {
  return fetch.get(`${NOTIFICATION_DETAIL_API}?id=${id}`, null);
};

NotificationService.getNotifyReadAll = function () {
  return fetch.get(`${NOTIFICATION_READ_ALL}`);
};

NotificationService.getNotifyMarkRead = function (notifiId) {
  return fetch.get(`${NOTIFICATION_MARK_READ}/${notifiId}`);
};

NotificationService.deleteNotifyAll = function () {
  return fetch.get(`${NOTIFICATION_DELETE_ALL}`);
};

export default NotificationService;
