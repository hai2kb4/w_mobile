import {TAI_SAN_PREFIX} from '../api/Constant';
import fetch from '../api/FetchInterceptor';

const TaiSanService = {};

TaiSanService.baoCaoThuChi = function () {
  return fetch.get(`${TAI_SAN_PREFIX}/bao-cao-thu-chi`);
};

TaiSanService.getTaiSanRong = function () {
  return fetch.get(`${TAI_SAN_PREFIX}/get-all`);
};

TaiSanService.create = function (data) {
  return fetch.post(TAI_SAN_PREFIX, data);
};

export default TaiSanService;
