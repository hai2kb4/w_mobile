import Config from 'react-native-config';
import {
  CHANGE_PASSWORD_API,
  GET_AUTH_METHOD,
  LOGIN_API,
  LOGIN_API_SMS,
  LOGIN_AS_GUEST,
  LOGOUT_API,
  REFRESH_TOKEN_API,
  RESET_PASSWORD_API,
  SEND_OTP_API,
  SIGNUP_API,
  USER_DELETE_API,
  USER_PROFILE_PREFIX,
  VERIFY_OTP_API,
} from '../api/Constant';
import fetch from '../api/FetchInterceptor';
const secretKey = Config.REACT_APP_SECRETKEY;
const baseURL = Config.REACT_APP_IDENTITY_ENDPOINT_URL;
const AuthService = {};

AuthService.getProfile = function () {
  return fetch.get(`${USER_PROFILE_PREFIX}/profile`);
};

AuthService.updateProfile = function (data) {
  return fetch.put(`${USER_PROFILE_PREFIX}/profile`, data);
};

AuthService.login = function (data) {
  return fetch.post(LOGIN_API, data);
};

AuthService.loginAsGuest = function (data) {
  return fetch.post(LOGIN_AS_GUEST, data, {
    baseURL,
  });
};

AuthService.loginSms = function (data) {
  return fetch.post(
    LOGIN_API_SMS,
    {...data, secretKey},
    {
      baseURL,
    },
  );
};

AuthService.signUpValidate = function (data) {
  return fetch.post(
    GET_AUTH_METHOD,
    {...data, secretKey},
    {
      baseURL,
    },
  );
};

AuthService.signUp = function (data) {
  return fetch.post(
    SIGNUP_API,
    {...data, secretKey},
    {
      baseURL,
    },
  );
};

AuthService.signOutApi = function (data) {
  return fetch.post(
    LOGOUT_API,
    {...data, secretKey},
    {
      baseURL,
    },
  );
};

AuthService.changePassword = function (data) {
  return fetch.post(
    CHANGE_PASSWORD_API,
    {...data, secretKey},
    {
      baseURL,
    },
  );
};

AuthService.resetPassword = function (data) {
  return fetch.post(
    RESET_PASSWORD_API,
    {...data, secretKey},
    {
      baseURL,
    },
  );
};

AuthService.refreshToken = function (data) {
  return fetch.post(REFRESH_TOKEN_API, data);
};

AuthService.logout = function (data) {
  return fetch.post(LOGOUT_API, data, {
    baseURL,
  });
};

AuthService.delete = function () {
  return fetch.delete(USER_DELETE_API, {
    baseURL,
  });
};

AuthService.sendOtp = function (data) {
  return fetch.post(
    SEND_OTP_API,
    {...data, secretKey},
    {
      baseURL,
    },
  );
};

AuthService.verifyOtp = function (data) {
  return fetch.post(
    VERIFY_OTP_API,
    {...data, secretKey},
    {
      baseURL,
    },
  );
};

export default AuthService;
