import React, {useEffect} from 'react';
import {Provider} from 'react-redux';
import FlashMessage from 'react-native-flash-message';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import store from './src/store/store';
import StackNavigator from './src/navigation/StackNavigator';
import 'react-native-gesture-handler';
import GlobalContext from './src/globalContext';
import {Alert, Linking, LogBox} from 'react-native';
import ModalExaminationBySoecialty from './src/components/Modal/ModalExaminationBySoecialty';
import NetworkState from './src/components/Warning/networkState';
import moment from 'moment';
import viLocale from 'moment/locale/vi';
import {checkVersionApp} from './src/utils/checkVersionApp';
import analytics from '@react-native-firebase/analytics';
import RNBootSplash from 'react-native-bootsplash';
import messaging from '@react-native-firebase/messaging';
import CodePush from 'react-native-code-push';
LogBox.ignoreAllLogs(true);

moment.updateLocale('vi', viLocale);
const App = () => {
  const routeNameRef = React.useRef();
  const navigationRef = React.useRef();

  useEffect(() => {
    CodePush.sync({
      updateDialog: true,
      installMode: CodePush.InstallMode.IMMEDIATE,
    }).then((r) => {});
  }, []);

  // const hostRemoveConfig = async () => {
  //   await remoteConfig()
  //     .fetch(300)
  //     .then(() => remoteConfig().activate())
  //     .then(() => remoteConfig().getValue('hostconfig'))
  //     .then(async (hostConfig) => {
  //       Storage.setItem(STORAGE_APP.HOSTCONFIG, hostConfig._value);
  //     });
  // };

  useEffect(() => {
    const init = async () => {
      // checkVersion();
    };
    init().finally(async () => {
      await messaging().registerDeviceForRemoteMessages();
      await RNBootSplash.hide();
    });
  }, []);

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {});
    return unsubscribe;
  }, []);

  useEffect(() => {
    // Assume a message-notification contains a "type" property in the data payload of the screen to open
    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log('onNotificationOpenedApp', remoteMessage);
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage) {
          setTimeout(() => {}, 1500);
        }
      });
  }, []);

  const checkVersion = () => {
    const interval = setInterval(() => {
      checkVersionApp().then((result) => {
        if (result) {
          Alert.alert(
            'Phiên bản cập nhật mới',
            'Có phiên bản mới cập nhật sửa lỗi. Bạn có muốn cập nhật?',
            [
              {
                text: 'Huỷ',
                style: 'cancel',
              },
              {text: 'Đồng ý', onPress: () => Linking.openURL(result)},
            ],
          );
        }
      });
    }, 180000);
    return () => clearInterval(interval);
  };

  return (
    <SafeAreaProvider style={{flex: 1}}>
      <Provider store={store}>
        <NavigationContainer
        // ref={navigationRef}
        // onReady={() => {
        //   routeNameRef.current = navigationRef.current.getCurrentRoute().name;
        // }}
        // onStateChange={async () => {
        //   const previousRouteName = routeNameRef.current;
        //   const currentRouteName =
        //     navigationRef.current.getCurrentRoute().name;
        //   if (previousRouteName !== currentRouteName) {
        //     await analytics().logScreenView({
        //       screen_name: currentRouteName,
        //       screen_class: currentRouteName,
        //     });
        //   }
        //   routeNameRef.current = currentRouteName;
        // }}
        >
          <StackNavigator />
        </NavigationContainer>
        <NetworkState />
        <FlashMessage position='top' />
        <GlobalContext.Consumer>
          {(global) => (
            <>
              {/* <LoadingWrapper ref={global.loadingRef} /> */}
              <ModalExaminationBySoecialty
                modalRef={global.modalRef}
                onBackButtonPress={() => global.modalRef.current?.close()}
                navigationRef={navigationRef}
              />
            </>
          )}
        </GlobalContext.Consumer>
      </Provider>
    </SafeAreaProvider>
  );
};

export default CodePush(App);
