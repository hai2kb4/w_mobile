/**
 * @format
 */
if (__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
}
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import App from './App';
import messaging from '@react-native-firebase/messaging';

messaging().setBackgroundMessageHandler(() => {});

AppRegistry.registerComponent(appName, () => App);
